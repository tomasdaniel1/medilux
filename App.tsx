import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Provider as PaperProvider } from 'react-native-paper';
import { createStackNavigator } from '@react-navigation/stack';
import RNBootSplash from "react-native-bootsplash";
import messaging from '@react-native-firebase/messaging';


import Login from './Componentes/Login.js';
import Crear from './Componentes/Crear.js';
import Medilux from './Componentes/Medilux.js';
import Tarjeta from './Componentes/Medilux/Tarjeta.js';


const Stack = createStackNavigator();

import firebase from '@react-native-firebase/app';
import Post from './Componentes/Medilux/Post.js';
import Update from './Componentes/Medilux/Update.js';
import PTP from './Componentes/Medilux/PTP.js';
import FAQ from './Componentes/Medilux/FAQ.js';
import Pagado from './Componentes/Medilux/Pagado.js';
import OTP from './Componentes/Medilux/OTP.js';
import Tipo from './Componentes/Medilux/Tipo.js';
import Notificacion from './Componentes/Medilux/Notificacion.js';
import Incidents from './Componentes/Medilux/Incidents.js';
import { PermissionsAndroid } from 'react-native';


import { Appearance } from 'react-native';
import Notificaciones from './Componentes/Medilux/Notificaciones.js';
import Medidores from './Componentes/Medilux/Medidores.js';

import { useColorScheme } from 'react-native';
import { setAppearanceLight } from 'react-native-appearance-control';
import Incidencia from './Componentes/Medilux/Incidencia.js';
import Email from './Componentes/Medilux/Email.js';
import Notification from './Componentes/Medilux/Notification.js';
import Edit from './Componentes/Medilux/Edit.js';

import EncryptedStorage from "react-native-encrypted-storage";




const RNfirebaseConfig = {
  databaseURL: "",
  apiKey: "AIzaSyAr7PuWEAZFZMigIypUntwNoflBkQndysM",
  authDomain: "medilux-1cec4.appspot.com",
  projectId: "medilux-1cec4",
  storageBucket: "medilux-1cec4.appspot.com",
  messagingSenderId: "936943575400",
  appId: "1:936943575400:android:a05c8a710785ab03a876dd"
};





export default function App() {


  const Stack = createStackNavigator();
  const [screen, setScreen] = React.useState("");

















  async function guardar() {
    try {

      if (firebase.apps.length) {


      }
      else {
        await firebase.initializeApp(RNfirebaseConfig);




      }




      setAppearanceLight();




      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS,
        {
          title: '',
          message: '',
          buttonNeutral: '',
          buttonNegative: '',
          buttonPositive: '',
        },
      );




      const tokenresponse = await EncryptedStorage.getItem("token");




      if (tokenresponse) {



        setScreen("Medilux");
      }
      else {

        setScreen("Login");

      }

      const Stack = createStackNavigator();




    } catch (error) {


      console.log(error);
    }

  }

  React.useEffect(() => {


    guardar();











    return () => {

    };
  }, [screen]);



  if (screen === "Medilux") {



    return (



      <PaperProvider>
        <NavigationContainer onReady={() => {






          RNBootSplash.hide();

        }}>
          <Stack.Navigator initialRouteName="Medilux">

            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Crear" component={Crear} />
            <Stack.Screen name="Medilux" component={Medilux} />
            <Stack.Screen name="Tarjeta" component={Tarjeta} />
            <Stack.Screen name="Incidents" component={Incidents} />
            <Stack.Screen name="Post" component={Post} />
            <Stack.Screen name="Update" component={Update} />
            <Stack.Screen name="PTP" component={PTP} />
            <Stack.Screen name="FAQ" component={FAQ} />
            <Stack.Screen name="Pagado" component={Pagado} />
            <Stack.Screen name="OTP" component={OTP} />
            <Stack.Screen name="Tipo" component={Tipo} />
            <Stack.Screen name="Notificacion" component={Notificacion} />
            <Stack.Screen name="Notificaciones" component={Notificaciones} />
            <Stack.Screen name="Email" component={Email} />
            <Stack.Screen name="Medidores" component={Medidores} />
            <Stack.Screen name="Incidencia" component={Incidencia} />
            <Stack.Screen name="Notification" component={Notification} />
            <Stack.Screen name="Edit" component={Edit} />











          </Stack.Navigator>
        </NavigationContainer>
      </PaperProvider>

    );

  }

  else if (screen === "Login") {



    return (



      <PaperProvider>
        <NavigationContainer onReady={() => {






          RNBootSplash.hide();

        }}>
          <Stack.Navigator initialRouteName="Login">

            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Crear" component={Crear} />
            <Stack.Screen name="Medilux" component={Medilux} />
            <Stack.Screen name="Tarjeta" component={Tarjeta} />
            <Stack.Screen name="Incidents" component={Incidents} />
            <Stack.Screen name="Post" component={Post} />
            <Stack.Screen name="Update" component={Update} />
            <Stack.Screen name="PTP" component={PTP} />
            <Stack.Screen name="FAQ" component={FAQ} />
            <Stack.Screen name="Pagado" component={Pagado} />
            <Stack.Screen name="OTP" component={OTP} />
            <Stack.Screen name="Tipo" component={Tipo} />
            <Stack.Screen name="Notificacion" component={Notificacion} />
            <Stack.Screen name="Notificaciones" component={Notificaciones} />
            <Stack.Screen name="Email" component={Email} />
            <Stack.Screen name="Medidores" component={Medidores} />
            <Stack.Screen name="Incidencia" component={Incidencia} />
            <Stack.Screen name="Notification" component={Notification} />
            <Stack.Screen name="Edit" component={Edit} />











          </Stack.Navigator>
        </NavigationContainer>
      </PaperProvider>

    );



  }
}

