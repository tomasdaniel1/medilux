import React from 'react';
import { BackHandler, View } from 'react-native';


import Logo from './Login/Logo.js';
import { StyleSheet } from 'react-native';

import Formbutton from './Login/Formbutton.js';
import PagerView from 'react-native-pager-view';
import { Portal, Text } from 'react-native-paper';




import Medidor from './Medilux/Medidor.js';
import Actual from './Medilux/Actual.js';
import Buttonscreen from './Medilux/Buttonscreen.js';
import Changescreen from './Medilux/Status.js';

import EncryptedStorage from "react-native-encrypted-storage";
import { gettransaction } from './Apicuentacontrato.js';
import moment from 'moment';
import Status from './Medilux/Status.js';

import messaging from '@react-native-firebase/messaging';





import { Alert } from 'react-native';
import { useToast } from 'react-native-toast-notifications';
import Mediluxalert from './Otros/Mediluxalert.js';


export default function Medilux({ navigation }) {




  let buttons = [{ imagen: require('../Imagenes/home.png'), color: '#A5ACB8', label: 'home', }, { imagen: require('../Imagenes/pago.png'), color: '#A5ACB8', label: 'pago', }, { imagen: require('../Imagenes/notifications.png'), color: '#A5ACB8', label: 'button', }, { imagen: require('../Imagenes/cuenta.png'), color: '#A5ACB8', label: 'cuenta', }];



  const [medilux, setMedilux] = React.useState([]);
  const [measurer, setMeasurer] = React.useState([]);
  const [medidor, setMedidor] = React.useState([]);
  const [cuentacontrato, setCuentacontrato] = React.useState([]);
  const [maxvalue, setMaxvalue] = React.useState(1);
  const [monthsconsumption, setMonthsconsumption] = React.useState([]);
  const [measurers, setMeasurers] = React.useState([{ alias: "Medidor depar", cuenta: "4212124312312", medidor: "3124124124" }, { alias: "Medidor #3", cuenta: "3123123123126", medidor: "1241241" }, { alias: "Medidor #5", cuenta: "3123123312312", medidor: "312321312" }, { alias: "tomas", cuenta: "3123123312312", medidor: "312321312" }, { alias: "Medidor #23", cuenta: "3123123312312", medidor: "312321312" }]);
  const [statusscreen, setStatusscreen] = React.useState("home");


  const [actualmeasurer, setActualmeasurer] = React.useState("");

  const [statuspago, setStatuspago] = React.useState(0);

  const [screen, setScreen] = React.useState("");

  const [hide, setHide] = React.useState(0);
  const [label, setLabel] = React.useState("");
  const [content, setContent] = React.useState("");

  const [actualtransaction, setActualtransaction] = React.useState([]);


  const [email, setEmail] = React.useState([]);

  const [statuscolor, setStatuscolor] = React.useState("#FFFFFF");





  const [token, setToken] = React.useState(null);




  async function cargar() {
    try {



      const monthsconsumptionresponse = await JSON.parse(await EncryptedStorage.getItem("monthsconsumption"));

      const medidorresponse = await JSON.parse(await EncryptedStorage.getItem("medidor"));


      const cuentacontratoresponse = await JSON.parse(await EncryptedStorage.getItem("cuentacontrato"));

      const measurerresponse = await JSON.parse(await EncryptedStorage.getItem("measurers"));

      const tokenresponse = await EncryptedStorage.getItem("token");




      setMedidor(medidorresponse);

      setToken(tokenresponse);

      setMeasurers(measurerresponse);
      // setMeasurer(measurerresponse);
      // setCuentacontrato([]);





      // const measurersresponse = await JSON.parse(await EncryptedStorage.getItem("measurers"));


      // measurersresponse.splice(0, 1);



      // let measurersmedilux = [{ cuenta: '', medilux: "Voltaje  \n L1" }, { cuenta: '', medilux: "Corriente  \n L1" }, { cuenta: '', medilux: "Voltaje  \n L3" }, { cuenta: '', medilux: "Corriente  \n L3" }];


      // for (measure in measurersmedilux) {







      //   measurersmedilux[measure].cuenta = measurersresponse[measure];


      // }



      // setMeasurers(measurersmedilux);

      setActualmeasurer(measurerresponse[0]);


    } catch (error) {


      console.log(error);
    }
  }




  async function load() {
    try {



      setStatuspago(0);




      const transaction = await gettransaction(actualmeasurer, token);

      transaction.data.medidorConsumo.numeroMedidor = actualmeasurer.numeroMedidor;





      setActualtransaction(transaction.data);



      if (actualmeasurer.historicoMedidor.length === 0) {


        setMedilux([{ label: "No hay pagos realizados", value: "" }]);
        setMonthsconsumption([{ label: "", value: 1 }, { label: "", value: 1 }, { label: "", value: 1 }, { label: "", value: 1 }, { label: "", value: 1 }, { label: "", value: 1 }]);





      }
      else {


        let monthconsumption = [];


        for (month in actualmeasurer.historicoMedidor) {

          monthconsumption.push({
            value: actualmeasurer.historicoMedidor[month].totalPagar, labelTextStyle: {


              fontFamily: "Outfit-Medium",
              fontSize: 10,

              color: '#A5ACB8'


            }
            , label: moment(actualmeasurer.historicoMedidor[month].fecha).format("DD/MM/YY"),
          });



        }



        setMonthsconsumption(monthconsumption);



        // setMaxvalue(maxpago);









        setMedilux(monthconsumption.slice(0, 3));



      }









    } catch (error) {


      console.log(error);



      if (error.response) {

        if (error.response.status === 401) {

          mediluxalert("Error", "El token de sesión ha expirado");


        }

        else if (error.response.data) {

          console.log(error.response.data);





          mediluxalert("Error", error.response.data.message);






        }

      }


      else if (error.request) {

        mediluxalert("Error", "Hubo un error en el servidor ");


      }




    }

    setStatuspago(1);

  }

  function mediluxalert(label, content) {


    setHide(1);



    setLabel(label);
    setContent(content);


  }

  React.useEffect(


    () => {

      let navigationhandler = BackHandler.addEventListener('hardwareBackPress', () => {





        if (navigation.isFocused()) {

          if (statusscreen === "home") {

            BackHandler.exitApp()
          }
          else {

            setStatusscreen("home");

            return 1;


          }



        }

      });

      return () => {

        navigationhandler.remove();

      }


    }
    ,
    [statusscreen]
  );


  React.useEffect(() => {


    load();


    console.log("El medidor actual es ", actualmeasurer);








    return () => {

    };
  }, [actualmeasurer]);



  React.useEffect(() => {


    cargar();



    navigation.setOptions({ headerShown: 0 });








    return () => {

    };
  }, []);

  React.useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {





      setStatuscolor("#11D077");




    });

    return unsubscribe;
  }, []);




  return (


    <View style={styles.medilux} >

      <Portal >


        <Mediluxalert hide={hide} label={label} content={content} screen={screen} navigation={navigation} setHide={setHide} />
      </Portal >


      <Status medilux={medilux} statuspago={statuspago} monthsconsumption={monthsconsumption} maxvalue={maxvalue} measurers={measurers} statuscolor={statuscolor} setStatuscolor={setStatuscolor} measurer={measurer} actualmeasurer={actualmeasurer} setActualmeasurer={setActualmeasurer} actualtransaction={actualtransaction} cuentacontrato={cuentacontrato} statusscreen={statusscreen} email={email} token={token} medidor={medidor} navigation={navigation} />

      <Buttonscreen buttons={buttons} setStatusscreen={setStatusscreen} statusscreen={statusscreen} />



    </View>


  );
}


const styles = StyleSheet.create({



  medilux: {
    backgroundColor: "#F5F8F9",

    height: "100%",

    fontFamily: 'Open Sans',


  },


  formbutton: {
    marginTop: 50,

  },

  pagerView: {
    height: "20%",
  },

  labelchartmedilux: {


    fontFamily: "Outfit-Medium"


  },

});
