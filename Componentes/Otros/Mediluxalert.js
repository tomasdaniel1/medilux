import React from 'react';
import { View } from 'react-native';

import { Image } from 'react-native';
import { StyleSheet } from 'react-native';

import { FormBuilder } from 'react-native-paper-form-builder';
import { useForm } from 'react-hook-form';
import Textmedilux from '../Otros/Textmedilux.js';
import { Button } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import { Portal } from 'react-native-paper';
import { Modal } from 'react-native-paper';

import EncryptedStorage from "react-native-encrypted-storage";

import { Checkbox } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from "react-native";




import { getcuentacontrato, postuser } from '../Apicuentacontrato.js';
import { getmeasurers, getmonthsconsumption } from '../Api.js';





export default function Mediluxalert({ label, content, hide, setHide, screen = null, route = null, navigation }) {



    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }










    return (

        <View style={styles.formbutton}>



            <Portal>
                <Modal visible={hide}   >

                    <View style={styles.modal}  >


                        <Textmedilux variant="headlineSmall" style={styles.label} label={label} ></Textmedilux>

                        <Textmedilux variant="titleMedium" style={styles.content} label={content} ></Textmedilux>

                        <Button buttonColor="#11D077" onPress={() => {

                            if (screen) {




                                if (route) {

                                    navigation.replace(screen, route);

                                }
                                else {

                                    navigation.replace(screen);

                                }



                            }
                            setHide(0);
                        }} style={styles.button} contentStyle={styles.contentbutton} labelStyle={styles.labelbutton} mode="contained"  >
                            Aceptar
                        </Button>



                    </View>


                </Modal>
            </Portal>





        </View>




















    );
}


const styles = StyleSheet.create({





    logo: {

        backgroundColor: "#FFFFFF",




        color: '#092D3D',
        fontFamily: "Outfit-Medium",







    },


    create: {





    },


    registrar: {


        textAlign: 'center',





        color: '#11D077',





    },
    modal: {

        backgroundColor: '#FFFFFF',

        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: "6%",
        borderRadius: 10,



    },

    dialogcontainer: {

        justifyContent: "center",
        alignItems: "center",
        marginTop: ""


    },


    texttheme: {

        colors: {

            background:

                "#FFFFFF"

        }


    },



    oauth: {

        justifyContent: "center",
        alignItems: "center",
        marginTop: "10%"


    },


    google: {

        width: 50,
        height: 50,
        resizeMode: 'stretch'

    },




    imagen: {

        width: '89%',
        height: '89%',

        aspectRatio: 0.7,
        resizeMode: 'contain',

    },
    buttonlabel: {



        color: '#FFFFFF',


        fontFamily: "Outfit-Bold"









    },

    labelpago: {
        fontFamily: "Outfit-Regular",
    },


    button: {


        borderRadius: 10,




        height: 50,

        marginTop: "5%",
        marginBottom: "5%"








    },

    contentbutton: {

        height: "100%",
        width: "100%"




    },





    medilux: {


        color: '#092D3D',
        fontFamily: "Outfit-Medium"




    },

    label: {

        color: '#092D3D',
        fontFamily: "Outfit-Medium",
        marginTop: "5%"






    },

    content: {

        color: '#092D3D',
        fontFamily: "Outfit-Regular",
        marginHorizontal: "6%",
        marginTop: "5%"







    },

    mediluxpago: {


        marginTop: "10%",


        color: '#092D3D',
        fontFamily: "Outfit-Medium",
        textAlign: 'center'






    },




    formbutton: {



        marginHorizontal: "5%",

        height: "10%",
        marginTop: 50,



    },



    text: {



        marginTop: 50,
    }

});
