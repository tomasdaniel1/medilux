const baseURL = "https://www.servicios.medilux.ec/medilux-api";





const testbaseURL = "https://172.17.200.50/medilux-api";


import axios from "axios";

import UserAgent from 'react-native-user-agent';



export async function getcuentacontrato(cuentacontrato) {







    try {
        const response = await axios.get(`${baseURL}/medidor/${cuentacontrato}`, null,

            {
                timeout: 23000
            });
        return response.data;
    } catch (err) {
        return null;
    }
}






export async function postpago(medidor, pago, tarjeta, tipo, token) {


    console.log("Pago", pago);

    console.log(Number(pago.medidorComision.comision));



    let document = "";
    let documentType = "";
    let parsedDocumentType = "";



    if (medidor.tipo === "1") {

        document = medidor.cedula;
        documentType = "1";
        parsedDocumentType = "Cedula";

    }
    else if (medidor.tipo === "2") {



        document = medidor.ruc;
        documentType = "2";
        parsedDocumentType = "RUC";


    }
    else if (medidor.tipo === "3") {



        document = medidor.pasaporte;
        documentType = "3";
        parsedDocumentType = "Pasaporte";



    }


    let date = new Date(Date.now()).toISOString();

    console.log(date);


    const response = await axios.post(`${baseURL}/api/Pagos/collect`, {


        userAgent: UserAgent.getUserAgent(),
        paymentDate: date,
        billingData: {
            name: medidor.nombre,
            surname: medidor.apellido,
            address: medidor.direccion,
            document: document,
            documentType: parsedDocumentType,
            // parsedDocumentType: parsedDocumentType,
            phone: medidor.telefono,
            email: medidor.email
        },
        tax: Number(pago.medidorComision.impuesto).toFixed(2),
        discount: 0.00,
        commission: Number(pago.medidorComision.comision).toFixed(2),
        subtotal: Number(pago.medidorConsumo.totalPagar).toFixed(2),
        meterNumber: pago.medidorConsumo.numeroMedidor,
        cardToken: tarjeta.payerId,
        installment: tipo.cuotas,
        installmentDescription: tipo.descripcion

    },

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}


export async function posttarjeta(token) {




    const response = await axios.post(`${baseURL}/api/Pagos/subscription`, {


        userAgent: "UserAgent.getUserAgent()",


    },

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}



export async function postuser(medidor, otp) {

    console.log(medidor.clave);


    let document = "";
    let documentType = "";
    let parsedDocumentType = "";



    if (medidor.tipo === "1") {

        document = medidor.cedula;
        documentType = 1;
        parsedDocumentType = "Cedula";

    }
    else if (medidor.tipo === "2") {


        document = medidor.pasaporte;
        documentType = 3;
        parsedDocumentType = "Pasaporte";


    }
    else if (medidor.tipo === "3") {



        medidor.nombre = medidor.razonsocial
        document = medidor.ruc;
        documentType = 2;
        parsedDocumentType = "RUC";


    }




    const response = await axios.post(`${baseURL}/api/User/create`, {





        nombre: medidor.nombre,
        apellido: medidor.apellido,
        email: medidor.email,
        clave: medidor.contrasena,
        identificacion: document,
        codigoOTP: otp,
        idTipoIdentificacion: documentType,
        telefono: medidor.telefono,
        direccion: medidor.direccion



    },

        {

            auth: {
                username: 'Med1Lux@dmin',
                password: 'fhIk6W1DOL38J3yMplx9Tw'
            },

            timeout: 23000,

        });
    return response.data;






}








export async function getpago(requestId, token) {


    const response = await axios.get(`${testbaseURL}/api/Pagos/payment/${requestId}`, null,

        {
            timeout: 23000,
            // headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}

export function tipopago(state) {


    let status = "";


    switch (state) {
        case "OK":
            status = "Ok";
            break;
        case "FAILED":
            status = "Fallido";
            break;
        case "APPROVED":
            status = "Aprobado";
            break;
        case "APPROVED_PARTIAL":
            status = "Reembolsado";
            break;
        case "PARTIAL_EXPIRED":
            status = "Reembolsado";
            break;
        case "REJECTED":
            status = "Rechazado";
            break;
        case "PENDING":
            status = "Pendiente";
            break;
        case "PENDING_VALIDATION":
            status = "Validación pendiente";
            break;
        case "PENDING_PROCESS":
            status = "Proceso pendiente";
            break;
        case "REFUNDED":
            status = "Reembolsado";
            break;
        case "REVERSED":
            status = "Reversado";
            break;
        case "ERROR":
            status = "Error";
            break;
        case "UNKNOWN":
            status = "Desconocido";
            break;
        case "MANUAL":
            status = "Manual";
            break;
        case "DISPUTE":
            status = "En disputa";
            break;
        default:
            break;
    }
    return status;


}

export async function gettipo(tarjeta, pago, token) {


    const response = await axios.get(`${baseURL}/api/Pagos/credit?payer=${tarjeta}&total=${pago}`,

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}

export async function getmonto(tarjeta, pago, tipo, token) {


    const response = await axios.post(`${baseURL}/api/Pagos/interest`,


        {
            "payer": tarjeta.payerId,
            "total": Number(Number(pago.medidorConsumo.totalPagar) + Number(pago.medidorComision.comision) + Number(pago.medidorComision.impuesto)).toFixed(2),
            "installmentDescription": tipo.descripcion,
            "installment": tipo.cuotas
        },

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}

export async function gettarjeta(requestId, token) {




    const response = await axios.get(`${baseURL}/api/Pagos/subscription/${requestId}`,

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}

export async function gethistoric(token) {


    const response = await axios.get(`${baseURL}/api/Pagos/historic`,
        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}

export async function updatenotificacion(id, token) {

    console.log();


    const response = await axios.put(`${baseURL}/api/Notification/update?idNotificacion=${id}`, null,
        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}

export async function updatetipo(tarjeta, token) {



    const response = await axios.put(`${baseURL}/api/Pagos/payers/${tarjeta}`, {


        userAgent: UserAgent.getUserAgent(),


    },
        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}

export async function deletetipo(tarjeta, token) {





    const response = await axios.delete(`${baseURL}/api/Pagos/payers/${tarjeta}`,
        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}


export async function getmeasurers(token) {


    const response = await axios.get(`${baseURL}/api/Medidor`,

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}

export async function postemail(email) {


    const response = await axios.post(`${baseURL}/api/Email/password/recovery-code`,

        {
            "email": email
        }

        ,

        {

            auth: {
                username: 'Med1Lux@dmin',
                password: 'fhIk6W1DOL38J3yMplx9Tw'
            },

            timeout: 23000
        });
    return response.data;






}

export async function postotp(email, tarjeta) {


    const response = await axios.post(`${baseURL}/api/Email/password/validate-recovery-code`,

        {
            "email": email,
            "code": tarjeta
        }
        ,

        {

            auth: {
                username: 'Med1Lux@dmin',
                password: 'fhIk6W1DOL38J3yMplx9Tw'
            },

            timeout: 23000
        });
    return response.data;






}

export async function postcontrasena(email, user) {


    console.log(email);


    const response = await axios.post(`${baseURL}/api/User/password/new`,

        {
            "email": email,
            "password": user.contrasena,
            "confirmPassword": user.confirmar
        }
        ,

        {

            auth: {
                username: 'Med1Lux@dmin',
                password: 'fhIk6W1DOL38J3yMplx9Tw'
            },

            timeout: 23000
        });
    return response.data;






}



export async function gettarjetas(token) {


    const response = await axios.get(`${baseURL}/api/Pagos/payers`,

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}



export async function getincidencias(token) {


    const response = await axios.get(`${baseURL}/api/Incidencias/usuario`,

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}

export async function getnotificaciones(token) {




    const response = await axios.get(`${baseURL}/api/Notification/user`,

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}

export async function gettipos(token) {


    const response = await axios.get(`${baseURL}/api/Incidencias/type-list`,

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}


export async function getuser(user) {


    const response = await axios.post(`${baseURL}/api/Auth/login`, {
        username: user.email,
        password: user.contrasena
    },

        {

            auth: {
                username: 'Med1Lux@dmin',
                password: 'fhIk6W1DOL38J3yMplx9Tw'
            },

            timeout: 23000
        });
    return response.data;






}

export async function getotp(user) {


    const response = await axios.post(`${baseURL}/api/Email/send-otp-register`, {
        email: user.email,
        password: user.contrasena
    },

        {

            auth: {
                username: 'Med1Lux@dmin',
                password: 'fhIk6W1DOL38J3yMplx9Tw'
            },

            timeout: 23000
        });
    return response.data;




}


export async function updatemeasurer(measurer, measurerupdate, token) {


    const response = await axios.put(`${baseURL}/api/Medidor/${measurer.id}/update`, {

        nuevoAlias: measurerupdate
    },

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}

export async function deletemeasurer(measurer, token) {


    const response = await axios.delete(`${baseURL}/api/Medidor/${measurer.id}/delete`,

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}



export async function postmeasurer(measurer, token) {


    const response = await axios.post(`${baseURL}/api/Medidor/create`, {


        nroCuentaContrato: measurer.cuenta,
        nuevoAlias: measurer.measurer
    },

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }

        });
    return response.data;






}

export async function posttoken(notificacion, token) {


    const response = await axios.post(`${baseURL}/api/Device/add
    
    
    
    `,
        {


            deviceNumber: UserAgent.getUserAgent(),
            deviceToken: notificacion


        },

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}

export async function postnotificacion(token) {


    const response = await axios.post(`${baseURL}/api/Device/valite-token
    
    `,
        {


            deviceNumber: UserAgent.getUserAgent()

        },

        {
            timeout: 23000,
            headers: { Authorization: `Bearer ${token}` }
        });
    return response.data;





}

export async function postincidencia(uri, measurer, incidencia, token) {



    let form = new FormData();

    if (uri) {

        form.append('Foto', {
            uri: uri,
            type: 'image/jpeg',
            name: 'dasdsadasdsadasdas.jpg',
        });

    }
    else {

        form.append('Foto', '');

    }



    form.append('IdMedidor', Number(measurer.measurer));

    form.append('FechaSuceso', incidencia);

    form.append('Descripcion', measurer.incidencia);






    const response = await axios.post(`${baseURL}/api/Incidencias/${measurer.tipo}/add`, form,

        {
            timeout: 23000,
            headers: {
                Authorization: `Bearer ${token}`, 'Content-Type': 'multipart/form-data'

            }

        });
    return response.data;






}

export async function gettransaction(measurer, token) {



    if (measurer.id) {

        if (token) {


            const response = await axios.post(`${baseURL}/api/Medidor/${measurer.id}/informacion-ultima-factura`, {

                nroCuentaContrato: measurer.numeroCuentaContrato

            },

                {
                    timeout: 23000,
                    headers: { Authorization: `Bearer ${token}` }

                });
            return response.data;


        }
    }






}


