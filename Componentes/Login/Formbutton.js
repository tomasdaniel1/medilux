import React from 'react';
import { View } from 'react-native';

import { Image } from 'react-native';
import { StyleSheet } from 'react-native';

import { FormBuilder } from 'react-native-paper-form-builder';
import { useForm } from 'react-hook-form';
import Textmedilux from '../Otros/Textmedilux.js';
import { Button } from 'react-native-paper';
import EncryptedStorage from "react-native-encrypted-storage";
import { ScrollView, TouchableOpacity } from "react-native";

import { TextInput } from 'react-native-paper';




import Mediluxalert from '../Otros/Mediluxalert.js';





import { getuser, getcuentacontrato, getmeasurers, gettransaction, postincidencia, postnotificacion, posttoken } from '../Apicuentacontrato.js';
import moment from 'moment';
import messaging from '@react-native-firebase/messaging';



export default function Formbutton({ cargar, estado, setCargar, setEstado, navigation }) {


  const { control, setFocus, handleSubmit } = useForm({
    defaultValues: {
      email: '',
      contrasena: '',
    },
    mode: 'onSubmit',
  });

  const [hide, setHide] = React.useState(0);
  const [label, setLabel] = React.useState("");
  const [content, setContent] = React.useState("");

  const [hidden, setHidden] = React.useState("password");

  const [icon, setIcon] = React.useState(require('../../Imagenes/see.png'));



  function edithidden() {


    if (hidden === "password") {

      setHidden("text");
      setIcon(require('../../Imagenes/hidden.png'));


    }
    else {

      setHidden("password");
      setIcon(require('../../Imagenes/see.png'));


    }

  }

  function mediluxalert(label, content) {


    setHide(1);



    setLabel(label);
    setContent(content);


  }


  async function guardar(data) {



    setEstado(1);


    setCargar("Cargando usuario");


    try {

      const user = await getuser(data);


      console.log(user);


      if (user.success) {


        const token = user.data.token;

        await EncryptedStorage.setItem("token", token);

        const measurers = await getmeasurers(token);

        console.log(measurers);

        let cedula = "";
        let ruc = "";
        let pasaporte = "";

        let razonsocial = "";

        let tipo = "";


        if (user.data.usuario.idTipoIdentificacion === 1) {

          cedula = user.data.usuario.identificacion;
          tipo = "1";



        }
        else if (user.data.usuario.idTipoIdentificacion === 2) {

          ruc = user.data.usuario.identificacion;
          tipo = "2";

          razonsocial = user.data.usuario.nombre;

        }
        else if (user.data.usuario.idTipoIdentificacion === 3) {

          pasaporte = user.data.usuario.identificacion;
          tipo = "3";

        }

        await EncryptedStorage.setItem("medidor", JSON.stringify({ email: user.data.usuario.email, nombre: user.data.usuario.nombre, apellido: user.data.usuario.apellido, pasaporte: pasaporte, razonsocial: razonsocial, ruc: ruc, telefono: user.data.usuario.telefono, tipo: tipo, direccion: user.data.usuario.direccion, cedula: cedula }));

        await EncryptedStorage.setItem("measurers", JSON.stringify(measurers.data));



        try {


          let asdasd = await postnotificacion(token);


        } catch (response) {


          console.log("dasdsadasdasd", response);


          try {


            await messaging().registerDeviceForRemoteMessages();
            const notificacion = await messaging().getToken();

            console.log("Se creo el token ", notificacion);


            await posttoken(notificacion, token);





          } catch (response) {




            console.log("dasdsadasdas", response);




          }




        }


        if (measurers.data.length === 0) {

          await navigation.replace('Medidores');


        }
        else {

          await navigation.replace('Medilux');


        }







      }
      else {

        mediluxalert("Error", user.data.message);


      }


    } catch (response) {


      console.log(response);


      if (response.response) {



        mediluxalert("Error", response.response.data.message);


      }
      else {

        mediluxalert("Error", "Ha ocurrido un error en el servidor");

      }



    }







    // if (user !== null) {



    //   if (user.contrasena === data.contrasena && user.contrasena !== null) {








    //     console.log(data.cuentacontrato);


    //     const cuentacontrato = await getcuentacontrato(user.cuentacontrato);




    //     console.log("Cuenta contrato", cuentacontrato);

    //     await EncryptedStorage.setItem("email", JSON.stringify(user.email));



    //     await EncryptedStorage.setItem("cuentacontrato", JSON.stringify(user.cuentacontrato));

    //     await EncryptedStorage.setItem("measurer", JSON.stringify(cuentacontrato[0].medidor));




    //     console.log("Cuenta contrato", cuentacontrato);






    //     if (cuentacontrato === null) {


    //       mediluxalert("Error", "El servidor esta tieniendo problemas.Intente en unos minutos");












    //     }
    //     else if (cuentacontrato.length === 0) {


    //       mediluxalert("Error", "El numero de cuenta contrato no existe");







    //     }
    //     else {






    //       console.log(cuentacontrato[0].medidor.toString().slice(2));



    //       const medidor = cuentacontrato[0].medidor.toString().slice(2);



    //       await EncryptedStorage.setItem("monthsconsumption", JSON.stringify([{ "mes": "Noviembre", "pago": "15.70" }, { "mes": "Octubre", "pago": "35.25" }, { "mes": "Septiembre", "pago": "31.23" }, { "mes": "Agosto", "pago": "36.20" }, { "mes": "Julio", "pago": "31.68" }, { "mes": "Junio", "pago": "29.11" }]));
    //       await EncryptedStorage.setItem("measurers", JSON.stringify(["true", "121.5", "1.28", "121.0", "1.28"]));

    //       await navigation.navigate('Medilux');


    //   const measurers = await getmeasurers(medidor);



    //   if (measurers !== null) {



    //     const monthsconsumption = await getmonthsconsumption(medidor);


    //     await EncryptedStorage.setItem("monthsconsumption", JSON.stringify(monthsconsumption));
    //     await EncryptedStorage.setItem("measurers", JSON.stringify(measurers));

    //     console.log("Months consumption log", monthsconsumption);
    //     console.log(measurers);




    //     await navigation.navigate('Medilux');







    //   }
    //   else {


    //     mediluxalert("Error", "Hubo un error en el servidor ");










    //   }



    //     }






    //     setEstado(0);

    //     setCargar("Crear usuario");




    //   }
    //   else {

    //     mediluxalert("Error", "Correo o contraseña incorrecta.Verifique e intente nuevamente");






    //   }



    // }
    // else {


    //   mediluxalert("Error", "Hubo un problema en el servidor");




    // }










    setEstado(0);

    setCargar("Entrar");

  }



  return (

    <View style={styles.formbutton}>





      <Textmedilux variant="headlineMedium" label='Ingresar' style={styles.medilux} ></Textmedilux>








      <FormBuilder
        control={control}
        setFocus={setFocus}
        formConfigArray={[
          {
            type: 'email',
            name: 'email',






            rules: {

              pattern: {
                value:
                  /[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/,
                message: 'Ingrese un email valido',
              },







              required: {
                value: 1,
                message: 'No puede dejar este campo vacio',
                contentStyle: styles.labelpago

              },
            },

















            textInputProps: {
              label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Correo*' ></Textmedilux>,
              activeOutlineColor: '#11D077',
              textColor: '#092D3D',
              underlineColor: '#11D077',
              outlineColor: '#11D077',
              contentStyle: styles.labelpago,
              theme: styles.texttheme



            },
          },
          {
            type: hidden,
            name: 'contrasena',
            rules: {
              required: {
                value: 1,
                message: 'No puede dejar este campo vacio',
              },
            },


            textInputProps: {

              label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Contraseña* ' ></Textmedilux>,
              right: <TextInput.Icon icon={icon} onPress={() => {

                edithidden();

              }} />,
              activeOutlineColor: '#11D077',
              textColor: '#092D3D',
              underlineColor: '#11D077',
              outlineColor: '#11D077',
              contentStyle: styles.labelpago,
              theme: styles.texttheme

            },
          },
        ]}
      />









      <Button mode='text' style={styles.logobutton} contentStyle={styles.logobutton} onPress={() => {
        navigation.navigate('Email');
      }}>   <Textmedilux variant="titleMedium" label='¿Olvidaste tu contraseña?' style={styles.contrasena} ></Textmedilux> <Textmedilux variant="titleMedium" style={styles.contrasena} ></Textmedilux></Button>





      <Button buttonColor="#11D077" style={styles.button} contentStyle={styles.contentbutton} labelStyle={styles.buttonlabel} mode="contained" disabled={"Cargando usuario" === cargar} onPress={handleSubmit((data: any) => {



        guardar(data);
      })} loading={estado}       >
        {cargar}
      </Button>
      {/* 

      <View style={styles.oauth}>





        <TouchableOpacity styles={styles.oauth}
          onPress={() => {

            // oauth();



          }} >

          <Image
            source={require('../../Imagenes/google.png')}
            style={styles.google}


          />

        </TouchableOpacity>




      </View> */}


      <Textmedilux style={styles.logo} variant="titleMedium" label="¿Todavía no tienes una cuenta?" ></Textmedilux>
      <Button mode='text' style={styles.pagobutton} onPress={() => {
        navigation.navigate('Crear');
      }}>   <Textmedilux onPress={() => console.log('Pressed')} style={styles.registrar} variant="titleMedium" label='Regístrate aquí' ></Textmedilux></Button>


      <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} />




    </View>


  );





}

const styles = StyleSheet.create({





  logo: {

    textAlign: 'center',


    color: '#092D3D',

    fontFamily: "Outfit-Medium",

    backgroundColor: "#FFFFFF"






  },


  contrasena: {




    color: '#092D3D',

    fontFamily: "Outfit-Light",







  },

  buttonlabel: {





    color: '#FFFFFF',


    fontFamily: "Outfit-Bold"





  },


  logobutton: {

    marginTop: "-2%",

    marginLeft: "-2.4%",


    width: "100%",

    justifyContent: "space-between"



  },

  pagobutton: {








  },

  labelpago: {
    fontFamily: "Outfit-Regular",

  },


  registrar: {



    textAlign: 'center',





    color: '#11D077',

    fontFamily: "Outfit-Bold"





  },


  medilux: {



    fontFamily: "Outfit-Medium",





    color: '#092D3D',




  },

  oauth: {

    justifyContent: "center",
    margin: "5%",
    flexDirection: "row",
    gap: 20


  },

  contentbutton: {


    height: 50,




  },

  google: {

    width: 50,
    height: 50,


  },



  button: {



    width: "100%",

    alignSelf: 'center',


    margin: "5%",









    borderRadius: 10,




  },




  texttheme: {

    colors: {

      background:

        "#FFFFFF"

    }


  },






  formbutton: {


    marginHorizontal: "6%",







  },



  text: {



    marginTop: 50,
  }
});


