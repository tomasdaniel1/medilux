import React from 'react';
import { View } from 'react-native';

import { Image } from 'react-native';
import { StyleSheet } from 'react-native';

import { FormBuilder } from 'react-native-paper-form-builder';
import { useForm } from 'react-hook-form';
import Textmedilux from '../Otros/Textmedilux.js';
import { Button, Text } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import { Portal } from 'react-native-paper';
import { Modal } from 'react-native-paper';

import EncryptedStorage from "react-native-encrypted-storage";

import { Checkbox } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from "react-native";
import CheckBox from 'react-native-check-box';


import Mediluxalert from '../Otros/Mediluxalert.js';




import { getcuentacontrato, getmeasurers, getotp, gettransaction, getuser, otp, postuser } from '../Apicuentacontrato.js';





export default function Formbutton({ cargar, estado, setCargar, setEstado, navigation }) {


  const { register, control, setFocus, handleSubmit } = useForm({
    defaultValues: {
      email: '',
      tipo: '',
      contrasena: '',
      cedula: '',
      pasaporte: '',
      ruc: '',
      nombre: '',
      apellido: '',
      razonsocial: '',
      direccion: '',
      telefono: ''







    },
    mode: 'onSubmit',
  });



  const [hide, setHide] = React.useState(0);
  const [label, setLabel] = React.useState("");
  const [content, setContent] = React.useState("");


  const [hidden, setHidden] = React.useState("password");

  const [icon, setIcon] = React.useState(require('../../Imagenes/see.png'));



  const [checkbox, setCheckbox] = React.useState("Disabled");






  const [tipo, setTipo] = React.useState(0);

  const [cedula, setCedula] = React.useState('none');

  const [pasaporte, setPasaporte] = React.useState('none');

  const [ruc, setRuc] = React.useState('none');

  const [tipolabel, setTipolabel] = React.useState('');

  const [tiposcreen, setTiposcreen] = React.useState('');

  const [pattern, setPattern] = React.useState('');

  const [patternlength, setPatternlength] = React.useState('');

  const [patternlogo, setPatternlogo] = React.useState('');


  const [screen, setScreen] = React.useState('');

  const [ptp, setPtp] = React.useState('');



  const [terminosestado, setTerminosestado] = React.useState("Disabled");




  const [cuentacontrato, setCuentacontrato] = React.useState(0);
  const [terminos, setTerminos] = React.useState(0);



  async function guardar() {


    setEstado(1);


    setCargar("Creando usuario");



    await EncryptedStorage.setItem("cuentacontrato", JSON.stringify(200032351237));


    await EncryptedStorage.setItem("measurer", JSON.stringify(2022240557));



    await EncryptedStorage.setItem("monthsconsumption", JSON.stringify([{ "mes": "Junio", "pago": "203.70" }, { "mes": "Mayo", "pago": "531.25" }, { "mes": "Abril", "pago": "507.23" }, { "mes": "Marzo", "pago": "517.20" }, { "mes": "Febrero", "pago": "662.68" }, { "mes": "Enero", "pago": "919.11" }]));
    await EncryptedStorage.setItem("measurers", JSON.stringify(["true", "121.5", "1.28", "121.0", "1.28"]));




    await navigation.navigate('Medilux');


    setEstado(0);


    setCargar("Crear usuario");


  }

  function generatehidden() {


    if (hidden === "password") {

      setHidden("text");
      setIcon(require('../../Imagenes/hidden.png'));


    }
    else {

      setHidden("password");
      setIcon(require('../../Imagenes/see.png'));


    }

  }






  function mediluxalert(label, content) {


    setHide(1);



    setLabel(label);
    setContent(content);


  }

  React.useEffect(() => {


    if (tipo === 1) {


      setTipolabel('cedula');
      setTiposcreen(' Cédula  *');
      setScreen("numeric");
      setPasaporte('none');
      setPatternlogo(/^([0-9]*)$/);
      setPattern(10);
      setPatternlength(10);
      setRuc('flex');
      setPtp("La cedula debe tener 10 caracteres numericos");

    }
    else if (tipo === 2) {

      setTipolabel('pasaporte');
      setTiposcreen('   Pasaporte*');
      setScreen("text");
      setPatternlogo(/^[a-zA-Z0-9_.-]*$/)
      setPattern(16);
      setPatternlength(4);

      setPtp("EL pasaporte debe tener entre 4 y 16 caracteres alfanumericos");


      setPasaporte('none');
      setRuc('flex');

    }
    else if (tipo === 3) {


      setTipolabel('ruc');
      setTiposcreen('   RUC*');
      setScreen("numeric");

      setPasaporte('flex');
      setPatternlogo(/^([0-9]*)$/);
      setPattern(13);
      setPatternlength(13);
      setPtp("El ruc debe tener 13  caracteres numericos");


      setRuc('none');


    }






    return () => {

    };
  }, [tipo]);


  async function guardarusuario(data) {


    setEstado(1);


    setCargar("Creando usuario");


    try {









      if (checkbox === "Enabled") {




        if (terminosestado === "Enabled") {




          const user = await getotp({ email: data.email, contrasena: data.contrasena });


          console.log(user);


          if (user.success) {


            // const token = user.data;

            // await EncryptedStorage.setItem("token", token);

            // const measurers = await getmeasurers(token);

            // console.log(measurers);

            await EncryptedStorage.setItem("medidor", JSON.stringify(data));

            // await EncryptedStorage.setItem("monthsconsumption", JSON.stringify([{ "mes": "Noviembre", "pago": "15.70" }, { "mes": "Octubre", "pago": "35.25" }, { "mes": "Septiembre", "pago": "31.23" }, { "mes": "Agosto", "pago": "36.20" }, { "mes": "Julio", "pago": "31.68" }, { "mes": "Junio", "pago": "29.11" }]));

            // await EncryptedStorage.setItem("medidor", JSON.stringify(data));


            // const transaction = await gettransaction(measurers.data[0], token);

            // await EncryptedStorage.setItem("transaction", JSON.stringify(transaction.data));






            await navigation.navigate('OTP');




          }
          else {

            mediluxalert("Error", user.message);


          }



        }
        else {




          mediluxalert("Error", "Tiene que aceptar la politica de privacidad ");

        }

        // if (data.contrasena === data.confirmarcontrasena) {





        //   const cuentacontrato = await getcuentacontrato(data.cuentacontrato);


        //   await EncryptedStorage.setItem("email", JSON.stringify(data.email));



        //   await EncryptedStorage.setItem("cuentacontrato", JSON.stringify(data.cuentacontrato));





        //   console.log("Cuenta contrato", cuentacontrato);




        //   if (cuentacontrato === null) {




        //     mediluxalert("Error", "El servidor esta tieniendo problemas.Intente en unos minutos");








        //   }
        //   else if (cuentacontrato.length === 0) {


        //     mediluxalert("Error", "El numero de cuenta contrato no existe");





        //   }
        //   else {


        //     console.log(cuentacontrato[0].medidor.toString().slice(2));

        //     const medidor = cuentacontrato[0].medidor.toString().slice(2);

        //     await EncryptedStorage.setItem("measurer", JSON.stringify(cuentacontrato[0].medidor));





        //     const measurers = await getmeasurers(medidor);




        //     if (measurers !== null) {


        //       const monthsconsumption = await getmonthsconsumption(medidor);







        //       await EncryptedStorage.setItem("monthsconsumption", JSON.stringify(monthsconsumption));
        //       await EncryptedStorage.setItem("measurers", JSON.stringify(measurers));


        //       console.log(monthsconsumption);
        //       console.log(measurers);





        //       const user = await postuser({

        //         "email": data.email,
        //         "contrasena": data.contrasena,
        //         "cuentacontrato": data.cuentacontrato







        //       });




        //       console.log("Crear usuario", user);



        //       if (user.response !== "Error") {





        //         await navigation.navigate('Medilux');





        //       }
        //       else {




        //         mediluxalert('Error', ` Hubo un problema al crear el usuario. ${user.code}`);



        //       }






        //     }
        //     else {


        //       mediluxalert("Error", "Hubo un error en el servidor ");










        //     }





        //   }
        // }
        // else {


        //   mediluxalert("Error", "Las contraseñas no coinciden");




        // }




      }
      else {

        mediluxalert("Error", "Tiene que aceptar los terminos y condiciones ");




      }


    }
    catch (error) {

      console.log(error);
      mediluxalert("Error", "Hubo un error en el servidor");


    }


    setEstado(0);

    setCargar("Crear usuario");

  }









  return (

    <View style={styles.formbutton}>





      <Textmedilux variant="headlineMedium" style={styles.medilux} label='Crear una cuenta' ></Textmedilux>




      {tipo === 0 && (




        <FormBuilder
          control={control}
          setFocus={setFocus}
          formConfigArray={[
            {
              type: 'email',
              name: 'email',







              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/,
                  message: 'Ingrese un email valido',
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Correo*' ></Textmedilux>,







                keyboardType: "email",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme


              },
            },

            {
              type: hidden,
              name: 'contrasena',
              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /(?=.*[A-Za-z])(?=.*\d)(?=.*[@!%*#?&.-])[A-Za-z\d@!%*#?&.-]/,
                  message: 'La contrasena debe tener un minimo de 8 hasta 12 caracteres,una letra,un numero y un caracter especial(@$!%*#?&)',
                },
                maxLength: {
                  value: 12,
                  message: 'La contrasena debe tener  hasta 12 caracteres',
                },
              },
              textInputProps: {

                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Contraseña*' ></Textmedilux>,
                right: <TextInput.Icon icon={icon} onPress={() => {

                  generatehidden();

                }} />,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme





















              },









            },



            {
              name: 'tipo',
              type: 'select',
              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Seleccione un tipo de documento*' ></Textmedilux>,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,
                ...register('tipo', {

                  onChange: text => {

                    setTipo(Number(text.target.value));
                  },

                })
              },
              options: [
                {
                  label: 'Cédula',
                  value: 1,
                },
                {
                  label: 'Pasaporte',
                  value: 2,
                },
                {
                  label: 'RUC',
                  value: 3,
                }
              ],
            },

            {
              type: 'text',
              name: 'direccion',

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                maxLength: {
                  value: 50,
                  message: 'Este campo puede tener hasta 50 caracteres',
                },


              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Dirección*' ></Textmedilux>,

                keyboardType: "text",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,













              },

















            },
            {
              type: 'text',
              name: 'telefono',

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /^([0-9]*)$/,
                  message: 'Ingrese un telefono valido',
                },
                maxLength: {
                  value: 10,
                  message: 'Este campo debe tener 10 caracteres',
                },
                minLength: {
                  value: 10,
                  message: 'Este campo debe tener 10 caracteres',
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Teléfono*' ></Textmedilux>,

                keyboardType: "numeric",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,













              },

















            },


          ]}
        />




      )}







      {tipo === 1 && (




        <FormBuilder
          control={control}
          setFocus={setFocus}
          formConfigArray={[
            {
              type: 'email',
              name: 'email',







              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/,
                  message: 'Ingrese un email valido',
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Correo*' ></Textmedilux>,







                keyboardType: "email",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme


              },
            },

            {
              type: hidden,
              name: 'contrasena',
              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /(?=.*[A-Za-z])(?=.*\d)(?=.*[@!%*#?&.-])[A-Za-z\d@!%*#?&.-]/,
                  message: 'La contrasena debe tener un minimo de 8 hasta 12 caracteres,una letra,un numero y un caracter especial(@$!%*#?&)',
                },
                maxLength: {
                  value: 12,
                  message: 'La contrasena debe tener  hasta 12 caracteres',
                },
              },
              textInputProps: {

                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Contraseña*' ></Textmedilux>,
                right: <TextInput.Icon icon={icon} onPress={() => {

                  generatehidden();

                }} />,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme





















              },









            },



            {
              name: 'tipo',
              type: 'select',
              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Seleccione un tipo de documento*' ></Textmedilux>,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,
                ...register('tipo', {

                  onChange: text => {

                    setTipo(Number(text.target.value));
                  },

                })
              },
              options: [
                {
                  label: 'Cédula',
                  value: 1,
                },
                {
                  label: 'Pasaporte',
                  value: 2,
                },
                {
                  label: 'RUC',
                  value: 3,
                }
              ],
            },
            {
              type: 'text',
              name: tipolabel,

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },

                pattern: {
                  value: patternlogo
                  ,
                  message: ptp,
                },
                maxLength: {
                  value: pattern,
                  message: ptp,
                },
                minLength: {
                  value: patternlength,
                  message: ptp,
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label={tiposcreen}></Textmedilux>,

                keyboardType: screen,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,

                disabled: tipo === 0











              },

















            },
            {
              type: 'text',
              name: 'direccion',

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                maxLength: {
                  value: 50,
                  message: 'Este campo puede tener hasta 50 caracteres',
                },


              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Dirección*' ></Textmedilux>,

                keyboardType: "text",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,













              },

















            },
            {
              type: 'text',
              name: 'telefono',

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /^([0-9]*)$/,
                  message: 'Ingrese un telefono valido',
                },
                maxLength: {
                  value: 10,
                  message: 'Este campo debe tener 10 caracteres',
                },
                minLength: {
                  value: 10,
                  message: 'Este campo debe tener 10 caracteres',
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Teléfono*' ></Textmedilux>,

                keyboardType: "numeric",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,













              },

















            },

            {
              type: 'text',
              name: 'nombre',

              rules: {
                required: {
                  value: ruc === 'flex',
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /^[a-zA-Z\u00C0-\u017F\s]+$/,
                  message: 'Ingrese un nombre valido',
                },
                maxLength: {
                  value: 30,
                  message: 'Este campo puede tener hasta 30 caracteres',
                },


              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Nombre*' ></Textmedilux>,

                keyboardType: "text",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,


                style: { display: ruc }











              },

















            },
            {
              type: 'text',
              name: 'apellido',

              rules: {
                required: {
                  value: ruc === 'flex',
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /^[a-zA-Z\u00C0-\u017F\s]+$/,
                  message: 'Ingrese un apellido valido',
                },
                maxLength: {
                  value: 30,
                  message: 'Este campo puede tener hasta 30 caracteres',
                },

              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Apellido*' ></Textmedilux>,

                keyboardType: "text",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,


                style: { display: ruc }











              },

















            },

          ]}
        />




      )}



      {tipo === 2 && (




        <FormBuilder
          control={control}
          setFocus={setFocus}
          formConfigArray={[
            {
              type: 'email',
              name: 'email',







              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/,
                  message: 'Ingrese un email valido',
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Correo*' ></Textmedilux>,







                keyboardType: "email",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme


              },
            },

            {
              type: hidden,
              name: 'contrasena',
              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /(?=.*[A-Za-z])(?=.*\d)(?=.*[@!%*#?&.-])[A-Za-z\d@!%*#?&.-]/,
                  message: 'La contrasena debe tener un minimo de 8 hasta 12 caracteres,una letra,un numero y un caracter especial(@$!%*#?&)',
                },
                maxLength: {
                  value: 12,
                  message: 'La contrasena debe tener  hasta 12 caracteres',
                },
              },
              textInputProps: {

                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Contraseña*' ></Textmedilux>,
                right: <TextInput.Icon icon={icon} onPress={() => {

                  generatehidden();

                }} />,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme





















              },









            },



            {
              name: 'tipo',
              type: 'select',
              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Seleccione un tipo de documento*' ></Textmedilux>,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,
                ...register('tipo', {

                  onChange: text => {

                    setTipo(Number(text.target.value));
                  },

                })
              },
              options: [
                {
                  label: 'Cédula',
                  value: 1,
                },
                {
                  label: 'Pasaporte',
                  value: 2,
                },
                {
                  label: 'RUC',
                  value: 3,
                }
              ],
            },
            {
              type: 'text',
              name: tipolabel,

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },

                pattern: {
                  value: patternlogo
                  ,
                  message: ptp,
                },
                maxLength: {
                  value: pattern,
                  message: ptp,
                },
                minLength: {
                  value: patternlength,
                  message: ptp,
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label={tiposcreen}></Textmedilux>,

                keyboardType: screen,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,

                disabled: tipo === 0











              },

















            },
            {
              type: 'text',
              name: 'direccion',

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                maxLength: {
                  value: 50,
                  message: 'Este campo puede tener hasta 50 caracteres',
                },


              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Dirección*' ></Textmedilux>,

                keyboardType: "text",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,













              },

















            },
            {
              type: 'text',
              name: 'telefono',

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /^([0-9]*)$/,
                  message: 'Ingrese un telefono valido',
                },
                maxLength: {
                  value: 10,
                  message: 'Este campo debe tener 10 caracteres',
                },
                minLength: {
                  value: 10,
                  message: 'Este campo debe tener 10 caracteres',
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Teléfono*' ></Textmedilux>,

                keyboardType: "numeric",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,













              },

















            },

            {
              type: 'text',
              name: 'nombre',

              rules: {
                required: {
                  value: ruc === 'flex',
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /^[a-zA-Z\u00C0-\u017F\s]+$/,
                  message: 'Ingrese un nombre valido',
                },
                maxLength: {
                  value: 30,
                  message: 'Este campo puede tener hasta 30 caracteres',
                },


              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Nombre*' ></Textmedilux>,

                keyboardType: "text",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,


                style: { display: ruc }











              },

















            },
            {
              type: 'text',
              name: 'apellido',

              rules: {
                required: {
                  value: ruc === 'flex',
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /^[a-zA-Z\u00C0-\u017F\s]+$/,
                  message: 'Ingrese un apellido valido',
                },
                maxLength: {
                  value: 30,
                  message: 'Este campo puede tener hasta 30 caracteres',
                },

              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Apellido*' ></Textmedilux>,

                keyboardType: "text",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,


                style: { display: ruc }











              },

















            },

          ]}
        />




      )}



      {tipo === 3 && (




        <FormBuilder
          control={control}
          setFocus={setFocus}
          formConfigArray={[
            {
              type: 'email',
              name: 'email',







              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/,
                  message: 'Ingrese un email valido',
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Correo*' ></Textmedilux>,







                keyboardType: "email",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme


              },
            },

            {
              type: hidden,
              name: 'contrasena',
              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /(?=.*[A-Za-z])(?=.*\d)(?=.*[@!%*#?&.-])[A-Za-z\d@!%*#?&.-]/,
                  message: 'La contrasena debe tener un minimo de 8 hasta 12 caracteres,una letra,un numero y un caracter especial(@$!%*#?&)',
                },
                maxLength: {
                  value: 12,
                  message: 'La contrasena debe tener  hasta 12 caracteres',
                },
              },
              textInputProps: {

                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Contraseña*' ></Textmedilux>,
                right: <TextInput.Icon icon={icon} onPress={() => {

                  generatehidden();

                }} />,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme





















              },









            },



            {
              name: 'tipo',
              type: 'select',
              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Seleccione un tipo de documento*' ></Textmedilux>,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,
                ...register('tipo', {

                  onChange: text => {

                    setTipo(Number(text.target.value));
                  },

                })
              },
              options: [
                {
                  label: 'Cédula',
                  value: 1,
                },
                {
                  label: 'Pasaporte',
                  value: 2,
                },
                {
                  label: 'RUC',
                  value: 3,
                }
              ],
            },
            {
              type: 'text',
              name: tipolabel,

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },

                pattern: {
                  value: patternlogo
                  ,
                  message: ptp,
                },
                maxLength: {
                  value: pattern,
                  message: ptp,
                },
                minLength: {
                  value: patternlength,
                  message: ptp,
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label={tiposcreen}></Textmedilux>,

                keyboardType: screen,
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,

                disabled: tipo === 0











              },

















            },
            {
              type: 'text',
              name: 'direccion',

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                maxLength: {
                  value: 50,
                  message: 'Este campo puede tener hasta 50 caracteres',
                },


              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Direccion*' ></Textmedilux>,

                keyboardType: "text",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,













              },

















            },
            {
              type: 'text',
              name: 'telefono',

              rules: {
                required: {
                  value: 1,
                  message: 'No puede dejar este campo vacio',
                },
                pattern: {
                  value:
                    /^([0-9]*)$/,
                  message: 'Ingrese un telefono valido',
                },
                maxLength: {
                  value: 10,
                  message: 'Este campo debe tener 10 caracteres',
                },
                minLength: {
                  value: 10,
                  message: 'Este campo debe tener 10 caracteres',
                },



              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Teléfono*' ></Textmedilux>,

                keyboardType: "numeric",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,













              },

















            },
            {
              type: 'text',
              name: 'razonsocial',

              rules: {
                required: {
                  value: pasaporte === 'flex',
                  message: 'No puede dejar este campo vacio',
                },

                maxLength: {
                  value: 30,
                  message: 'Este campo puede tener hasta 30 caracteres',
                },


              },
              textInputProps: {
                label: <Textmedilux style={styles.logo} variant="titleMedium" label='   Razonsocial*' ></Textmedilux>,

                keyboardType: "text",
                activeOutlineColor: '#11D077',
                textColor: '#092D3D',
                underlineColor: '#11D077',
                outlineColor: '#11D077',
                contentStyle: styles.labelpago,
                theme: styles.texttheme,


                style: { display: pasaporte }











              },

















            },


          ]}
        />




      )}







      <View style={styles.pagobutton}>


        <CheckBox
          checkBoxColor="#11D077"
          rightTextStyle={styles.checkboxlabel}
          style={styles.checkbox}
          onClick={() => {

            setCuentacontrato(1);






          }}
          isChecked={checkbox === "Enabled"}
          rightText={"Si acepto los términos y condiciones"}
        />


        <CheckBox
          checkBoxColor="#11D077"
          rightTextStyle={styles.checkboxlabel}
          style={styles.checkbox}
          onClick={() => {

            setTerminos(1);




          }}
          isChecked={terminosestado === "Enabled"}
          rightText={"Si acepto las políticas de protección de datos"}
        />










        <Button buttonColor="#11D077" disabled={"Creando usuario" === cargar} onPress={handleSubmit((data: any) => {


          console.log(data);
          guardarusuario(data);
          // guardar();
        })} style={styles.button} contentStyle={styles.button} labelStyle={styles.buttonlabel} mode="contained" loading={estado} >
          {cargar}
        </Button>

      </View>

      {/*  */}


      <Portal>
        <Modal visible={cuentacontrato} style={styles.modallabel} onDismiss={() => setCuentacontrato(0)}  >


          <View style={styles.modal} >


            <ScrollView style={styles.modalscroll} >

              <Textmedilux style={styles.logo} variant="titleMedium" label='Términos y condiciones
RELACIÓN CONTRACTUAL
la compañía DESARROLLO E INTEGRACIÓN DE PROCESOS DIGITALES
SODIG S.A. (en adelante “SODIG S.A.”) es una compañía privada constituida y
existente bajo las leyes ecuatorianas, que ha desarrollado una infraestructura tecnológica
propia para dispositivos móviles y paginas web a la cual en adelante se la denominara
“MEDILUX”, la cual tiene contenido y servicios puestos a su disposición.
Los presentes términos y condiciones de uso en adelante, “Condiciones” regulan la
relación contractual entre usted en adelante, “Usuario” con “SODIG S.A.”, que incluye
el acceso o uso que el Usuario haga de “MEDILUX” y sus servicios, sea como persona
natural o jurídica, desde cualquier lugar de la República del Ecuador. El Usuario al
aceptar estas Condiciones estará sujeto a todas las políticas y demás principios que rigen
a SODIG S.A., y que son incorporados al presente por referencia.
ANTES DE ACCEDER O USAR LOS SERVICIOS LEA ESTAS CONDICIONES
DETENIDAMENTE.
Mediante su registro y/o acceso a “MEDILUX” y/o la Plataforma y uso a los Servicios
puestos a disposición por SODIG S.A., el Usuario acepta y manifiesta su conformidad
con los mismos, así como también acuerda vincularse jurídicamente bajo estas
Condiciones y Políticas de Privacidad y Tratamiento de Datos Personales establecidas
por SODIG S.A. Si Usted no acepta estas Condiciones, no podrá acceder o usar los
Servicios.
SODIG S.A. se reserva el derecho de terminar estas Condiciones o cualquiera de los
Servicios y/o “Servicios de Terceros” (más adelante definidos) respecto del Usuario o, en
general, dejar de ofrecer o denegar el acceso a los Servicios y/o “Servicios de Terceros”,
en cualquier momento, en caso de que exista algún incumplimiento a las Condiciones o
cualquier política establecida por SODIG S.A.
SODIG S.A. podrá modificar las Condiciones relativas a los Servicios y/o “Servicios de
Terceros” cuando lo considere oportuno. Las modificaciones serán efectivas después de
la publicación por parte de SODIG S.A. de dichas Condiciones actualizadas en la
siguiente dirección electrónica www.medlix.ec o las políticas modificadas o condiciones
suplementarias sobre el Servicio aplicable. Su acceso o uso continuo de los Servicios
después de dicha publicación constituye su consentimiento a vincularse bajo estas
Condiciones y sus modificaciones.
CLÁUSULA PRIMERA: SERVICIOS DE SODIG S.A.
1.1 La Plataforma y “MEDILUX”, son herramientas tecnológicas, que ofrecen un
servicio en línea, los cuales constituyen un canal externo para la compra, venta,
contratación de servicios y pago de obligaciones del Usuario con proveedores
independientes y/o Entidades Públicas. SODIG S.A. a través de la Plataforma y
“MEDILUX” le permite al Usuario acceder a un catálogo de servicios que ofertan los
Proveedores Independientes. SODIG S.A. a través de la Plataforma y “MEDILUX”
también ofrece un canal externo para que el Usuario pueda acceder a la revisión e
instruir el pago de obligaciones que registra ante Entidades Públicas, estas 
obligaciones podrán incluir, pero no limitarse a: el pago de planillas de consumo
eléctrico y demás obligaciones relacionadas al uso y mantenimiento de los servicios
eléctricos. Tanto los servicios que ofertan los Proveedores Independientes, así como
también las obligaciones que registra el Usuario ante las Entidades Públicas se
denominarán en el presente documento como “Servicios de Terceros”, puesto que no
son provistos por SODIG S.A.
EN TAL SENTIDO, USTED RECONOCE Y ACEPTA QUE SODIG S.A. NO PRESTA
LOS SERVICIOS DE COBRO DE OBLIGACIONES, DEFINIDOS EN ESTE
DOCUMENTO COMO LOS “SERVICIOS DE TERCEROS” Y QUE DICHOS
SERVICIOS SE PRESTAN POR PROVEEDORES INDEPENDIENTES O
ENTIDADES PÚBLICAS, QUE NO TIENEN UNA RELACIÓN LABORAL O
CONTROL CON SODIG S.A. EL USUARIO DECLARA COMPRENDER QUE LOS
PROVEEDORES INDEPENDIENTES Y LAS ENTIDADES PÚBLICAS, PRESTAN
SERVICIOS Y/O EMITEN OBLIGACIONES POR CUENTA Y RIESGO PROPIO Y,
POR LO TANTO, EL USUARIO LIBERA A SODIG S.A. DE CUALQUIER
RESPONSABILIDAD QUE PUDIERA SURGIR AL MOMENTO DE REALIZAR LA
CONTRATACIÓN Y/O PAGO DE LOS SERVICIOS U OBLIGACIONES QUE
REALICE EL USUARIO.
1.2 El acceso a “MEDILUX” y a la Plataforma es gratuito. Se excluyen los costos de
conexión a través de la red de telecomunicaciones suministrada por el proveedor de
acceso contratado (ISP) directamente por el Usuario. SODIG S.A. cobrará únicamente
los valores correspondientes al “Costo de Procesamiento” (más adelante definido) o los
servicios que preste directamente SODIG S.A.
CLÁUSULA SEGUNDA: REGISTRO
2.1 A los efectos de adquirir la condición de Usuario Registrado de “MEDILUX” y/o la
Plataforma según se describe en esta sección, el Usuario deberá ser mayor de edad según
la legislación ecuatoriana, completar toda la información solicitada en el formulario de
registro, brindar su consentimiento para el tratamiento de su información personal,
aceptar la Política de Privacidad y Tratamiento de Datos Personales, y aceptar las
presentes Condiciones.
2.2 Para el acceso al catálogo de Servicios y a la posibilidad de contratar los “Servicios
de Terceros” mediante “MEDILUX” y/o la Plataforma, será necesario además de lo
establecido en el numeral 2.1 anterior, contar con acceso a internet a través de un
Smartphone con sistema operativo IOS o Android, y completar todos los campos del
formulario de inscripción correspondiente a los Usuarios con datos válidos.
2.3 El Usuario deberá registrarse, suministrando ciertos datos personales completos,
exactos y vigentes. SODIG S.A. podrá solicitar, recabar y almacenar la siguiente
información personal: apodo o seudónimo (nombre de Titular de los datos) para operar
en “MEDILUX” y/o Plataforma, nombre y apellido, número de documento de
identificación válida, información de contacto (como número de teléfono de domicilio,
dirección de e-mail, número de teléfono móvil), información de la cuenta contrato, y, por
lo menos un método de pago válido (bien una tarjeta de crédito, una transferencia bancaria 
o cualquier otro método de pago definido y aceptado por SODIG S.A.) (en adelante, la
“Información Personal”).
2.4 El Usuario Registrado tendrá la obligación de verificar que toda la información
proporcionada a SODIG S.A., sea exacta, precisa, verdadera y válida. Asimismo, el
Usuario Registrado asumirá el compromiso de actualizar dicha información cada vez que
esta se modifique. El Usuario Registrado deberá garantizar y responder, en cualquier caso,
por la veracidad, exactitud, vigencia y autenticidad de la información personal puesta a
disposición de SODIG S.A. Por tanto, SODIG S.A. no se responsabiliza por la certeza o
veracidad de la información personal otorgada por el Usuario Registrado.
2.5 Una vez efectuado el registro, SODIG S.A. otorgará al Usuario Registrado una cuenta
personal para acceder con la contraseña de seguridad elegida por el Usuario (en adelante,
la “Cuenta”). El Usuario Registrado es responsable de toda la actividad que se genere en
su Cuenta y se compromete y acepta como responsabilidad propia en mantener, en todo
momento, de forma segura y secreta, el nombre de usuario y la contraseña de su Cuenta.
La Cuenta es personal, única e intransferible, y está prohibido que un mismo Usuario
Registrado registre o posea más de una Cuenta. En caso de que SODIG S.A. detecte
distintas cuentas que contengan datos coincidentes o relacionados, podrá cancelarlas,
suspenderlas o inhabilitarlas.
2.6 SODIG S.A. se reserva el derecho de solicitar algún comprobante y/o dato adicional
a efectos de corroborar la Información Personal, y de suspender temporal y/o
definitivamente al Usuario Registrado cuyos datos no hayan podido ser confirmados.
SODIG S.A. no se responsabiliza por la certeza de los datos consignados en el registro.
El Usuario Registrado garantiza y responde, en cualquier caso, por la veracidad,
exactitud, vigencia y autenticidad de su Información Personal. La Información Personal
que el Usuario Registrado proporcione se integrará en una base de datos personales de la
que es responsable SODIG S.A. Para más información consultar la “Política de
Privacidad y Tratamiento de Datos Personales”.
2.7 El Usuario es consciente y acepta que SODIG S.A. pueda enviar a su número de
teléfono móvil de contacto, mensajes de texto informativos (SMS) como parte de la
actividad comercial normal del uso de los Servicios. El Usuario podrá solicitar la no
recepción de mensajes de texto informativos (SMS) de SODIG S.A. en cualquier
momento enviando un correo electrónico a notificaciones@medilux.ec indicando que no
desea recibir dichos mensajes, junto con el número de teléfono del dispositivo móvil en
el que recibe los mensajes.
CLÁUSULA TERCERA: CONDICIONES DE USO DE LOS SERVICIOS DE
TERCEROS
3.1 Los “Servicios de Terceros” contratados a través de “MEDILUX” y/o la Plataforma,
son dirigidos exclusivamente para el uso del Usuario Registrado.
3.2. La solicitud de cualquier “Servicio de Terceros” a través de “MEDILUX” y/o
Plataforma, podrá abonarse a través de los procesos y medios de pago que determine
SODIG S.A., lo cual podrá incluir la participación de un procesador de pagos. El
procesamiento de pagos es un servicio externo que estará sujeto a todos los Términos y
Condiciones y Políticas de Privacidad y Tratamiento de Datos Personales de SODIG S.A. 
y aquellos que determine el procesador de pagos correspondiente. Por tanto, el Usuario
entiende y acepta que SODIG S.A. no será responsable por ningún error u omisión
cometido por el procesador de pagos, al efectuar cualquier pago instruido por el Usuario.
3.3 El Usuario conoce y acepta que SODIG S.A. puede aplicar ciertos cargos por el uso
de los Servicios (en adelante, el “Costo de Procesamiento”). SODIG S.A. se reserva el
derecho de incluir, modificar o eliminar el Costo de Procesamiento en cualquier momento
durante la vigencia de estas Condiciones. En los casos que aplique, el Usuario verá
reflejado el monto del Costo de Procesamiento y su respectivo detalle previamente a la
confirmación de la solicitud del Servicio. Los cargos por el Costo de Procesamiento
pagados por el Usuario son definitivos y no reembolsables, a menos de que SODIG S.A.
determine lo contrario.
3.4. El Usuario Registrado conoce y entiende que “MEDILUX” y/o la Plataforma es un
canal externo que contiene un catálogo de servicios ofrecidos por distintos Proveedores
Independientes, así como también servicios para el pago de obligaciones con Entidades
Públicas, las cuales, a su vez, utilizan “MEDILUX” y/o Plataforma como medios externos
para ofrecer sus servicios y/o detallar las obligaciones que mantenga el Usuario en las
Entidades Públicas. En tal sentido, el catálogo de servicios permite al Usuario Registrado,
mediante el uso de “MEDILUX” y/o la Plataforma, encontrar, adquirir y dar aviso de
manera inmediata que desea recibir un producto y/o “Servicio de Terceros” ofertado por
los Proveedores Independientes y/o cancelar las obligaciones que mantiene con las
Entidades Públicas.
3.5. SODIG S.A. se reserva la facultad de bloquear y/o suspender el acceso del Usuario
Registrado al catálogo de servicios, por cuestiones de seguridad ante posibilidades de
fraude, estafa, uso de datos ajenos, o algún otro supuesto que se desprenda como
infracción de las presentes Condiciones de uso de “MEDILUX” y/o la Plataforma o de la
Ley ecuatoriana. Dicha situación será debidamente comunicada al Usuario Registrado.
3.6. SODIG S.A. podrá rechazar y/o suspender cualquier solicitud de los Usuarios
Registrados, en caso de identificar una infracción a las presentes Condiciones.
3.7. Cualquier cambio en los “Servicios de Terceros” será informado al Usuario
Registrado al número de teléfono de contacto y/o al correo electrónico provisto por el
Usuario Registrado en su Cuenta.
3.8. El Proveedor Independiente cumplirá el “Servicio de Terceros” solicitado en
“MEDILUX” y/o la Plataforma de SODIG S.A., a favor de la persona que se encuentre
en los detalles de entrega, siendo el Usuario Registrado el único responsable por la
veracidad de los datos indicados.
3.9. Se encuentra totalmente prohibido utilizar “MEDILUX” y/o la Plataforma para
contratar los “Servicios de Terceros” para realizar cualquier acto con fines ilícitos,
ilegales, contrarios a lo establecido en las presentes Condiciones, a la buena fe y al orden
público, lesivos de los derechos e intereses de terceros.
CLÁUSULA CUARTA: PROCEDIMIENTO Y USO DE LOS “SERVICIOS DE
TERCEROS”
4.1. El Usuario Registrado podrá efectuar en “MEDILUX” y/o la Plataforma la búsqueda
y/o solicitud de contratación o pago de los “Servicios de Terceros”. SODIG S.A.
informará sobre todos los servicios disponibles por los Proveedores Independientes, de
acuerdo con las categorías de búsqueda realizada por el Usuario Registrado.
4.2 Cuando el Usuario Registrado haya definido y seleccionado el producto o Servicio de
Terceros que requiere o que desea pagar, se pondrán a su disposición: las características,
condiciones y valor total del Servicio de Terceros, según la información del Proveedor
Independiente o de la Entidad Pública. Además, se detallará el “Costo de Procesamiento”
de SODIG S.A. y adicionalmente los impuestos correspondientes. El Usuario Registrado
tras revisar dicha información deberá validar el requerimiento y seleccionar el método de
pago a través de los medios de pago disponibles en “MEDILUX” y/o la Plataforma según
lo definido por SODIG S.A.
4.3 Seleccionado el método de pago y verificando el precio final, el Usuario Registrado
deberá confirmar la solicitud de servicio. Tras esta confirmación, SODIG S.A.
comunicará con el requerimiento al Proveedor Independiente para que acepte o niegue la
solicitud conforme la disponibilidad del servicio. En el caso de que el servicio solicitado
por el Usuario Registrado sea negado por el Proveedor Independiente, no se realizará
cobro por valores de Costo de Procesamiento o valores por los Servicios de Terceros y se
informará de la negativa del Proveedor Independiente al Usuario Registrado.
4.4 Durante el tiempo que transcurra entre la confirmación del requerimiento y la entrega
del producto o Servicio de Terceros en los establecimientos de los Proveedores
Independientes, el Usuario podrá comunicarse con SODIG S.A. a través de “MEDILUX”
y/o la Plataforma para efectuar preguntas, presentar quejas, entre otros casos, en los cuales
recibirá una respuesta en el menor tiempo posible.
4.5 Dependiendo del servicio seleccionado SODIG S.A., se reserva el derecho de
establecer las políticas de modificación, sin perjuicio del cambio de tarifa que tal
modificación pueda ocasionar. SODIG S.A. se reserva la facultad de realizar recargos por
los cambios o modificaciones a los servicios requeridos conforme la cláusula de
cancelación y modificación de estas Condiciones.
4.6 Queda establecido, entendido y aceptado por el Usuario que SODIG S.A. mediante
“MEDILUX” y/o la Plataforma actúa como un canal externo para la compra/venta/pago
de obligaciones o de servicios que brindan los Proveedores Independientes, y empleará
sus mejores esfuerzos por canalizar todas las inquietudes e inconvenientes que surjan
entre Usuarios Registrados y Proveedores Independientes.
CLÁUSULA QUINTA: CONDICIONES DE USO DE SERVICIOS PARA EL
PAGO DE OBLIGACIONES
5.1 El consumo eléctrico y cualquier otro servicio correspondiente a Entidades Públicas
que se encuentre en el catálogo de servicios de “MEDILUX” y/o la Plataforma, así como
el servicio de pago de obligaciones, son dirigidos exclusivamente para el uso del Usuario
Registrado de SODIG S.A.
5.2 El Usuario Registrado podrá revisar previamente a confirmar el pago de los valores
correspondientes a los servicios requeridos, el resumen de pagos que contiene el detalle 
de servicios seleccionados y el valor de cada uno, así como los valores del “Costo de
Procesamiento” y los impuestos relacionados a los servicios de SODIG S.A.
5.3 Con la aceptación del precio final, el Usuario Registrado deberá seleccionar el método
de pago y confirmar la cancelación de la obligación. Tras esta confirmación, SODIG S.A.
remitirá el comprobante de cancelación de la obligación al email señalado por el Usuario
Registrado y la factura únicamente por el valor del Costo de Procesamiento.
5.4 El Usuario comprende y acepta que SODIG S.A. es un canal externo independiente,
por tanto, no es responsable por los valores a pagar determinados por las Entidades
Públicas que se reflejan en “MEDILUX” y/o la Plataforma, los cuales son tomados de la
información publicada por las Entidades Públicas, en relación con los datos
proporcionados por el Usuario Registrado. En tal virtud, el Usuario acepta que SODIG
S.A. no tiene responsabilidad alguna y, en consecuencia, no responderá por valores
cancelados que estén sujetos a reclamos o apelaciones legales por errores cometidos por
las Entidades Públicas.
5.5 El Usuario declara que entiende y acepta de manera expresa que SODIG S.A. no es
depositario ni recaudador de las obligaciones con Entidades Públicas, sino que brinda al
Usuario a través de “MEDILUX” y/o la Plataforma un servicio para cancelar en su
representación dichas obligaciones, mediante la ejecución de sus procesos internos y
métodos establecidos para el efecto.
5.6 De conformidad con lo previsto en esta cláusula, todos los valores relativos al pago
de obligaciones con Entidades Públicas, no estarán sujetos a reembolso ni reclamos por
pagos indebidos mediante el uso de los Servicios de SODIG S.A. Cualquier controversia
o reclamo deberá ser dirigido directamente a la Entidad Pública responsable de emitir las
obligaciones respectivas.
5.7 El Usuario comprende y acepta que en caso de pagos indebidos reconocidos mediante
resolución o cualquier acto administrativo de autoridad competente, válido según la
normativa ecuatoriana, o cualquier pago de obligaciones que haya generado cargos al
Usuario, no estará sujeto a reembolso o reclamo alguno en contra de SODIG S.A. por
parte del Usuario.
5.8 Las disposiciones contenidas en la cláusula octava de estas Condiciones con respecto
a las políticas de cancelación y penalidad, no son aplicables a los servicios de pago de
obligaciones y ningún otro servicio contenido en esta cláusula.
5.9. El Usuario comprende y reconoce que los pagos realizados por los servicios y los
Costos de Procesamiento pueden estar sujetos al pago del impuesto a la salida de divisas
(“ISD”) de conformidad con la normativa tributaria vigente.
CLÁUSULA SEXTA: CONDICIONES DE PAGO Y FACTURACIÓN
6.1. Los Costos de Procesamiento aplicables al “Servicio de Terceros” los fijará SODIG
S.A. conforme a los servicios que se encuentren activos al momento en que el Usuario
Registrado haga uso de “MEDILUX” y/o la Plataforma. El Usuario Registrado acepta y
reconoce el derecho de SODIG S.A. de modificar los Costos de Procesamiento. Estos
Costos de Procesamiento serán cobrados directamente por SODIG S.A. a través de
“MEDILUX” y/o la Plataforma, estos también estarán siempre acorde con los rango y 
valores establecidos por la normativa legal vigente y serán siempre notificados al usuario
mediante correo electrónico o por medio de “MEDILUX” y/o la Plataforma. El cobro se
realizará online y de forma automática con cargo al método de pago seleccionado por el
Usuario Registrado.
6.2 Con el método de pago registrado mediante “MEDILUX” y/o la Plataforma, el
Usuario Registrado podrá efectuar el pago del servicio requerido, incluyendo las
comisiones, cargos y los impuestos aplicables, en la misma moneda en que los “Servicios
de Terceros” son presentados por los Proveedores Independientes.
6.3 Finalizado y cumplido el “Servicio de Terceros”, SODIG S.A. enviará un correo
electrónico al Usuario Registrado con el resumen de los servicios realizados, un detalle
del servicio efectivamente entregado, descripción de la fecha y hora de entrega y el total
del monto abonado con los impuestos incluidos (en adelante, el “Resumen”). El Resumen
no tendrá validez para efectos tributarios ni hará las veces de un comprobante de pago.
6.4 SODIG S.A. facilitará la dirección de correo electrónico y otros datos del Usuario,
conforme la Política de Privacidad de Datos, al Proveedor Independiente para que éste
remita la factura correspondiente por el servicio prestado. Cualquier reclamo o solicitud
de cambio de datos e información que corresponda a la factura, deberá ser tramitado por
el Usuario directamente con el Proveedor Independiente correspondiente.
6.5. Los Costos de Procesamiento cobrados en “MEDILUX” y/o la Plataforma, así como
cualquier otra tarifa o cargo cobrado por los Servicios de SODIG S.A., no serán
reembolsables.
6.6. El titular de la tarjeta de crédito o débito o cuenta bancaria o cualquier otro método
de pago definido y aceptado por SODIG S.A., es el responsable por los datos consignados
al momento de la solicitud del “Servicio de Terceros” y es el único obligado al pago frente
al emisor del medio de pago o entidad financiera. Cualquier desacuerdo deberá ser
comunicado directamente al emisor del medio de pago o entidad financiera, según
corresponda, de conformidad con lo dispuesto por en la normativa vigente.
6.7. SODIG S.A. se reserva el derecho de tomar las medidas judiciales y extrajudiciales
que estime pertinentes para obtener el pago del monto debido que le pueda adeudar el
Usuario Registrado. SODIG S.A. se reserva el derecho de modificar, cambiar, agregar, o
eliminar los Costos de Procesamiento, en cualquier momento, lo cual será debidamente
notificado al Usuario Registrado.
6.8 El Usuario reconoce y entiende que SODIG S.A. no es el procesador del cobro de los
servicios de “Terceros Independientes” ni de los servicios que ofrece. Todo pago se
realiza a través de un Botón de Pagos independiente. Es este Botón de Pagos el que
procesa la transacción de cobro y pago y el que podrá almacenar los datos de los medios
de pago o cuentas bancarias empleados por el Usuario. En tal sentido, SODIG S.A. no
almacena datos de tarjetas de crédito, débito, cuentas bancarias ni ningún tipo de
información relacionada con los medios de pago utilizados.
CLÁUSULA SÉPTIMA: USO DE “MEDILUX”
7.1. SODIG S.A. tendrá las facultades para denegar o restringir el uso de “MEDILUX”
y/o la Plataforma a cualquier Usuario Registrado en caso de incumplimiento de estas 
Condiciones. SODIG S.A. no será responsable si el Usuario Registrado no cuenta con un
teléfono celular inteligente compatible con el uso de “MEDILUX”. El Usuario Registrado
se compromete a hacer un uso adecuado y lícito de “MEDILUX” y/o la Plataforma de
conformidad con la legislación aplicable, estas Condiciones, la moral y buenas
costumbres generalmente aceptadas y el orden público. Al utilizar “MEDILUX” y/o la
Plataforma, el Usuario Registrado acuerda y se obliga a:
Solo utilizar el Servicio para su uso personal y no tendrá facultades para revender, alquilar
o dar cualquier uso comercial a su Cuenta en favor de terceros.
No autorizará a terceros a usar su Cuenta.
No cederá ni transferirá de otro modo su Cuenta a ninguna otra persona o entidad legal.
No utilizará una cuenta que esté sujeta a cualquier derecho de una persona que no sea ella
sin la autorización adecuada.
No solicitará el Servicio con fines ilícitos, ilegales, contrarios a lo establecido en estas
Condiciones, a la buena fe y al orden público, lesivos de los derechos e intereses de
terceros incluyendo, sin limitación, el transporte de material ilegal o con fines
fraudulentos.
Guardará de forma segura y confidencial la contraseña de su Cuenta y cualquier
identificación facilitada para permitirle acceder “MEDILUX” y/o la Plataforma.
No tratará de dañar el Servicio o “MEDILUX” y/o la Plataforma de ningún modo, ni
accederá a recursos restringidos en “MEDILUX” y/o la Plataforma.
No introducirá ni difundirá virus informáticos o cualesquiera otros sistemas físicos o
lógicos que sean susceptibles de provocar daños en “MEDILUX” y/o la Plataforma.
No intentará acceder, utilizar y/o manipular los datos de SODIG S.A., Proveedores
Independientes y otros Usuarios.
No utilizará el Servicio o “MEDILUX” con un dispositivo incompatible o no autorizado.
CLÁUSULA OCTAVA: MODIFICACIONES, CANCELACIÓN Y POLÍTICAS
DE DEVOLUCIÓN
8.1 La Modificación sólo aplicará para el cambio de fecha y hora del servicio solicitado
por el Usuario, más no para el cambio de un servicio por otro servicio previsto en el
catálogo. En caso de que el Usuario no requiera el servicio seleccionado, deberá solicitar
la Cancelación del servicio y realizar una nueva solicitud por el servicio solicitado. La
Cancelación y posterior requerimiento de un nuevo servicio no genera penalidad alguna
para el Usuario.
8.2 El Usuario entiende y acepta que el único servicio que provee SODIG S.A. es aquel
detallado en la cláusula 1.1 de estas Condiciones. Por lo que las políticas de devolución,
calidad y satisfacción de los cuales se responsabiliza SODIG S.A. son únicamente
aquellos detallados en la cláusula mencionada. Esto sin perjuicio de que el Usuario podrá
ejercer su derecho de devolución del valor total cancelado por el servicio solicitado o
adquirido al “Proveedor Independiente”, dentro del término de quince (15) días 
posteriores a la recepción de este, conforme las disposiciones de la Ley Orgánica de
Defensa del Consumidor y su Reglamento.
8.3. SODIG S.A. podrá, por disposición propia, reembolsar los valores por los productos
o servicios contratados con los “Proveedores Independientes”. El Usuario entiende y
acepta que este reembolso voluntario por parte de SODIG S.A. no responsabiliza a
SODIG S.A. por los daños, falencias, falta de calidad, defectos o demás reclamos que el
Usuario tenga sobre los productos o servicios que proveen los “Proveedores
Independientes”.
8.4. SODIG S.A. podrá realizar cambios o modificaciones a esta sección cuando
considere oportuno conforme a lo determina en la Ley de Comercio Electrónico, Firmas
y Mensajes de Datos, Código de Comercio y el procedimiento y condiciones
determinadas en este documento.
CLÁUSULA NOVENA: SERVICIOS QUE PROVEE SODIG S.A.
9.1 Los servicios que factura SODIG S.A., como el “Costo de Procesamiento”, el servicio
de lavado, y todos los servicios que realice directamente SODIG S.A. se cancelarán en el
mismo modo que aquellos servicios de “Proveedores Independientes”, a través de un
botón de pagos. Para los servicios que preste directamente SODIG S.A., ésta emitirá la
factura de consumidor final, sin perjuicio de que el Usuario también podrá solicitar
previamente para la factura correspondiente los datos detallados a los cuales quiere que
consten para efectos tributarios.
CLÁUSULA DÉCIMA: RESPONSABILIDAD
10.1. El Usuario Registrado conoce y acepta que SODIG S.A. pone a disposición de
Proveedores Independientes sólo un espacio virtual que, en calidad de intermediador, les
permite ponerse en comunicación mediante “MEDILUX” y/o Plataforma para que el
Proveedor Independiente pueda brindar los “Servicios de Terceros” a favor de los
Usuarios Registrados. SODIG S.A. no interviene en el perfeccionamiento de las
operaciones, actividades o servicios realizadas por el “Servicio de Terceros” a favor del
Usuario Registrado, por ello no será responsable respecto de la calidad, cantidad, estado,
integridad o legitimidad del servicio ofrecido por el Proveedor Independiente, así como
de la capacidad para contratar con él o de la veracidad de la Información Personal que
este ha ingresado.
10.2. El Usuario Registrado conoce y acepta que al realizar operaciones con el Proveedor
Independiente lo hace bajo su propio riesgo y responsabilidad. En ningún caso SODIG
S.A. será responsable por lucro cesante, o por cualquier otro daño y/o perjuicio que haya
podido sufrir el Usuario Registrado o el Proveedor Independiente, debido al “Servicio de
Terceros” contratado a través de “MEDILUX” y/o la Plataforma.
CLÁUSULA DÉCIMA PRIMERA: USO Y GARANTÍA DE “MEDILUX”
11.1. SODIG S.A. no garantiza la disponibilidad y continuidad del funcionamiento de
“MEDILUX” y/o la Plataforma. En consecuencia, SODIG S.A. no será en ningún caso
responsable por cualquier daño y/o perjuicio que puedan derivarse de: (a) la falta de
disponibilidad o accesibilidad a “MEDILUX” y/o la Plataforma; (b) la interrupción en el
funcionamiento de “MEDILUX” y/o la Plataforma o fallos informáticos, averías 
telefónicas, desconexiones, retrasos o bloqueos causados por deficiencias o sobrecargas
en las líneas telefónicas, centros de datos, en el sistema de Internet o en otros sistemas
electrónicos, producidos en el curso de su funcionamiento; y, (c) otros daños que puedan
ser causados por terceros mediante intromisiones no autorizadas ajenas al control de
SODIG S.A..
11.2. SODIG S.A. no garantiza la ausencia de virus ni de otros elementos en
“MEDILUX” y/o la Plataforma, introducidos por terceros ajenos a SODIG S.A. que
puedan producir alteraciones en los sistemas físicos o lógicos del Usuario Registrado o
en los documentos electrónicos almacenados en sus sistemas. En consecuencia, SODIG
S.A. no será en ningún caso responsable de cualesquiera de los daños y perjuicios de
cualquier naturaleza que pudieran derivarse de la presencia de virus u otros elementos
que puedan producir alteraciones en los sistemas físicos o lógicos, documentos
electrónicos del Usuario Registrado.
11.3. Con la suscripción de las presentes Condiciones, el Usuario que acepta el presente
documento, declara y se compromete a mantener indemne frente a cualquier reclamación
a SODIG S.A., su sociedad matriz, directores, socios, empleados, abogados y agentes,
derivadas de: (a) el incumplimiento por parte del Usuario de cualquier disposición
contenida las presentes Condiciones o de cualquier ley o regulación aplicable a las
mismas; (b) el incumplimiento o violación de los derechos de terceros incluyendo, a título
meramente enunciativo, otros Usuarios; y, (c) el incumplimiento del uso permitido de
“MEDILUX” y/o la Plataforma, siendo estas condiciones meramente enunciativas y no
taxativas, por lo que el Usuario mantendrá indemne a SODIG S.A. por cualquier otra
violación normativa o daño a terceros que pueda producirse como consecuencia de la
utilización del “Servicio de Terceros” por parte del Usuario.
CLÁUSULA DÉCIMA SEGUNDA: DERECHOS DE PROPIEDAD
INTELECTUAL E INDUSTRIAL
12.1. El Usuario Registrado reconoce y acepta que todos los derechos de propiedad
intelectual e industrial sobre los contenidos y/o cualesquiera otros elementos insertados
en “MEDILUX” y/o la Plataforma incluyendo sin limitación; marcas, logotipos, nombres
comerciales, lemas comerciales, textos, imágenes, gráficos, diseños, sonidos, bases de
datos, software, diagramas de flujo, presentación, audio y vídeo y/o cualquier otro
derecho de propiedad intelectual e industrial de cualquier naturaleza que éstos sean,
pertenecen y son de propiedad exclusiva de SODIG S.A.
12.2. SODIG S.A. autoriza al Usuario a utilizar, visualizar, imprimir, descargar y
almacenar los contenidos y/o los elementos insertados en “MEDILUX” y/o la Plataforma
exclusivamente para su uso personal y no lucrativo, absteniéndose de realizar sobre los
mismos cualquier acto de ingeniería inversa, modificación, divulgación o suministro.
Cualquier otro uso o explotación de cualesquiera contenidos y/u otros elementos
insertados en “MEDILUX” y/o la Plataforma distinto de los aquí expresamente previstos
estará sujeto a la autorización previa de SODIG S.A.
12.3. Bajo ningún concepto se entenderá que el acceso a “MEDILUX” y/o la Plataforma
o la aceptación de estas Condiciones, generan algún derecho de cesión a favor del Usuario 
ni de cualquier tercero de los derechos de propiedad intelectual e industrial que pertenecen
a SODIG S.A.
CLÁUSULA DÉCIMA TERCERA: PROTECCIÓN DE DATOS
13.1. Los datos personales que el Usuario proporcione en el proceso de registro serán
tratados según lo dispone la Ley de Comercio Electrónico, Firmas Electrónicas y
Mensajes de Datos, Ley Orgánica de Protección de Datos Personales, y demás normas
conexas. En ese sentido, SODIG S.A. se obliga al cumplimiento estricto de las normas
anteriormente mencionadas, así como a mantener los estándares máximos de seguridad,
protección, resguardo, conservación y confidencialidad de la información recibida o
enviada.
13.2. El Usuario declara que los datos personales han sido entregados de forma
absolutamente libre y voluntaria, sin ningún tipo de presión, obligación o condición de
por medio, por tanto, el Usuario consciente específica, informada, expresa e
inequívocamente en entregar a SODIG S.A. sus datos personales para que sean tratados
de acuerdo con su Política de Privacidad y Tratamiento de Datos Personales.
13.3 Para más información consultar la Política de Privacidad y Tratamiento de Datos
Personales que se encuentra en www.medilux.ec SODIG S.A. se reserva el derecho de
realizar modificaciones a estas políticas cuando considere oportuno.
CLÁUSULA DÉCIMA CUARTA: NOTIFICACIONES
14.1. El Usuario entiende y acepta que SODIG S.A. podrá realizar las notificaciones
necesarias a través de “MEDILUX” y/o la Plataforma, mensajes de texto, o la dirección
de correo electrónico facilitada por el Usuario en su Cuenta, incluyendo el envío de
mensajes con fines promocionales o publicitarios.
El Usuario podrá notificar a SODIG S.A. y solicitar el cese de la actividad promocional
o publicitaria mediante una solicitud a través de la sección de Soporte disponible en la
Plataforma.
CLÁUSULA DÉCIMA QUINTA: CESIÓN
15.1. El Usuario no podrá ceder sus derechos y obligaciones de las presentes Condiciones
sin el previo consentimiento escrito de SODIG S.A.
15.2. SODIG S.A. podrá ceder, sin necesidad de recabar el consentimiento previo del
Usuario, los presentes Condiciones a cualquier entidad comprendida dentro de su grupo
de sociedades en todo el mundo, así como a cualquier persona o entidad que le suceda en
el ejercicio de su negocio por cualesquiera títulos.
CLÁUSULA DÉCIMA SEXTA: LEY APLICABLE Y JURISDICCIÓN
16.1. Las presentes Condiciones, así como la relación entre SODIG S.A. y el Usuario, se
regirán con apego a la legislación vigente en la República del Ecuador.' ></Textmedilux>

            </ScrollView>



            <Button buttonColor="#11D077" onPress={() => {

              setCheckbox("Enabled");


              setCuentacontrato(0);

            }} style={styles.button} contentStyle={styles.button} labelStyle={styles.buttonlabel} mode="contained" loading={estado} >
              Aceptar
            </Button>

          </View>

        </Modal>
      </Portal>

      <Portal>
        <Modal visible={terminos} style={styles.modallabel} onDismiss={() => setTerminos(0)}  >

          <View style={styles.modal} >

            <ScrollView style={styles.modalscroll} >


              <Textmedilux style={styles.logomodal} variant="titleMedium" label='POLÍTICA DE PROTECCIÓN DE DATOS  \n

De conformidad con lo establecido en la Ley de Comercio Electrónico, Firmas Electrónicas y Mensajes de datos, Ley Orgánica de Protección de Datos Personales (en adelante, “LOPDP”) y cualquiera disposición legal conexa que fuere aplicable, el responsable del presente banco de datos en el que se almacenarán los datos personales de los usuarios facilitados en la presente solicitud, es la compañía DESARROLLO E INTEGRACIÓN DE PROCESOS DIGITALES SODIG S.A. (en adelante “SODIG S.A.”), quien obtiene del Titular de los Datos Personales (en adelante, el “Titular”) la presente Declaración de Consentimiento conforme al artículo 8 de la LOPDP, para el tratamiento de sus datos personales a través de la plataforma virtual y/o aplicación que opera.

Esta política de privacidad y tratamiento de datos personales (en adelante, la “Política”) entrará en vigor desde que el Titular de los datos personales preste su consentimiento a la presente Política, mediante su aceptación y posterior uso de la aplicación y plataforma web de SODIG S.A., y estará vigente hasta que éste revoque la autorización al tratamiento de sus datos.  

Como parte normal de las actividades generales de SODIG S.A., recoge y, en algunos casos, revela información sobre los usuarios, proveedores independientes y visitantes (en adelante, el “Titular”) almacenada en la Aplicación (en adelante, la “Aplicación”) y plataforma web (en adelante, la “Plataforma”) que contienen los servicios de intermediación entre proveedores independientes y usuarios, para la compra y venta de servicios de mecánica, llantaje, vulcanizadora, lavado, adquisición de repuestos y accesorios, pago de multas de tránsito, entre otros servicios ofertados para el uso y mantenimiento de vehículos y medios de transporte (en adelante, los “Servicios Ofertados”). Esta Política describe la información que SODIG S.A. recoge del Titular, y el tratamiento de dicha información durante el giro de negocios de SODIG S.A.

El Titular acepta expresamente y otorga su consentimiento libre, específico, previo, informado e inequívoco para que SODIG S.A., quien es el responsable de la base de datos, utilice y realice el tratamiento de la información personal e identificable del Titular bajo la presente Política.

Con el consentimiento del Titular, SODIG S.A. podrá recopilar la información que proporcione (detallada en el numeral 1.1 de la presente Política). De la misma manera, el Titular autoriza a SODIG S.A. a recopilar, acceder, registrar, organizar, almacenar, conservar, elaborar, modificar, extraer, consultar, utilizar, bloquear, entregar para tratamiento por encargo transferir a nivel nacional e internacional, comunicar, modificar, suprimir y, en general, tratar sus datos personales.

Asimismo, el Titular da su consentimiento explícito a SODIG S.A. para la transferencia internacional de sus datos personales, para las mismas finalidades detalladas para el tratamiento local de datos personales.

La privacidad de la información del Titular es muy importante para SODIG S.A. Es por esa razón que se toman las precauciones y recaudos para resguardar su información, utilizando los mecanismos de seguridad informática de protección de la información en cumplimiento de la LOPDP.

 PRIMERA: DEFINICIÓN DE INFORMACIÓN PERSONAL

1.1. A fin de acceder a cualquiera de los Servicios Ofertado en la Aplicación y Plataforma de SODIG S.A., los Titulares de los datos deben registrarse, suministrando ciertos datos personales completos, exactos y vigentes. SODIG S.A. podrá solicitar, recabar y almacenar la siguiente información personal: apodo o seudónimo (nombre de Titular de los datos) para operar en el sitio de SODIG S.A., nombre y apellido, número de documento o identificación válida, información física de contacto (como número de teléfono domicilio, dirección de e-mail, número de teléfono móvil), información del vehículo registrado; y, por lo menos, un método de pago válido (bien una tarjeta de crédito o bien un socio de pago aceptado por SODIG S.A.), (en adelante, la “Información Personal”). SODIG S.A. podrá confirmar la Información Personal suministrada, acudiendo a entidades públicas, compañías especializadas o centrales de riesgo, para lo cual el Titular de los datos mediante la presente Política da su consentimiento expreso. La información que SODIG S.A. obtenga de estas entidades será tratada en forma confidencial.

1.2 La información personal relativa al método y procesamiento de pago que SODIG S.A. destine para realizar la compra y venta de los Servicios Ofertados, quedará excluida de las disposiciones contenidas en referencia a la transferencia de información a terceros Proveedores Independientes.

1.3. Al Titular de los datos que se registre en SODIG S.A., se le otorgará mediante un usuario y contraseña una cuenta personal (en adelante, la “Cuenta”), y su vez, consiente expresamente que SODIG S.A. tenga acceso, en cualquier momento, a la totalidad de la información contenida en su Cuenta, incluyendo en particular, pero sin limitación, a su Información Personal, e información sobre sus intereses, gustos, contactos y cualquier otro contenido alojado en su Cuenta.

SEGUNDA: DERECHOS DE ACCESO, CANCELACIÓN Y RECTIFICACIÓN DE LA INFORMACIÓN PERSONAL

2.1. El Titular practica los derechos de acceder a sus datos personales contenidos en su Cuenta, que están en posesión de SODIG S.A., conocer las características de su tratamiento y fin, actualizar, rectificarlos en caso de ser inexactos o incompletos, solicitar que sean suprimidos o cancelar su información personal, incluyendo su dirección de email, así como a oponerse al tratamiento de la misma y a ser informado de las cesiones y/o transferencias internacionales de la Información Personal llevadas a cabo, todo ello de conformidad y en los límites previstos en la LOPDP.

A fin de ejercer los derechos antes mencionados, el Titular deberá presentar una solicitud enviando un correo electrónico a la siguiente dirección: soporte@sodigsa.com  Esta solicitud deberá incluir los siguientes datos: a) nombre del titular y domicilio u otro medio para recibir respuesta; documentos que acrediten su identidad o la representación legal, descripción clara y precisa de los datos respecto de los que busca ejercer sus derechos y otros elementos o documentos que faciliten la localización de los datos.

SODIG S.A. comunicará a los terceros a quienes haya transferido los datos del Titular; sin embargo, no será responsable por los actos de los terceros o el tratamiento que ellos den a la información remitida, salvo que dichos actos o tratamiento haya sido dispuesto por SODIG S.A.

2.2. El Titular garantiza y responde, en cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de la Información Personal facilitada, y se compromete a mantenerla debidamente actualizada.

2.3. Una vez registrado en SODIG S.A., el Titular de los datos podrá revisar y cambiar la información que ha enviado a SODIG S.A. durante el proceso de registro, incluyendo: A) el usuario y dirección de correo electrónico. B) La información de registro como: domicilio, ciudad, región, código postal, número principal de teléfono, número secundario de teléfono, correo electrónico, detalle del vehículo, etc. C) la clave. D) La información sobre métodos de pago seleccionados por el Usuario. Sin perjuicio de los cambios que realice, SODIG S.A. conservará la información personal anterior por motivos de seguridad y control de fraude.

2.4 SODIG S.A. se regirá a lo dispuesto en el artículo 18 de la LOPDP en relación con la excepción a los derechos de rectificación, actualización, eliminación, oposición, anulación y portabilidad de la información personal de los Usuarios.

2.5. SODIG S.A. conservará la Información Personal del Titular por un periodo de diez (10) años a fin de resolver disputas o reclamos, detectar problemas o incidencias y solucionarlos, y dar cumplimiento a lo dispuesto en los Términos y Condiciones, salvo que exista expresa declinación de la autorización por parte del Titular. SODIG S.A. podrá modificar esta Política y/o las prácticas de envío de e-mails cuando considere oportuno. En caso de que SODIG S.A. modifique la Política, éste notificará al Titular publicando una versión actualizada de la Política mediante el envío de un e-mail o informándolo en la página web principal u otras secciones de la Aplicación. Si las modificaciones a la Política no alteran la finalidad para los que SODIG S.A. trata los datos personales, tales modificaciones serán efectivas al momento de su publicación en la Aplicación y/o Plataforma; caso contrario, SODIG S.A. recabará el consentimiento respectivo del Titular.

TERCERA: USO DE LA INFORMACIÓN PERSONAL

3.1. Para suministrar de manera eficiente el servicio como intermediador de la compra y venta de los Servicios Ofertados; y a fin de que el Titular pueda aplicar de manera ágil y fácil a los mismos, SODIG S.A. requiere cierta información de carácter personal, incluyendo dirección de email y demás datos estipulados en la sección 1.3 de esta Política. La recolección de información permite ofrecer al Titular los servicios y funcionalidades que se adecuen mejor a sus necesidades. La Información Personal que se recaba tiene las siguientes finalidades:

Que la persona que brinda los Servicios Ofertados ubique en tiempo y espacio al Titular para poder prestar un servicio eficiente y rápido.

Por esta razón SODIG S.A. compartirá los datos personales del Titular con los Proveedores Independientes, para que estos se comuniquen con el Titular ante cualquier eventualidad que surja durante la prestación de los Servicios Ofertados, conforme a lo establecido en el artículo 33 y 36 sobre la transferencia de información personal a terceros de la LOPDP.

La Información Personal, sólo podrá ser utilizada en el marco de la prestación de los Servicios Ofertados y, en el caso de los Proveedores Independientes, no deberá ser empleada con fines publicitarios o promocionales u otras actividades no relacionadas con el servicio que prestan a través de SODIG S.A.

Desarrollar estudios internos sobre los intereses, comportamientos y demografía del Titular para comprender mejor sus necesidades e intereses y ofrecer mejores servicios o proveerles información relacionada.

Adicionalmente, el usuario autoriza a SODIG S.A. para que pueda usar su Información Personal para: 

Enviar información o mensajes por e-mail, mensajes (SMS) o plataformas de mensajería instantánea, sobre publicidad o promociones, banners de interés para el Titular y noticias sobre SODIG S.A. Si el Titular lo prefiere, puede solicitar que lo excluyan de las listas para el envío de información promocional o publicitaria, enviando un correo electrónico a la siguiente dirección: soporte@sodigsa.com

En caso de que el Usuario no acepte alguno(s) de los tratamientos de datos personales, esto no afectará a los demás. Sus datos personales solo serán utilizados con propósitos limitados, tal como lo aquí expuesto.

3.2. SODIG S.A. compartirá la Información Personal (incluyendo dirección de e-mail), considerando lo determinado en la sección Primera, 1.3, con los proveedores independientes de servicios o las empresas de “outsourcing” o con las empresas con quienes SODIG S.A. tenga una relación de colaboración o alianza, que contribuyan a mejorar o facilitar las operaciones a través de SODIG S.A.

SODIG S.A. velará porque se cumplan ciertos estándares, mediante la firma de acuerdos o convenios cuyo objeto sea la privacidad de los datos personales del Titular. En algunos casos, estos proveedores de servicios serán quienes recojan información directamente del Titular (por ejemplo, si les solicitamos que realicen encuestas o estudios). En tales casos, podrá recibir una notificación acerca de la participación de un proveedor de servicios independiente en tales actividades, y quedará a discreción del Titular toda la información que quiera. En caso de que el Titular le facilite por propia iniciativa información adicional a dichos prestadores de servicios directamente, tales prestadores usarán esta información conforme a sus propias políticas de privacidad. SODIG S.A. no se hace responsable por el uso indebido de la Información Personal del Titular que hagan compañías o sitios de Internet que actúen por cuenta propia.

3.3. El Titular reconoce y acepta que SODIG S.A. podrá revelar o compartir Información Personal con terceros que son proveedores de servicios o empresas aliadas, afiliadas o relacionadas con SODIG S.A., es sujeción a lo dispuesto en la sección 1.2 y 1.3 de esta Política. En caso de que no sea así, requerirá el consentimiento del Titular de los datos para hacerlo.

3.4 Los Servicios Ofertados podrán ponerse a disposición o ser accesible en relación con servicios y contenido de terceros (incluida la publicidad) que SODIG S.A. no controle. El usuario reconoce que podrán mantener diferentes condiciones y políticas de privacidad al uso que haga de dichos servicios y contenido de terceros. SODIG S.A. no respalda dichos servicios y contenido de terceros; y, en ningún caso, SODIG S.A. será responsable de cualquier producto o servicio de dichos terceros proveedores. SODIG S.A. solo responderá cuando estos terceros realicen su encargo de conformidad con las instrucciones de SODIG S.A.

 CUARTA: CONFIDENCIALIDAD DE LA INFORMACIÓN PERSONAL

4.1. Una vez registrado en la Aplicación o Plataforma, SODIG S.A. no venderá, intercambiará, alquilará o compartirá la Información Personal del Titular excepto en las formas establecidas en esta Política. Sin perjuicio de ello, el Titular consiente en forma expresa que SODIG S.A. transfiera total o parcialmente la Información Personal a cualquiera de las sociedades vinculadas con SODIG S.A., a cualquier título y en el momento, forma y condiciones que estime pertinente. SODIG S.A. hará todo lo posible para proteger la privacidad de la Información Personal. Sin perjuicio de ello, puede suceder que, en virtud de órdenes judiciales, cuando se encuentre permitido por ley, o en caso de prevención de lavado de activos o financiamiento del terrorismo, SODIG S.A. revele la Información Personal a las autoridades o terceras partes sin el consentimiento del Titular.

QUINTA: CLAVE PERSONAL

5.1. Para acceder a los servicios reservados únicamente para el Titular debidamente registrado, el Titular dispondrá de una clave personal. Con ella podrá contratar y calificar los servicios que prestan por terceros a través de la Aplicación y/o Plataforma de SODIG S.A. El Titular deberá mantener esta clave bajo absoluta confidencialidad y, en ningún caso, deberá revelarla o compartirla con otras personas.

5.2. El Titular será responsable de todos los actos que tengan lugar mediante el uso de su nombre de Titular de los datos y clave, lo que incluye hacerse cargo del pago de las tarifas que eventualmente se devenguen o por los perjuicios que puedan sufrir otros Titulares de los datos por tal motivo. Si por cualquier razón un Titular de los datos creyera que alguien puede conocer su clave, deberá modificarla ingresando a la opción de modificación dispuesta para tal fin en la plataforma de SODIG S.A.

SEXTA: MENORES DE EDAD

6.1. La Aplicación está dirigida exclusivamente a personas mayores de 18 años, estando por tanto restringida la entrada de menores de 18 años al mismo. Sin perjuicio de lo anterior, SODIG S.A. se reserva el derecho de verificar, por los medios que considere más oportunos, la edad real de cualquier Titular de los datos.

6.2. Bajo sospecha de que un Titular de los datos de la Aplicación sea menor de 18 años, y de que ha falseado los datos que se requieren para su acceso, SODIG S.A. podrá denegar al referido el acceso a los Servicios Ofertados

SÉPTIMA: REQUERIMIENTOS LEGALES

7.1. SODIG S.A. cooperará con las autoridades competentes y con otros terceros para garantizar el cumplimiento de las leyes. SODIG S.A. podrá revelar la Información Personal del Titular sin su consentimiento únicamente cuando esté permitido por ley, requerido por mandato judicial, o en casos de prevención de lavado de activos o financiamiento del terrorismo o el caso que dicho Titular participa de actividades ilegales. En tales situaciones, SODIG S.A. colaborará con las autoridades competentes con el fin de salvaguardar la integridad y la seguridad de la comunidad y la de sus Titulares de los datos.

OCTAVA: SEGURIDAD Y ALMACENAMIENTO DE LA INFORMACIÓN PERSONAL

8.1. SODIG S.A. está obligado a cumplir con toda la normativa en materia de medidas de seguridad aplicables a los datos personales. Adicionalmente, SODIG S.A. utilizará los estándares de la industria entre materia de protección de la confidencialidad de su Información Personal, incluyendo, entre otras medidas, cortafuegos (“firewalls”) y Secure Socket Layers (“SSL”).

8.2. SODIG S.A. considera la Información Personal del Titular como un activo que debe ser protegido de cualquier pérdida o acceso no autorizado. En razón de esto SODIG S.A. emplea técnicas de seguridad para proteger los datos de accesos no autorizados por Titulares de los datos de dentro o fuera de la compañía. Sin perjuicio de lo expuesto, considerando que Internet es un sistema abierto, de acceso público, SODIG S.A. no puede garantizar que terceros no autorizados no puedan eventualmente superar las medidas de seguridad y utilizar la Información Personal en forma indebida.

8.3. El Titular conoce y acepta expresamente que SODIG S.A., a su exclusivo criterio, recabe, almacene y eventualmente monitoree el intercambio de mensajes y correos electrónicos entre el Titular y Proveedores Independientes, efectuado dentro de la Aplicación y/o Plataforma de SODIG S.A., con el único objetivo de contribuir a la seguridad de las relaciones y comunicaciones que se desarrollan a través de la Aplicación y/o Plataforma.

8.4. La Información Personal de Titular será almacenada en la base de datos de SODIG S.A., la misma que se encuentra en un archivo o soporte automatizado de datos personales. El archivo o soporte de datos personales de los Titulares de los datos de SODIG S.A. reside en los servidores del proveedor de servicios de almacenamiento.

8.5. En el caso de la Aplicación “SODIG S.A.”, para que sus usuarios puedan aceptar y tramitar las órdenes más cercanas a su ubicación, la Aplicación “SODIG S.A.” recogerá datos de su localización, aun cuando la Aplicación se encuentre cerrada o no esté en uso, salvo que el Titular expresamente se oponga.

8.6. SODIG S.A. no se hace responsable por interceptaciones ilegales o violación de sus sistemas o bases de datos por parte de personas no autorizadas. SODIG S.A., tampoco se hace responsable por la indebida utilización de la información obtenida por esos medios.

8.7. La Aplicación ‘SODIG S.A.’ recoge y almacena el número telefónico del usuario el cual es almacenado en nuestra base de datos usando métodos de encriptación para garantizar la protección de la información personal del usuario. La data recolectada se utiliza para identificar posibles comportamientos atípicos dentro de la aplicación y no es compartida ni transferida a terceros.

NOVENA: CAMBIO EN LAS PREFERENCIAS DE E-MAILS

9.1. Sin perjuicio que SODIG S.A. quiere mantener al Titular actualizado en todo momento sobre promociones, novedades, cambios, etc., el Titular puede seleccionar los e-mails e información promocional que gustarían recibir de SODIG S.A., siempre que el Titular haya prestado su consentimiento para recibir esta información.

9.2. En caso de que el Titular no quiera recibir dichos correos electrónicos, éste podrá des-inscribirse cambiando sus preferencias en el e-mail siguiendo las instrucciones que SODIG S.A. proporciona en sus comunicaciones o bien accediendo a su Cuenta Personal. En esa sección el Titular de los datos podrá seleccionar las preferencias para que sean tenidas en cuenta por SODIG S.A. en las futuras comunicaciones o enviando sus preferencias por correo a la dirección postal indicada en estas Políticas de Privacidad. La presente Política de privacidad rige a partir del momento en que el Titular preste su consentimiento.

Protección de datos personales Place To Pay

El titular de los datos acepta y emite su consentimiento unilateral de manera libre y voluntaria para el tratamiento de sus datos personales para que sean: recolectados a través de nuestros canales electrónicos o por cualquier medio, almacenados, procesados, transmitidos y eliminados o para ser objeto de cualquier tratamiento o conjunto de tratamientos a realizar, ya sea por procedimientos técnicos de carácter automatizado, parcialmente automatizado o no automatizados por el Comercio o su delegado, con la finalidad de identificar, actualizar sus datos y ofrecerle servicios y/o productos a través de los canales digitales o cualquier otro medio.

La transmisión y almacenamiento de la información recopilada por el Comercio o su delegado, será realizada en la infraestructura que este administre, dentro o fuera del territorio ecuatoriano. Esta infraestructura puede ser implementada en servidores físicos o en la nube de proveedores que cuenten con las debidas certificaciones internacionales para el tratamiento de Seguridad de la Información.

Adicionalmente, mediante la presente el titular de los datos declara conocer y aceptar el Aviso de Política de Privacidad de Place To Play en la cual se indican sus derechos, datos tratados y las actividades comerciales de los aliados, así como otros datos de interés respecto a la protección de datos personales, que se encuentran detallados en el sitio web de Place To Play en la ruta https://sites.placetopay.ec/  , mediante la cual, se podrá solicitar la gestión de derechos o consentimientos.

El Comercio o su delegado notificará sobre las actualizaciones del Aviso de Política de Privacidad antes mencionado por medio de los diferentes canales digitales.' ></Textmedilux>

            </ScrollView>


            <Button buttonColor="#11D077" onPress={() => {



              setTerminosestado("Enabled");



              setTerminos(0);
            }} style={styles.button} contentStyle={styles.button} labelStyle={styles.buttonlabel} mode="contained" loading={estado} >
              Aceptar
            </Button>

          </View>

        </Modal>
      </Portal>


      <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} />











    </View>




















  );
}


const styles = StyleSheet.create({





  logo: {

    backgroundColor: "#FFFFFF",




    color: '#092D3D',
    fontFamily: "Outfit-Medium",







  },

  checkbox: {


    flex: 1

  },

  checkboxlabel: {


    color: '#092D3D',

    fontFamily: "Outfit-Light",





  },


  create: {





  },


  registrar: {


    textAlign: 'center',





    color: '#11D077',





  },
  modal: {

    backgroundColor: '#FFFFFF',
    gap: 23,
    margin: "5%",



  },

  modallabel: {

    backgroundColor: '#FFFFFF',

    marginTop: "23%",

    borderRadius: 10,

    height: "65%",


    margin: "5%",


  },

  modalscroll: {


    height: "80%"


  },

  dialogcontainer: {

    justifyContent: "center",
    alignItems: "center",
    marginTop: ""


  },


  texttheme: {

    colors: {

      background:

        "#FFFFFF"

    }


  },



  logomodal: {





    color: '#092D3D',
    fontFamily: "Outfit-Medium",







  },
  oauth: {

    justifyContent: "center",
    marginTop: "20%",
    marginBottom: "5%",
    flexDirection: "row",
    gap: 20


  },


  google: {

    width: 50,
    height: 50,
    resizeMode: 'stretch'

  },




  imagen: {

    width: '89%',
    height: '89%',

    aspectRatio: 0.7,
    resizeMode: 'contain',

  },
  buttonlabel: {



    color: '#FFFFFF',


    fontFamily: "Outfit-Bold"









  },

  labelpago: {
    fontFamily: "Outfit-Regular",
  },


  button: {


    borderRadius: 10,




    height: 50,







  },





  medilux: {


    color: '#092D3D',
    fontFamily: "Outfit-Medium",





  },

  mediluxpago: {


    marginTop: "10%",


    color: '#092D3D',
    fontFamily: "Outfit-Medium",
    textAlign: 'center'






  },


  pagobutton: {



    gap: 10,





  },




  formbutton: {



    margin: "5%",

    height: "100%",




  },



  text: {



    marginTop: 50,
  }

});
