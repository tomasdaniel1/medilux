import * as React from "react";
import { StyleSheet } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { ActivityIndicator, IconButton, Text } from "react-native-paper";
import { Button } from "react-native-paper";
import { Image, View } from "react-native";
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import moment from "moment";











const FirstRoute = ({ navigation, token, medidor, actualtransaction }) => (



  <View style={styles.pendientes} >
















    {/* <View style={styles.alertpago}   >

      <Image

        source={require('../../Imagenes/pagostipo.png')}
        style={styles.imagen}







      />






      <View style={styles.labelpendientepago}           >











        <Text variant="bodyLarge" style={styles.labelpago}>
          No tiene pagos pendientes
        </Text>


        <Text variant="bodyLarge" style={styles.labelpagopendiente}>
          Informacion aqui
        </Text>










      </View>






    </View> */}

    {actualtransaction.medidorConsumo.totalPagar > 0 && (


      <View style={styles.alertpendiente}   >







        <View style={styles.labelpendientepago}           >











          <Text variant="bodyLarge" style={styles.labelpago}>
            Pago de luz
          </Text>

          <Text variant="bodyLarge" style={styles.pagopendiente}>
            {moment(Date.now()).format('MM/DD/YYYY')}
          </Text>


          {/* <Text variant="bodyLarge" style={styles.labelpagopendiente}>
    Informacion aquimedi
  </Text> */}

          <Text variant="bodyLarge" style={styles.labelpago}>
            Total: {actualtransaction.medidorConsumo.totalPagar}
          </Text>

          <Button buttonColor="#11D077" labelStyle={styles.labelbuttonpago} onPress={() => {






            navigation.replace("Tarjeta", { medidor: JSON.stringify(medidor), pago: JSON.stringify(actualtransaction), token: token });



          }} style={styles.buttonpago} contentStyle={styles.button} mode="contained"  >
            Pagar
          </Button>






        </View>






      </View>

    )}



    {actualtransaction.medidorConsumo.totalPagar === 0 && (


      <View style={styles.alertpendiente}   >







        <View style={styles.labelpendientepago}           >











          <Text variant="bodyLarge" style={styles.labelpago}>
            Todos los valores están pagados
          </Text>



          <Button buttonColor="#11D077" labelStyle={styles.labelbuttonpago} disabled onPress={() => {



            navigation.reset({
              index: 1,
              routes: [{ name: "Medilux" }, { name: "Tarjeta", params: { medidor: JSON.stringify(medidor), pago: JSON.stringify(actualtransaction), token: token } }],
            })





          }} style={styles.buttonpago} contentStyle={styles.button} mode="contained"  >
            Pagar
          </Button>






        </View>






      </View>

    )}


    {actualtransaction.medidorConsumo.totalPagar < 0 && (


      <View style={styles.alertpendiente}   >







        <View style={styles.labelpendientepago}           >











          <Text variant="bodyLarge" style={styles.labelpago}>



            EL medidor tiene un saldo a favor de {Number(String(actualtransaction.medidorConsumo.totalPagar).replace("-", "")).toFixed(2)} dolares
          </Text>



          <Button buttonColor="#11D077" labelStyle={styles.labelbuttonpago} disabled onPress={() => {



            navigation.reset({
              index: 1,
              routes: [{ name: "Medilux" }, { name: "Tarjeta", params: { medidor: JSON.stringify(medidor), pago: JSON.stringify(actualtransaction), token: token } }],
            })





          }} style={styles.buttonpago} contentStyle={styles.button} mode="contained"  >
            Pagar
          </Button>






        </View>






      </View>

    )}




  </View>






);

const SecondRoute = () => (
  <View style={{ flex: 1, backgroundColor: "transparent" }} />
);




const renderTabBar = props => (
  <TabBar
    {...props}
    activeColor="#092D3D"
    inactiveColor="#A5ACB8"
    indicatorStyle={{ backgroundColor: "#FFFFFF", marginHorizontal: "9.5%", width: "20%" }}
    style={{ backgroundColor: "#F5F8F9" }}
    labelStyle={{ fontFamily: "Outfit-Regular", fontSize: 15 }}
  />
);


export default function Pago({ navigation, statuspago, token, medidor, actualtransaction }) {



  console.log("Pago", actualtransaction);


  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'Pagos Pendientes', style: styles.tipolabel },
  ]);

  console.log(medidor);




  function guardar() {




    navigation.navigate('Tarjeta');




  }




  return (




    <View style={styles.view}>



      {/* <View style={styles.pagoslabel}>












        <Text variant="headlineMedium" style={styles.pago}>
          Pagos
        </Text>


        <View style={styles.pagolabel}      >

          <Text variant="headlineSmall" style={styles.tipolabel}>
            {"Seleccione el tipo de pago "}
          </Text>


          <Text variant="headlineSmall" style={styles.tipolabel}>
            {"que desea realizar"}
          </Text>



        </View>








        <Button buttonColor="#11D077" onPress={() => {





          guardar();
        }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.tipolabel} mode="contained"  >
          Pago de luz
        </Button>

        <Button buttonColor="#11D077" onPress={() => {
        }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.tipolabel} mode="contained"  >
          Otros pagos
        </Button>



      </View>

 */}


      {statuspago === 1 && (


        <View style={styles.pagostipo}>

          <TabView
            renderTabBar={renderTabBar}
            navigationState={{ index, routes }}
            renderScene={SceneMap({
              first: () => (<FirstRoute navigation={navigation} token={token} medidor={medidor} actualtransaction={actualtransaction} />),
            })}
            onIndexChange={setIndex}
          />
        </View>



      )}

      {statuspago === 0 && (


        <ActivityIndicator animating size='large' color="#11D077" />


      )}




    </View>




  );

}

const styles = StyleSheet.create({

  view: {

    flex: 1,

  },

  pago: {

    marginTop: "5%",
    marginBottom: "5%",
    color: "#FFFFFF",
    fontFamily: "Outfit-SemiBold"






  },

  buttonpago: {


    borderRadius: 15


  },

  labelbuttonpago: {

    color: "#FFFFFF",

    fontFamily: "Outfit-Medium",

    fontSize: 18
  },

  alertpendiente: {

    marginTop: "5%",

    backgroundColor: "#FFFFFF",
    justifyContent: "center",
    borderRadius: 15,


  },

  pagoslabel: {
    flex: 1,

    borderBottomLeftRadius: 23,
    borderBottomRightRadius: 23,


    alignItems: "center",




    backgroundColor: "#092D3D",



  },
  tipolabel: {

    fontFamily: "Outfit-Medium",


    color: "#FFFFFF"


  },
  pagolabel: {

    alignItems: "center",
    marginBottom: "5%"



  },
  pagostipo: {

    flex: 1,


  },

  goBackButton: {

    width: '50%',
    height: '50%',

    aspectRatio: 0.6,
    resizeMode: 'contain',
    margin: 5



  }

  ,







  pagopendiente: {




    textAlign: 'left',


    color: "#11D077",


    fontFamily: "Outfit-Regular",






  },










  logo: {
    marginTop: '3%',

    textAlign: 'center',


    color: '#092D3D',

    borderRadius: 10,

    width: "80%"

  },



  button: {



    height: 50,





  },

  labelpago: {


    fontSize: 18,

    fontFamily: "Outfit-SemiBold",





  },

  labelpagopendiente: {




    fontFamily: "Outfit-Regular",



    color: "#A5ACB8"


  },

  alertpago: {

    borderColor: "rgba(165, 172, 184, 0.5)",
    borderWidth: 1,

    width: "100%",
    flexDirection: "row",
    backgroundColor: "#E4EEED",
    alignItems: "center",
    marginTop: '5%',
    borderRadius: 10,






  },


  labelpendientepago: {

    gap: 10,
    margin: "5%"





  },


  button: {



    height: 47,




  },
  imagen: {
    width: '23%',
    height: '23%',

    aspectRatio: 1.1,
    resizeMode: 'contain',
    margin: "5%"

  },

  pendientes: {



    marginHorizontal: "5%",



  }
});
