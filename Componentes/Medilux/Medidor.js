import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';

import { StyleSheet } from 'react-native';
import PagerView from 'react-native-pager-view';
import { IconButton, Modal, Portal, Text } from 'react-native-paper';
import Textmedilux from '../Otros/Textmedilux.js';
import { Button } from 'react-native-paper';
import { List } from 'react-native-paper';
import { SegmentedButtons } from 'react-native-paper';
import { ScrollView } from 'react-native';
import { FormBuilder } from 'react-native-paper-form-builder';
import { useForm } from 'react-hook-form';









import { TextInput } from 'react-native-paper';



export default function Medidor({ measurer, actualmeasurer, medidor, setActualmeasurer, cuentacontrato, measurers, statuscolor, setStatuscolor, token, navigation }) {




  const { control, setFocus, handleSubmit } = useForm({
    defaultValues: {
      buscar: '',

    },
    mode: 'onSubmit',
  });


  const [measurermodal, setMeasurermodal] = React.useState("");


  const [modalmeasurer, setModalmeasurer] = React.useState(0);







  return (










    <View style={styles.medidor}      >

      <View style={styles.medidorpago}      >


        <View style={styles.logopago}      >


          <View style={styles.mediluxlabel}      >
            <Textmedilux style={styles.mediluxhola} variant="headlineSmall" label='Hola,' ></Textmedilux>
            <Textmedilux style={styles.mediluxlabelhola} variant="headlineSmall" label={medidor.nombre}></Textmedilux>

          </View>


          <IconButton

            onPress={() => {

              setStatuscolor("#FFFFFF");

              navigation.replace('Notificaciones', { token: token });



            }}

            size={23}


            iconColor={statuscolor}

            icon={require('../../Imagenes/notificacion.png')}
            style={styles.icon}
          />


        </View>

        <View style={styles.pagomedidor}  >





          <View style={styles.mediluxlogo}  >

            <Textmedilux style={styles.mediluxcuenta} variant="titleMedium" label='Cuenta' ></Textmedilux>
            <Textmedilux style={styles.medilux} variant="headlineMedium" label={actualmeasurer.numeroCuentaContrato} ></Textmedilux>
            <View style={styles.medidormedilux}      >
              <Textmedilux style={styles.mediluxlabelcuenta} variant="labelLarge" label='Nro de medidor:' ></Textmedilux>
              <Textmedilux style={styles.medilux} variant="bodyLarge" label={actualmeasurer.numeroMedidor} ></Textmedilux>



            </View>

          </View>


          <View style={styles.mediluxlogo}  >


            <Button icon={({ size, color }) => (
              <IconButton size={40} style={styles.iconlogo} icon={require('../../Imagenes/pagos.png')} />
            )} onPress={() => {





              setModalmeasurer(1);
            }} buttonColor="#FFFFFF" style={styles.button} contentStyle={styles.buttoncontent} labelStyle={styles.buttonlabel} mode="contained" >
              {actualmeasurer.alias}
            </Button>



          </View>





        </View>

      </View>


      {/* <Button buttonColor="#08CE72" style={styles.button} contentStyle={styles.buttoncontent} labelStyle={styles.buttonlabel} mode="contained" >
        Activo
      </Button>



      <View style={styles.cuentalabel}      >
        {
          measurers.map((label) => {
            return (

              <View style={styles.cuentamedidor}      >
                <Textmedilux style={styles.medilux} variant="labelSmall" label={label.cuenta} ></Textmedilux>
                <Textmedilux style={styles.mediluxcuenta} variant="labelSmall" label={label.medilux} ></Textmedilux>


              </View>
            )





          })

        }

      </View> */}




      <Portal>
        <Modal visible={modalmeasurer} style={styles.modal} onDismiss={() => setModalmeasurer(0)}  >


          <View style={styles.modalcontent}      >

            <ScrollView style={styles.contentmeasurermodal}      >



              <Textmedilux style={styles.modallabel} variant="headlineMedium" label="Medidores" ></Textmedilux>




              <FormBuilder
                control={control}
                setFocus={setFocus}
                formConfigArray={[

                  {
                    type: "email",
                    name: 'measurer',



                    textInputProps: {

                      label: <Textmedilux style={styles.logo} variant="titleMedium" label='Buscar' ></Textmedilux>,
                      value: { measurermodal },
                      onChange: text => {

                        setMeasurermodal(text.nativeEvent.text);
                      },
                      right: <TextInput.Icon size={40} icon={require('../../Imagenes/pagos.png')} />,
                      activeOutlineColor: 'transparent',
                      textColor: '#092D3D',
                      underlineColor: 'transparent',
                      outlineColor: 'transparent',
                      contentStyle: styles.labelcontent,
                      theme: styles.texttheme

                    },
                  },
                ]}
              />


              <View style={styles.contentmeasurer}      >





                {


                  measurermodal === "" && (

                    measurers.map((content) => {
                      return (


                        <TouchableOpacity style={styles.measurermodal} onPress={() => {

                          setActualmeasurer(content);
                          setModalmeasurer(0);



                        }}    >

                          <View style={styles.measurerpago}      >

                            <Textmedilux style={styles.measurerlabel} variant="labelLarge" label={content.alias} ></Textmedilux>


                            <View style={styles.measurercontent}      >
                              <Textmedilux style={styles.logo} variant="labelLarge" label="Nro de Medidor:" ></Textmedilux>
                              <Textmedilux style={styles.measurerlabel} variant="labelLarge" label={content.numeroCuentaContrato} ></Textmedilux>








                            </View>


                          </View>


                        </TouchableOpacity>
                      )






                    })

                  )


                }

                {


                  measurermodal !== "" && (

                    measurers.map((content) => {


                      return (


                        content.alias.includes(measurermodal) && (



                          <TouchableOpacity style={styles.measurermodal} onPress={() => {

                            setActualmeasurer(content);
                            setModalmeasurer(0);



                          }}    >
                            <Textmedilux style={styles.measurerlabel} variant="labelLarge" label={content.alias} ></Textmedilux>


                            <View style={styles.measurercontent}      >
                              <Textmedilux style={styles.logo} variant="labelLarge" label="Nro de Medidor:" ></Textmedilux>
                              <Textmedilux style={styles.measurerlabel} variant="labelLarge" label={content.numeroCuentaContrato} ></Textmedilux>








                            </View>




                          </TouchableOpacity>
                        )



                      )


                    })

                  )


                }
              </View>

            </ScrollView>


          </View>

        </Modal>
      </Portal>



    </View>








  );
}









const styles = StyleSheet.create({


  labelcontent: {

    borderRadius: 20,

    backgroundColor: "#F5F8F9"
  },


  medidor: {
    backgroundColor: "#031D36",




    height: '43%',
    width: "100%",


    borderBottomLeftRadius: 23,
    borderBottomRightRadius: 23,

  },

  logoimagen: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    aspectRatio: 3.7


  },

  logopago: {

    alignItems: "center",

    flexDirection: "row",




    textAlign: 'center',



    justifyContent: 'space-between'


  },

  pagos: {
    marginTop: 10,

    width: "100%",





  },


  modal: {



  },

  contentmeasurermodal: {

    flex: 1,

    gap: 23,

    margin: "7%"
  },

  contentmeasurer: {

    flex: 1,
    gap: 36
  },

  measurermodal: {

    backgroundColor: "#F5F8F9",

    height: 80,

    justifyContent: "center",

    borderRadius: 10,



  },
  texttheme: {


    roundness: 20,
    colors: {



      background:

        "#F5F8F9"

    }


  },

  modalcontent: {

    alignSelf: "center",

    borderRadius: 10,

    backgroundColor: "#FFFFFF",
    width: "80%",
    height: "80%"

  },


  logo: {
    color: "#A5ACB8",

    fontFamily: "Outfit-SemiBold",


    fontSize: 15,







  },

  measurerpago: {


    margin: "5%"


  },

  iconlogo: {


  },



  icon: {



    bordertoRadius: 2,


    height: "100%"

  },


  mediluxlogo: {

    flex: 1


  },

  medidormedilux: {




    flexDirection: "row",
    alignItems: "center",


  },

  medidorpago: {



    margin: "10%",

    marginHorizontal: "7%",

    justifyContent: 'space-between',

    gap: 23,

    flex: 1,

  },

  mediluxlabelcuenta: {

    color: "rgba(255,255,255,0.5)",






    fontFamily: "Outfit-Medium",



  },

  buttonpagos: {
    backgroundColor: "white",
    width: "10%",
  },

  cuentamedidor: {

    backgroundColor: "rgba(255,255,255,0.1)",


    flex: 1,
    marginHorizontal: '2%',
    borderRadius: 10,


    marginTop: '3%',
    justifyContent: "center",
    alignItems: "center",

  },

  iconbutton: {

    marginBottom: "50%"
  },



  cuentalabel: {
    height: '29%',
    flexDirection: "row",

    marginHorizontal: '5%',

    marginTop: "2%"
  },
  buttonlabel: {

    fontSize: 17,

    color: "#092D3D",

    fontFamily: "Outfit-Medium",






  },

  measurerlabel: {


    color: "#092D3D",

    fontFamily: "Outfit-SemiBold",


    fontSize: 15,



  },

  button: {




    borderRadius: 10,



  },

  mediluxhola: {

    fontFamily: "Outfit-Bold"




  },

  mediluxlabelhola: {

    fontFamily: "Outfit-Medium",





  },

  measurercontent: {

    marginTop: "5%",

    flexDirection: "row",

  },

  buttoncontent: {

    height: 50,

    justifyContent: "space-between",

    flexDirection: 'row-reverse'





  },

  mediluxcuenta: {




    textAlign: 'left',


    color: "#11D077",


    fontFamily: "Outfit-Regular",






  },


  medilux: {



    textAlign: 'left',

    fontFamily: "Outfit-Medium",












  },

  modallabel: {

    color: "#092D3D",


    textAlign: 'left',

    fontFamily: "Outfit-Medium",


  },

  pagomedidor: {






    height: "100%",


  },


  mediluxlabelpago: {


    fontFamily: "Outfit-SemiBold",



  },


  mediluxlabel: {

    alignItems: "center",

    flexDirection: "row",



    textAlign: 'center',










  },
  labelmedilux: {

    fontFamily: "Outfit-Medium"

  },




  pagerView: {
    height: "20%",
  },
});





