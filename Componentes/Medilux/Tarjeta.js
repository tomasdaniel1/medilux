import * as React from "react";
import { StyleSheet, SafeAreaView, Image, BackHandler } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { Button, Card, IconButton, Text } from "react-native-paper";
import { ScrollView } from 'react-native';
import { View } from 'react-native';












import Mediluxalert from '../Otros/Mediluxalert.js';
import Textmedilux from "../Otros/Textmedilux.js";
import { getmeasurers, postpago, tipopago } from "../Apicuentacontrato.js";
import { err } from "react-native-svg/lib/typescript/xml.js";
import moment from "moment";



export default function Tarjeta({ navigation, route }) {


  const [hide, setHide] = React.useState(0);
  const [label, setLabel] = React.useState("");
  const [content, setContent] = React.useState("");

  const [token, setToken] = React.useState("");


  const [estado, setEstado] = React.useState("Pagar");

  const [tarjeta, setTarjeta] = React.useState([]);

  const [tipo, setTipo] = React.useState([]);

  const [pago, setPago] = React.useState(null);






  const [screen, setScreen] = React.useState(null);

  const [tipotarjeta, setTipotarjeta] = React.useState('none');

  const [medidor, setMedidor] = React.useState([]);



  async function guardar() {
    try {


      if (tipo) {


        setEstado("Guardando")

        const tarjetaresponse = await postpago(medidor, pago, tarjeta, tipo, token);

        const tarjetatipo = tarjetaresponse.data;

        const state = tipopago(tarjetatipo.transactionState);




        try {


          const measurers = await getmeasurers(token);



          await EncryptedStorage.setItem("measurers", JSON.stringify(measurers.data));

        } catch (response) {


        }
        setScreen("Medilux");



        mediluxalert(state, `Tu pago con número de referencia ${tarjetatipo.transactionReference} del ${moment(tarjetatipo.transactionDate).format("YYYY-MM-DD")} se encuentra ${state}. El total de la transacción es:  $${tarjetatipo.transactionTotal}`);




      }
      else {

        mediluxalert("Error", "Debe poner una tarjeta y un tipo de pago ");


      }





    } catch (error) {


      console.log(error);



      if (error.response) {

        if (error.response.status === 401) {

          mediluxalert("Error", "El token de sesión ha expirado");


        }

        else if (error.response.data) {

          console.log(error.response.data);





          mediluxalert("Error", error.response.data.message);






        }

      }


      else {

        mediluxalert("Error", "Hubo un error en el servidor ");


      }



    }

    setEstado("Pagar");
  }




  function mediluxalert(label, content) {


    setHide(1);



    setLabel(label);
    setContent(content);


  }


  React.useEffect(() => {



    BackHandler.addEventListener('hardwareBackPress', () => {



      if (navigation.isFocused()) {

        navigation.replace("Medilux");

        return 1;

      }




    });

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', () => {



        if (navigation.isFocused()) {

          navigation.replace("Medilux");

          return 1;

        }




      });
    };
  }, []);



  React.useEffect(() => {






    setMedidor(JSON.parse(route.params.medidor));

    let pagomedidor = JSON.parse(route.params.pago);

    if (pagomedidor.medidorConsumo.monto) {


    }
    else {

      pagomedidor.medidorConsumo.monto = null;

    }


    setPago(pagomedidor);

    console.log("dsadsadas", route.params.pago.medidorConsumo);


    if (route.params.tipo) {

      setTipo(JSON.parse(route.params.tipo));

    }
    else {

      setTipo(route.params.tipo);

    }


    if (route.params.tarjeta) {

      setTipotarjeta('flex');

      setTarjeta(JSON.parse(route.params.tarjeta));

    }
    else {

      setTarjeta(route.params.tarjeta);

    }



    setToken(route.params.token);

    navigation.setOptions({
      headerShown: 0
    });

    return () => { };
  }, []);


  return (



    <View style={styles.view}      >


      <View style={styles.iconButton}      >


        <IconButton
          icon={require('../../Imagenes/credit.png')}
          size={50}
          iconColor='#08CE72'
          onPress={() => {

            navigation.replace("Medilux");




          }}
        />
        <Text style={styles.pago} variant="headlineSmall"    > Pago  </Text>




      </View>


      <ScrollView>



        <View style={styles.card}      >




          <Text style={styles.label} variant="headlineSmall"    > Información de factura  </Text>

          {medidor.tipo === "1" && (


            <View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Tipo de documento" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label="Cédula" ></Textmedilux>




              </View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Cedula" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.cedula} ></Textmedilux>




              </View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Nombre" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.nombre} ></Textmedilux>




              </View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Apellido" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.apellido} ></Textmedilux>




              </View>

            </View>


          )}

          {medidor.tipo === "2" && (


            <View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Tipo de documento" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label="RUC" ></Textmedilux>




              </View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="RUC" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.ruc} ></Textmedilux>




              </View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Razon social" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.razonsocial} ></Textmedilux>




              </View>

            </View>


          )}




          {medidor.tipo === "3" && (


            <View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Tipo de documento" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label="Pasaporte" ></Textmedilux>




              </View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Cedula" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.pasaporte} ></Textmedilux>




              </View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Nombre" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.nombre} ></Textmedilux>




              </View>

              <View style={styles.mediluxlabel}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Apellido" ></Textmedilux>



                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.apellido} ></Textmedilux>




              </View>

            </View>

          )}






          <View style={styles.mediluxlabel}      >
            <Textmedilux style={styles.medilux} variant="labelLarge" label="Teléfono" ></Textmedilux>



            <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.telefono} ></Textmedilux>




          </View>

          <View style={styles.mediluxlabel}      >
            <Textmedilux style={styles.medilux} variant="labelLarge" label="Dirección" ></Textmedilux>



            <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.direccion} ></Textmedilux>




          </View>

          <View style={styles.mediluxlabel}      >
            <Textmedilux style={styles.medilux} variant="labelLarge" label="Email" ></Textmedilux>



            <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={medidor.email} ></Textmedilux>




          </View>




        </View>


        {pago && (


          <View style={styles.card}      >



            <Text style={styles.label} variant="headlineSmall"    > Información de pago </Text>





            <View style={styles.mediluxcontent}      >
              <Textmedilux style={styles.medilux} variant="labelLarge" label="Detalle" ></Textmedilux>



              <Textmedilux style={styles.medilux} variant="labelLarge" label="Valor" ></Textmedilux>




            </View>

            <View style={styles.mediluxcontent}      >
              <Textmedilux style={styles.mediluxtarjeta} variant="labelLarge" label="Pago de servicio de luz" ></Textmedilux>



              <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={pago.medidorConsumo.totalPagar} ></Textmedilux>




            </View>

            <View style={styles.mediluxpago}      >
              <Textmedilux style={styles.medilux} variant="labelLarge" label="Subtotal" ></Textmedilux>



              <Textmedilux style={styles.medilux} variant="labelLarge" label={pago.medidorConsumo.totalPagar} ></Textmedilux>




            </View>


            {pago.medidorConsumo.monto && (


              <View style={styles.mediluxpago}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Interes del pago" ></Textmedilux>



                <Textmedilux style={styles.medilux} variant="labelLarge" label={pago.medidorConsumo.monto} ></Textmedilux>




              </View>

            )}

            <View style={styles.mediluxpago}      >
              <Textmedilux style={styles.medilux} variant="labelLarge" label="Comisión" ></Textmedilux>



              <Textmedilux style={styles.medilux} variant="labelLarge" label={Number(pago.medidorComision.comision)} ></Textmedilux>




            </View>



            <View style={styles.mediluxpago}      >
              <Textmedilux style={styles.medilux} variant="labelLarge" label="Impuesto" ></Textmedilux>



              <Textmedilux style={styles.medilux} variant="labelLarge" label={Number(pago.medidorComision.impuesto).toFixed(2)} ></Textmedilux>




            </View>

            {pago.medidorConsumo.monto && (


              <View style={styles.mediluxpago}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Total" ></Textmedilux>





                <Textmedilux style={styles.medilux} variant="labelLarge" label={Number(Number(pago.medidorConsumo.totalPagar) + Number(pago.medidorComision.comision) + Number(pago.medidorConsumo.monto) + Number(pago.medidorComision.impuesto)).toFixed(2)} ></Textmedilux>



              </View>

            )}


            {pago.medidorConsumo.monto === null && (


              <View style={styles.mediluxpago}      >
                <Textmedilux style={styles.medilux} variant="labelLarge" label="Total" ></Textmedilux>





                <Textmedilux style={styles.medilux} variant="labelLarge" label={Number(Number(pago.medidorConsumo.totalPagar) + Number(pago.medidorComision.comision) + Number(pago.medidorConsumo.monto) + Number(pago.medidorComision.impuesto)).toFixed(2)} ></Textmedilux>



              </View>

            )}



          </View>

        )}


        <Card style={styles.cardpago} onPress={() => {






          navigation.replace("Tipo", { medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), token: token });



        }}      >


          <View style={styles.mediluxtipo}      >
            <Textmedilux style={styles.medilux} variant="labelLarge" label="Seleccione la tarjeta" ></Textmedilux>







          </View>






          {typeof (tarjeta) === "undefined" && (


            <View style={styles.mediluxtipo}      >
              <Textmedilux style={styles.mediluxpagotarjeta} variant="labelLarge" label="No hay ninguna tarjeta seleccionada" ></Textmedilux>






            </View>
          )}

          {tarjeta && (


            <View style={styles.mediluxtipo}      >
              <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={tarjeta.franquicia}  ></Textmedilux>


              <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={"**********" + tarjeta.ultimosDigitos} ></Textmedilux>




            </View>


          )}




        </Card>

        <Card style={{

          display: tipotarjeta,
          backgroundColor: "#FFFFFF",
          marginHorizontal: "5%",
          borderRadius: 20,
          marginTop: "5%",


          borderColor: "#11D077",
          borderWidth: 1,


        }} onPress={() => {




          navigation.replace("Notificacion", { medidor: JSON.stringify(medidor), tarjeta: JSON.stringify(tarjeta), pago: JSON.stringify(pago), token: token });





        }}      >


          <View style={styles.mediluxtipo}      >
            <Textmedilux style={styles.medilux} variant="labelLarge" label="Seleccione un tipo de pago" ></Textmedilux>







          </View>


          {typeof (tipo) === "undefined" && (


            <View style={styles.mediluxtipo}      >
              <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label="No hay ninguna tipo de pago seleccionado" ></Textmedilux>






            </View>
          )}

          {tipo && (


            <View style={styles.mediluxtipo}      >
              <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={tipo.descripcion}  ></Textmedilux>


              <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={"Cuotas: " + tipo.cuotas} ></Textmedilux>




            </View>


          )}







        </Card>



        <Button buttonColor="#11D077" labelStyle={styles.labelbuttonpago} disabled={estado === "Guardando"} loading={estado === "Guardando"} onPress={() => {






          guardar();



        }} style={styles.buttonpago} contentStyle={styles.button} mode="contained"  >
          Pagar
        </Button>




        <View style={styles.logoptp}>






          <Image
            source={{ uri: "https://static.placetopay.com/placetopay-logo.png" }}
            style={styles.logo}


          />

          <Image
            source={require('../../Imagenes/tarjeta.png')}
            style={styles.logo}


          />








        </View>


        <Mediluxalert hide={hide} label={label} content={content} screen={screen} setHide={setHide} navigation={navigation} />

      </ScrollView>





    </View>






  );

}

const styles = StyleSheet.create({
  view: {




    flex: 1, backgroundColor: "#F5F8F9"
  },














  card: {

    backgroundColor: "#FFFFFF",
    marginHorizontal: "5%",
    borderRadius: 20,
    marginTop: "5%",



  },

  cardpago: {

    backgroundColor: "#FFFFFF",
    marginHorizontal: "5%",
    borderRadius: 20,
    marginTop: "5%",

    borderColor: "#11D077",
    borderWidth: 1,

  },


  label: {


    color: '#092D3D',

    margin: "6%",

    fontSize: 19,

    marginBottom: "8%",


    fontFamily: "Outfit-SemiBold",










  },


  logo: {

    width: 235,
    height: 55,
    resizeMode: 'contain',
    aspectRatio: 5.3

  },

  labelbuttonpago: {

    fontFamily: "Outfit-Medium",

    fontSize: 18
  },

  mediluxtipo: {

    justifyContent: "center",


    gap: 2,
    margin: "5%",

    marginBottom: "2%",


  },




  mediluxlabel: {

    flexDirection: "row",
    gap: 5,
    margin: "5%",
    marginTop: "0%",

  },

  logoptp: {

    alignItems: "center",

    flex: 1,

    marginHorizontal: "10%",

    marginTop: "20%",
    marginBottom: "5%",
    gap: 20


  },



  mediluxcontent: {

    flexDirection: "row",
    justifyContent: "space-between",
    margin: "4%",
    marginTop: "0%",
    borderBottomColor: "#11D077",
    borderBottomWidth: 1,

  },

  mediluxpago: {

    flexDirection: "row",
    justifyContent: "space-between",
    margin: "5%",
    marginTop: "0%",


  },


  iconButton: {

    flexDirection: "row",
    alignItems: "center",





  }



  ,

  medilux: {


    marginLeft: "3%",

    fontSize: 16,

    textAlign: 'center',

    color: "#092D3D",


    fontFamily: "Outfit-Medium"










  },

  buttonpago: {


    marginTop: "5%",


    marginHorizontal: "5%",

    borderRadius: 10,


  },

  button: {

    height: 50


  },


  mediluxcuenta: {


    fontSize: 16,

    textAlign: 'center',


    color: "#A5ACB8",


    fontFamily: "Outfit-Medium"



  },

  mediluxpagotarjeta: {


    fontSize: 16,

    textAlign: 'center',


    color: "#A5ACB8",

    marginTop: -5,

    fontFamily: "Outfit-Medium"



  },

  mediluxtarjeta: {

    marginLeft: "3%",

    fontSize: 16,

    textAlign: 'center',


    color: "#A5ACB8",



    fontFamily: "Outfit-Medium"



  },

  pago: {


    color: '#092D3D',



    marginLeft: "21%",

    fontFamily: "Outfit-SemiBold",










  },







  goBackButton: {
    marginLeft: "40%",
    justifyContent: "center",
  }
});
