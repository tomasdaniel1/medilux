import * as React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { Button, IconButton, Text } from "react-native-paper";
import { ScrollView } from 'react-native';
import { View } from 'react-native';












import Mediluxalert from '../Otros/Mediluxalert.js';
import Textmedilux from "../Otros/Textmedilux.js";
import { postpago } from "../Apicuentacontrato.js";
import { err } from "react-native-svg/lib/typescript/xml.js";



export default function FAQ({ navigation }) {



    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");

    const [token, setToken] = React.useState("");





    const FAQ = [
        {
            pregunta: "1. ¿Qué es Placetopay?",
            respuesta:
                "Placetopay es la plataforma de pagos electrónicos que usa Medilux para procesar en línea las transacciones generadas en la tienda virtual con las formas de pago habilitadas para tal fin.",
        },
        {
            pregunta: "2. ¿Cómo puedo pagar? ",
            respuesta:
                "En la tienda virtual de AutoApp usted podrá realizar su pago con los medios habilitados para tal fin. Usted, de acuerdo a las opciones de pago escogidas por el comercio, podrá pagar a través Diners, Discover, Visa y MasterCard; de todos los bancos con pago corriente y en los diferido, únicamente las tarjetas emitidas por Banco Pichincha, Diners, Loja, BGR y Manabí.",
        },
        {
            pregunta: "3. ¿Es seguro ingresar mis datos bancarios en este sitio web?",
            respuesta:
                `Para proteger tus datos AutoApp delega en Placetopay la captura de la información sensible. Nuestra plataforma de pagos cumple con los más altos estándares exigidos por la norma internacional PCI DSS de seguridad en transacciones con tarjeta de crédito. Además tiene certificado de seguridad SSL expedido por GeoTrust una compañía Verisign, el cual garantiza comunicaciones seguras mediante la encriptación de todos los datos hacia y desde el sitio; de esta manera, te podrás sentir seguro a la hora de ingresar la información de su tarjeta. 
    Durante el proceso de pago, en el navegador se muestra el nombre de la organización autenticada, la autoridad que lo certifica y la barra de dirección cambia a color verde. Estas características son visibles de inmediato y dan garantía y confianza para completar la transacción en Placetopay.
    Placetopay también cuenta con el monitoreo constante de McAfee Secure y la firma de mensajes electrónicos con Certicámara.
    Placetopay es una marca de la empresa colombiana EGM Ingeniería Sin Fronteras S.A.S.`,
        },
        {
            pregunta: "4. ¿Puedo realizar el pago cualquier día y a cualquier hora? ",
            respuesta:
                "Sí, en AutoApp podrás realizar tus compras en línea los 7 días de la semana, las 24 horas del día a sólo un clic de distancia.",
        },
        {
            pregunta: "5. ¿Puedo cambiar la forma de pago?",
            respuesta:
                "Si aún no has finalizado tu pago, podrás volver al paso inicial y elegir la forma de pago que prefieras. Una vez finalizada la compra no es posible cambiar la forma de pago.",
        },
        {
            pregunta: "6. ¿Pagar electrónicamente tiene algún valor para mí como comprador?",
            respuesta:
                "No, los pagos electrónicos realizados a través de Placetopay no generan costos adicionales para el comprador.",
        },
        {
            pregunta: "7. ¿Qué debo hacer si mi transacción no concluyó?",
            respuesta:
                `En primera instancia deberás revisar si llegó un mail de confirmación del pago en tu cuenta de correo electrónico (la inscrita en el momento de realizar el pago), en caso de no haberlo recibido, deberás contactar a info@autoapp.info para confirmar el estado de la transacción.
    En caso que tu transacción haya declinado, debes verificar si la información de la cuenta es válida, está habilitada para compras no presenciales y si tienes cupo o saldo disponible. Si después de esto continua con la declinación debes comunicarte con AutoApp. En última instancia, puedes remitir tu solicitud a servicioposventa@placetopay.ec.`,
        },
        {
            pregunta: "8. ¿Qué debo hacer si no recibí el comprobante de pago?",
            respuesta:
                "Por cada transacción aprobada a través de Placetopay, recibirás un comprobante del pago con la referencia de compra en la dirección de correo electrónico que indicaste al momento de pagar.  Si no lo recibes, podrás contactar al correo electrónico info@autoapp.info, para solicitar el reenvío del comprobante a la misma dirección de correo electrónico registrada al momento de pagar. En última instancia, puedes remitir tu solicitud a servicioposventa@placetopay.ec.",
        },
        {
            pregunta: "9. No me recibí el servicio que compré ¿qué hago?",
            respuesta:
                "Debes verificar si la transacción fue exitosa en tu extracto bancario. En caso de ser así, debes revisar nuestras políticas de envío en el sitio web https://medilux.ec para identificar los tiempos de entrega.",
        },
    ];

    const [estado, setEstado] = React.useState("Pagar");



    const [medidor, setMedidor] = React.useState([]);


    async function guardar() {
        try {

            setEstado("Guardando")

            const tarjeta = await postpago(measurer, token);





        } catch (error) {


            mediluxalert(error.response.data);

            console.log(error.response.data);

        }

        setEstado("Pagar");
    }

    async function cargar() {
        try {
            const credito = await EncryptedStorage.getItem("credito");

            const creditoparse = 340.00;


            const params = await new URLSearchParams({
                entityId: "8ac7a4ca8810780f01882096776e090b",
                amount: creditoparse,
                currency: "USD",
                paymentType: "DB",

            }).toString();

            await axios
                .post(`https://test.oppwa.com/v1/checkouts?${params}`, null, {
                    headers: {
                        Authorization: `Bearer OGE4Mjk0MTg1YTY1YmY1ZTAxNWE2YzhjNzI4YzBkOTV8YmZxR3F3UTMyWA==


`,
                    },
                })
                .then((response) => {
                    setId(response.data.id);

                    console.log(response.data.id);
                })
                .catch((error) => {
                    console.log(error.response.data.result.parameterErrors);
                });
        } catch (error) {

        }
    }


    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }

    async function guardar() {
        mediluxalert("Error", "Se ha realizado el pago");
    }

    React.useEffect(() => {

        navigation.setOptions({
            headerShown: 0
        });

        return () => { };
    }, []);


    return (



        <View style={styles.view}      >


            <View style={styles.iconButton}      >


                <IconButton
                    icon={require('../../Imagenes/credit.png')}
                    size={50}
                    iconColor='#08CE72'
                    onPress={() => {

                        navigation.goBack();




                    }}
                />
                <Text style={styles.pago} variant="headlineSmall"    > FAQ  </Text>




            </View>


            <ScrollView>






                <View style={styles.card}      >


                    {
                        FAQ.map((label) => {
                            return (


                                <View  >

                                    <Textmedilux style={styles.medilux} variant="labelLarge" label={label.pregunta} ></Textmedilux>

                                    <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={label.respuesta} ></Textmedilux>



                                </View>
                            )






                        })


                    }





                </View>




                <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} change={"Medilux"} navigation={navigation} />

            </ScrollView>

        </View>






    );

}

const styles = StyleSheet.create({
    view: {




        flex: 1, backgroundColor: "#FFFFFF"
    },














    card: {

        backgroundColor: "#FFFFFF",
        marginHorizontal: "10%",
        borderRadius: 5,
        gap: 11,
        marginTop: "5%"


    },


    label: {


        color: '#092D3D',

        margin: "5%",

        fontFamily: "Outfit-SemiBold",










    },

    mediluxlabel: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginTop: "0%"

    },

    mediluxcontent: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",
        borderBottomColor: "green",
        borderBottomWidth: 2

    },

    mediluxpago: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",


    },


    iconButton: {

        flexDirection: "row",
        alignItems: "center",





    }



    ,

    medilux: {




        color: "#092D3D",


        fontFamily: "Outfit-SemiBold"










    },


    mediluxcuenta: {





        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },

    pago: {


        color: '#092D3D',



        marginLeft: "21%",

        fontFamily: "Outfit-SemiBold",










    },







    goBackButton: {
        marginLeft: "40%",
        justifyContent: "center",
    }
});
