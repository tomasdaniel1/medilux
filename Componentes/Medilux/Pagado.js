import * as React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { ActivityIndicator, IconButton, Portal, Text } from "react-native-paper";
import { Button } from "react-native-paper";
import { Image, View } from "react-native";
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Textmedilux from "../Otros/Textmedilux";
import { gethistoric, tipopago } from "../Apicuentacontrato";
import { ScrollView } from "react-native-gesture-handler";
import moment from "moment";
import Mediluxalert from "../Otros/Mediluxalert";











const FirstRoute = ({ navigation, payedTransactions }) => (




    <ScrollView style={styles.pendientes} >

        {payedTransactions.length === 0 && (



            <Textmedilux style={styles.mediluxnotificacion} variant="titleLarge" label="No tiene pagos" ></Textmedilux>

        )}


        {
            payedTransactions.map((label) => {
                return (




                    <View style={styles.card}      >


                        <View style={styles.mediluxpago}      >



                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Referencia" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={label.transactionReference} ></Textmedilux>




                            </View>

                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Fecha y hora" ></Textmedilux>


                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={moment(label.transactionDate).format('MM-DD-YYYY HH:mm:ss')} ></Textmedilux>




                            </View>

                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Estado" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={tipopago(label.transactionState)} ></Textmedilux>



                            </View>


                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Valor" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={Number(label.transactionTotal).toFixed(2)} ></Textmedilux>




                            </View>



                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Autorizacion" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={label.transactionAuthorization} ></Textmedilux>




                            </View>


                        </View>



                    </View>



                )
            })}










    </ScrollView>






);

const SecondRoute = ({ navigation, pendingTransactions }) => (




    <ScrollView style={styles.pendientes} >


        {pendingTransactions.length === 0 && (



            <Textmedilux style={styles.mediluxnotificacion} variant="titleLarge" label="No tiene pagos" ></Textmedilux>

        )}


        {
            pendingTransactions.map((label) => {
                return (




                    <View style={styles.card}      >


                        <View style={styles.mediluxpago}      >



                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Referencia" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={label.transactionReference} ></Textmedilux>




                            </View>

                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Fecha y hora" ></Textmedilux>


                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={moment(label.transactionDate).format('MM-DD-YYYY HH:mm:ss')} ></Textmedilux>




                            </View>

                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Estado" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={tipopago(label.transactionState)} ></Textmedilux>



                            </View>


                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Valor" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={Number(label.transactionTotal).toFixed(2)} ></Textmedilux>




                            </View>



                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Autorizacion" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={label.transactionAuthorization} ></Textmedilux>




                            </View>


                        </View>



                    </View>



                )
            })}










    </ScrollView>






);




const ThirdRoute = ({ navigation, rejectedTransactions }) => (




    <ScrollView style={styles.pendientes} >


        {rejectedTransactions.length === 0 && (



            <Textmedilux style={styles.mediluxnotificacion} variant="titleLarge" label="No tiene pagos" ></Textmedilux>

        )}



        {
            rejectedTransactions.map((label) => {
                return (




                    <View style={styles.card}      >

                        <View style={styles.mediluxpago}      >

                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Referencia" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={label.transactionReference} ></Textmedilux>




                            </View>

                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Fecha y hora" ></Textmedilux>


                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={moment(label.transactionDate).format('MM-DD-YYYY HH:mm:ss')} ></Textmedilux>




                            </View>

                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Estado" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={tipopago(label.transactionState)} ></Textmedilux>



                            </View>


                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Valor" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={Number(label.transactionTotal).toFixed(2)} ></Textmedilux>




                            </View>



                            <View style={styles.mediluxlabel}      >
                                <Textmedilux style={styles.medilux} variant="labelLarge" label="Autorizacion" ></Textmedilux>



                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={label.transactionAuthorization} ></Textmedilux>




                            </View>



                        </View>


                    </View>



                )
            })}










    </ScrollView>






);




const renderTabBar = props => (
    <TabBar
        {...props}
        activeColor="#092D3D"
        inactiveColor="#A5ACB8"
        indicatorStyle={{ backgroundColor: "#11D077", marginHorizontal: "4%", width: "20%" }}
        style={{ backgroundColor: "#F5F8F9" }}
        labelStyle={{ fontFamily: "Outfit-Regular", fontSize: 12 }}
    />
);


export default function Pagado({ navigation, route }) {





    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'first', title: 'Aprobadas', style: styles.tipolabel },
        { key: 'second', title: 'Pendientes', style: styles.tipolabel },
        { key: 'third', title: 'Rechazadas', style: styles.tipolabel },

    ]);

    const [payedTransactions, setPayedTransactions] = React.useState([]);
    const [pendingTransactions, setPendingTransactions] = React.useState([]);
    const [rejectedTransactions, setRejectedTransactions] = React.useState([]);

    const [token, setToken] = React.useState([]);


    const [screen, setScreen] = React.useState("");



    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");


    const [status, setStatus] = React.useState("enabled");

    React.useEffect(() => {

        setToken(route.params.token);



        guardar(route.params.token);

        navigation.setOptions({
            headerShown: 0
        });

        return () => { };
    }, []);


    async function guardar(token) {


        try {


            setStatus("disabled");

            const historicresponse = await gethistoric(token);


            console.log(historicresponse.data.pendingTransactions);

            setPayedTransactions(historicresponse.data.payedTransactions);
            setPendingTransactions(historicresponse.data.pendingTransactions);
            setRejectedTransactions(historicresponse.data.rejectedTransactions);






        } catch (error) {


            if (error.response.status === 401) {

                mediluxalert("Error", "El token de sesión ha expirado");


            }


            else if (error.response.data) {

                console.log(error.response.data);





                mediluxalert("Error", error.response.data.message);






            }
            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }

            console.log(error);


        }


        setStatus("enabled");









    }




    return (




        <View style={styles.view}>



            {/* <View style={styles.pagoslabel}>












        <Text variant="headlineMedium" style={styles.pago}>
          Pagos
        </Text>


        <View style={styles.pagolabel}      >

          <Text variant="headlineSmall" style={styles.tipolabel}>
            {"Seleccione el tipo de pago "}
          </Text>


          <Text variant="headlineSmall" style={styles.tipolabel}>
            {"que desea realizar"}
          </Text>



        </View>








        <Button buttonColor="#11D077" onPress={() => {





          guardar();
        }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.tipolabel} mode="contained"  >
          Pago de luz
        </Button>

        <Button buttonColor="#11D077" onPress={() => {
        }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.tipolabel} mode="contained"  >
          Otros pagos
        </Button>



      </View>

 */}
            <View style={styles.iconButton}      >


                <IconButton
                    icon={require('../../Imagenes/credit.png')}
                    size={50}
                    iconColor='#08CE72'
                    onPress={() => {

                        navigation.goBack();




                    }}
                />
                <Text style={styles.pago} variant="headlineSmall"    > Historial de pagos  </Text>

                <Text style={styles.pago} variant="headlineSmall"    >  </Text>



            </View>



            <View style={styles.pagostipo}>




                {status === "disabled" && (


                    <ActivityIndicator animating size='large' color="#11D077" />


                )}


                {status === "enabled" && (


                    <TabView
                        renderTabBar={renderTabBar}
                        navigationState={{ index, routes }}
                        renderScene={SceneMap({
                            first: () => (<FirstRoute navigation={navigation} payedTransactions={payedTransactions} />),
                            second: () => (<SecondRoute navigation={navigation} pendingTransactions={pendingTransactions} />),
                            third: () => (<ThirdRoute navigation={navigation} rejectedTransactions={rejectedTransactions} />),
                        })}
                        onIndexChange={setIndex}
                    />

                )}
            </View>


            <Portal >


                <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} screen={screen} navigation={navigation} />

            </Portal >



        </View>




    );

}

const styles = StyleSheet.create({

    view: {

        backgroundColor: "#F5F8F9",

        flex: 1,

    },

    pago: {

        marginTop: "5%",
        marginBottom: "5%",
        color: "#FFFFFF",
        fontFamily: "Outfit-SemiBold"






    },

    alertpendiente: {

        marginTop: "5%",

        backgroundColor: "#FFFFFF",
        height: 200,
        justifyContent: "center",
        borderRadius: 10

    },


    mediluxpago: {


        margin: "5%"

    },

    pagoslabel: {
        flex: 1,

        borderBottomLeftRadius: 23,
        borderBottomRightRadius: 23,


        alignItems: "center",




        backgroundColor: "#092D3D",



    },
    tipolabel: {

        fontFamily: "Outfit-Medium",

        color: "#FFFFFF"


    },
    pagolabel: {

        alignItems: "center",
        marginBottom: "5%"



    },
    pagostipo: {

        flex: 1,


    },

    goBackButton: {

        width: '50%',
        height: '50%',

        aspectRatio: 0.6,
        resizeMode: 'contain',
        margin: 5



    }

    ,

    pago: {


        color: '#092D3D',




        fontFamily: "Outfit-SemiBold",










    },


    iconButton: {

        flexDirection: "row",
        alignItems: "center",





    },




    pagopendiente: {




        textAlign: 'left',


        color: "#11D077",


        fontFamily: "Outfit-Regular",






    },


    mediluxnotificacion: {




        color: "#A5ACB8",



        fontFamily: "Outfit-Medium",




        margin: "5%",

        alignSelf: 'center'



    },




    mediluxlabel: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginTop: "0%",
        marginBottom: "0%"


    },

    medilux: {



        textAlign: 'center',

        color: "#092D3D",


        fontFamily: "Outfit-SemiBold"










    },


    mediluxcuenta: {



        textAlign: 'center',


        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },



    logo: {
        marginTop: '3%',

        textAlign: 'center',


        color: '#092D3D',

        borderRadius: 10,

        width: "80%"

    },


    card: {

        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        marginTop: "5%",



    },


    button: {



        height: 50,





    },

    labelpago: {



        fontFamily: "Outfit-Regular",





    },

    labelpagopendiente: {




        fontFamily: "Outfit-Regular",



        color: "#A5ACB8"


    },

    alertpago: {

        borderColor: "rgba(165, 172, 184, 0.5)",
        borderWidth: 1,

        width: "100%",
        flexDirection: "row",
        backgroundColor: "#E4EEED",
        alignItems: "center",
        marginTop: '5%',
        borderRadius: 10,






    },


    labelpendientepago: {

        gap: 10,
        marginHorizontal: "5%"





    },


    button: {



        height: 50,





    },
    imagen: {
        width: '23%',
        height: '23%',

        aspectRatio: 1.1,
        resizeMode: 'contain',
        margin: "5%"

    },

    pendientes: {


        backgroundColor: "#F5F8F9",
        marginHorizontal: "5%",



    }
});
