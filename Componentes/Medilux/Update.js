import * as React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { ActivityIndicator, Button, IconButton, Modal, Portal, Text, TextInput } from "react-native-paper";
import { ScrollView } from 'react-native';
import { View } from 'react-native';












import Mediluxalert from '../Otros/Mediluxalert.js';
import Textmedilux from "../Otros/Textmedilux.js";
import { FormBuilder } from "react-native-paper-form-builder";
import { useForm } from 'react-hook-form';
import { deletemeasurer, getmeasurers, gettarjetas, updatemeasurer } from "../Apicuentacontrato.js";



export default function Update({ navigation, route }) {




    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");

    const [measurer, setMeasurer] = React.useState([]);

    const [modalstatus, setModalstatus] = React.useState(0);

    const [status, setStatus] = React.useState("enabled");

    const [medidor, setMedidor] = React.useState([]);






    const [screen, setScreen] = React.useState(null);



    const [token, setToken] = React.useState("");




    const { control, setFocus, handleSubmit } = useForm({
        defaultValues: {
            medidor: '',

        },
        mode: 'onSubmit',
    });



    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }

    async function update(data) {

        try {


            setStatus("disabled");

            const update = await updatemeasurer(measurer, medidor, token);

            const measurers = await getmeasurers(token);

            await EncryptedStorage.setItem("measurers", JSON.stringify(measurers.data));

            setScreen("Medilux");

            mediluxalert("¡Listo!", "Se Edito el medidor ");


        }

        catch (err) {

            console.log(err);



            if (err.response) {

                if (err.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (err.response.data) {

                    console.log(err.response.data);





                    mediluxalert("Error", err.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }

        }

        setStatus("enabled");

    }

    async function deletetipo() {

        try {












            setModalstatus(0);
            setStatus("disabled");


            if (route.params.measurers === 1) {



                mediluxalert("Error", "Debe tener un medidor en la aplicacion");


            }
            else {

                const update = await deletemeasurer(measurer, token);

                const measurers = await getmeasurers(token);

                await EncryptedStorage.setItem("measurers", JSON.stringify(measurers.data));


                setScreen("Medilux");




                mediluxalert("¡Listo!", "Se borro el medidor ");


            }


        }

        catch (err) {


            console.log(err);



            if (err.response) {

                if (err.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (err.response.data) {

                    console.log(err.response.data);





                    mediluxalert("Error", err.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }


        }


        setStatus("enabled");
    }


    async function guardar() {

        const measurerresponse = await JSON.parse(await EncryptedStorage.getItem("measurer"));

        console.log(measurerresponse);

        const tokenresponse = await EncryptedStorage.getItem("token");

        setMeasurer(measurerresponse);

        setToken(tokenresponse)



    }

    React.useEffect(() => {



        guardar();

        navigation.setOptions({ headerShown: 0 });








        return () => {

        };
    }, []);



    return (


        <View style={styles.view}      >


            <View style={styles.iconButton}      >


                <IconButton
                    icon={require('../../Imagenes/credit.png')}
                    size={50}
                    iconColor='#08CE72'
                    onPress={() => {

                        navigation.goBack();




                    }}
                />
                <Text style={styles.pago} variant="headlineSmall"    > Editar medidores  </Text>




            </View>

            {status === "enabled" && (


                <View style={styles.labelpago}      >


                    <Textmedilux style={styles.mediluxlabelcuenta} variant="labelLarge" label='Nombre de medidor:' ></Textmedilux>


                    <FormBuilder
                        control={control}
                        setFocus={setFocus}
                        formConfigArray={[

                            {
                                type: "text",
                                name: 'medidor',








                                rules: {

                                    required: {
                                        value: 1,
                                        message: 'No puede dejar este campo vacio',
                                    },





                                },




                                textInputProps: {

                                    label: <Textmedilux style={styles.logo} variant="titleMedium" label='Nombre de medidor' ></Textmedilux>,
                                    value: measurer.alias,
                                    onChange: text => {

                                        setMedidor(text.nativeEvent.text);
                                    },
                                    activeOutlineColor: "#11D077",
                                    textColor: '#092D3D',
                                    underlineColor: 'transparent',
                                    outlineColor: 'transparent',
                                    contentStyle: styles.labelcontent,
                                    theme: styles.texttheme

                                },
                            },
                        ]}
                    />

                    <Textmedilux style={styles.mediluxlabelcuenta} variant="labelLarge" label='Nro de medidor:' ></Textmedilux>


                    <View style={styles.measurermodal} >
                        <Textmedilux style={styles.measurerlabel} variant="labelLarge" label={measurer.numeroMedidor} ></Textmedilux>




                    </View>

                    <Textmedilux style={styles.mediluxlabelcuenta} variant="labelLarge" label='Nro de cuenta contrato:' ></Textmedilux>

                    <View style={styles.measurermodal} >
                        <Textmedilux style={styles.measurerlabel} variant="labelLarge" label={measurer.numeroCuentaContrato} ></Textmedilux>




                    </View>

                    <View style={styles.buttonedit}      >



                        <Button buttonColor="#FFFFFF" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbutton} textColor='#092D3D' mode="contained" onPress={(() => {


                            setModalstatus(1);
                            //   deletetipo();
                        })} >
                            Eliminar
                        </Button>
                        <Button buttonColor="#11D077" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbutton} mode="contained" onPress={handleSubmit((data: any) => {



                            update(data);
                        })} >
                            Editar
                        </Button>
                    </View>



                </View >



            )}


            {status === "disabled" && (


                <ActivityIndicator animating size='large' color="#11D077" />


            )}

            <Portal>



                <Modal visible={modalstatus} onDismiss={(() => {

                    setModalstatus(0);

                })} >

                    <View style={styles.modal}   >


                        <Textmedilux style={styles.mediluxlabelcuenta} variant="labelLarge" label='¿Está seguro?' ></Textmedilux>


                        <View style={styles.modaltarjeta}   >


                            <Button buttonColor="#11D077" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbutton} mode="contained" onPress={(() => {

                                deletetipo();


                            })} >
                                Confirmar
                            </Button>

                            <Button buttonColor="#FFFFFF" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbutton} textColor='#092D3D' mode="contained" onPress={(() => {

                                setModalstatus(0);
                            })} >
                                Cancelar
                            </Button>


                        </View >

                    </View >





                </Modal >

            </Portal >





            <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} screen={screen} navigation={navigation} />


        </View >






    );

}

const styles = StyleSheet.create({
    view: {




        flex: 1, backgroundColor: "transparent"
    },














    card: {

        backgroundColor: "#FFFFFF",
        marginHorizontal: "10%",
        borderRadius: 5,
        marginTop: "5%"


    },
    buttonlabel: {

        borderRadius: 10,
        marginTop: "5.9%"







    },

    buttonedit: {


    },

    label: {


        color: '#092D3D',

        margin: "5%",

        fontFamily: "Outfit-SemiBold",










    },

    modal: {

        backgroundColor: "#F5F8F9",


        justifyContent: 'center',

        alignItems: 'center',


        borderRadius: 10,





        marginHorizontal: "5%",

        height: "50%",



    },


    modallabel: {



        justifyContent: 'center',

        alignItems: 'center',





    },

    modaltarjeta: {


        flexDirection: 'row',

        justifyContent: 'center',

        alignItems: 'center',

        gap: 53,




    },

    labelpago: {

        marginHorizontal: "5%"

    },

    mediluxlabel: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginTop: "0%"

    },

    mediluxcontent: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",
        borderBottomColor: "green",
        borderBottomWidth: 2

    },

    mediluxpago: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",


    },

    button: {



        height: 50,













    },
    labelbutton: {





        fontFamily: "Outfit-Medium",

        fontSize: 18

    },

    measurermodal: {

        justifyContent: "center",


        backgroundColor: "#EEF3F3",

        height: 50,

        borderRadius: 10,

        borderColor: "#E3E6E5",

        borderWidth: 1,

        margin: "2%",
        marginLeft: "0%"


    },
    iconButton: {

        flexDirection: "row",
        alignItems: "center",





    },
    logo: {
        marginTop: "15%",

        textAlign: 'center',


        color: '#092D3D',

        fontFamily: "Outfit-Medium",

        backgroundColor: "#FFFFFF"






    },

    measurerlabel: {




        color: "#092D3D",

        fontFamily: "Outfit-SemiBold",


        fontSize: 15,
        marginHorizontal: "5%"



    },

    texttheme: {


        roundness: 10,


    },



    medilux: {



        textAlign: 'center',

        color: "#092D3D",


        fontFamily: "Outfit-SemiBold"










    },
    mediluxlabelcuenta: {


        fontSize: 20,


        fontFamily: "Outfit-Regular",



        color: "#A5ACB8"




    },

    mediluxcuenta: {



        textAlign: 'center',


        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },

    pago: {


        color: '#092D3D',



        marginLeft: "7%",

        fontFamily: "Outfit-SemiBold",










    },







    goBackButton: {
        marginLeft: "40%",
        justifyContent: "center",
    }
});
