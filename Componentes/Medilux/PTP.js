import * as React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { IconButton, Portal, Text } from "react-native-paper";
import { ScrollView } from 'react-native';
import { View } from 'react-native';












import Mediluxalert from '../Otros/Mediluxalert.js';
import { getpago, gettarjeta, tipo, tipopago } from "../Apicuentacontrato.js";
import moment from "moment";



export default function PTP({ navigation, route }) {
  const [nombre, setNombre] = React.useState("");

  const [indicator, setIndicator] = React.useState(0);

  const [id, setId] = React.useState("");

  const [ip, setIp] = React.useState("")
  const baseURL = "http://192.168.50.15:4700";

  const [tarjeta, setTarjeta] = React.useState([]);

  const [token, setToken] = React.useState("");

  const [medidor, setMedidor] = React.useState([]);

  const [pago, setPago] = React.useState([]);

  const [screen, setScreen] = React.useState([]);


  const [hide, setHide] = React.useState(0);
  const [label, setLabel] = React.useState("");
  const [content, setContent] = React.useState("");



  let pagoptp = "ptp";


  function mediluxalert(label, content) {


    setHide(1);



    setLabel(label);
    setContent(content);


  }

  async function guardar() {
    try {



      const requestresponse = await gettarjeta(tarjeta.requestId, route.params.token);

      console.log(requestresponse.data);


      setScreen("Tipo");


      const request = requestresponse.data;




      const state = tipopago(request.subscriptionState);


      if (state === "Ok") {


        mediluxalert(state, `Tu registro de tarjeta con número de referencia ${request.subscriptionReference} del ${moment(request.transactionDate).format("YYYY-MM-DD")} se encuentra aceptado.La tarjeta se ha registrado`);

      }
      else {

        mediluxalert(state, `Tu registro de tarjeta con número de referencia ${request.subscriptionReference} del ${moment(request.transactionDate).format("YYYY-MM-DD")} se encuentra ${state}`);

      }



    } catch (error) {

      console.log(error);



      if (error.response) {

        if (error.response.status === 401) {

          mediluxalert("Error", "El token de sesión ha expirado");


        }

        else if (error.response.data) {

          console.log(error.response.data);





          mediluxalert("Error", error.response.data.message);






        }

      }


      else {

        mediluxalert("Error", "Hubo un error en el servidor ");


      }



    }
  }




  React.useEffect(() => {


    setTarjeta(JSON.parse(route.params.tarjeta));

    setToken(route.params.token);

    setMedidor(JSON.parse(route.params.medidor));

    setPago(JSON.parse(route.params.pago));





    navigation.setOptions({
      headerShown: 0
    });

    return () => { };
  }, []);


  return (


    <SafeAreaView style={styles.view}>



      <View style={styles.iconButton}      >


        <IconButton
          icon={require('../../Imagenes/credit.png')}
          size={50}
          iconColor='#08CE72'
          onPress={() => {

            navigation.goBack();




          }}
        />
        <Text style={styles.pago} variant="headlineSmall"    > Registro de tarjeta </Text>




      </View>

      <WebView

        style={styles.pagocuenta}

        source={{ uri: tarjeta.subscriptionUrl }}
        onNavigationStateChange={(navState) => {


          console.log(navState);




          if (navState.url === "https://www.servicios.medilux.ec/medilux-administrador/land" && pagoptp === "ptp") {


            pagoptp = "disabled";
            guardar();

          }



        }}
      />

      <Portal>


        <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} screen={screen} route={{ medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), token: token }} navigation={navigation} />

      </Portal>


    </SafeAreaView>



  );

}

const styles = StyleSheet.create({
  view: { height: "100%", backgroundColor: "#FFFFFF", width: "100%" },


















  iconButton: {

    flexDirection: "row",
    alignItems: "center",
    backgroundColor: 'rgba(52, 52, 52, 0.1)'





  }



  ,

  pago: {


    color: '#092D3D',



    marginLeft: "5%",

    fontFamily: "Outfit-SemiBold",










  },


  pagocuenta: {

    flex: 1,
    height: "100%"


  },




  goBackButton: {
    marginLeft: "40%",
    justifyContent: "center",
  }
});
