import * as React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { Button, IconButton, Text } from "react-native-paper";
import { ScrollView } from 'react-native';
import { View } from 'react-native';












import Mediluxalert from '../Otros/Mediluxalert.js';
import Textmedilux from "../Otros/Textmedilux.js";
import { getmeasurers, getotp, gettransaction, otp, postnotificacion, posttoken, postuser } from "../Apicuentacontrato.js";
import { err } from "react-native-svg/lib/typescript/xml.js";
import { OtpInput } from "react-native-otp-entry";



export default function OTP({ navigation }) {



    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");

    const [token, setToken] = React.useState("");

    const [otp, setOtp] = React.useState("");

    const [medidor, setMedidor] = React.useState("");




    const [labelotp, setLabelotp] = React.useState("Enviar OTP");




    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }


    async function otpsend() {
        try {

            const user = await getotp({ email: medidor.email, contrasena: medidor.contrasena });


            mediluxalert("Ok", "Se envio el codigo OTP");

        } catch (error) {

            console.log(error.response);


            mediluxalert("Error", "Hubo un error en el servidor");


        }
    }



    async function cargar() {
        try {

            const medidorresponse = await JSON.parse(await EncryptedStorage.getItem("medidor"));


            setMedidor(medidorresponse);

            console.log(medidorresponse);

        } catch (error) {

        }
    }


    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }

    async function guardar() {

        try {

            if (otp.length === 6) {

                setLabelotp("Enviando");

                const user = await postuser(medidor, otp);


                let cedula = "";
                let ruc = "";
                let pasaporte = "";

                let razonsocial = "";

                let tipo = "";


                if (user.data.usuario.idTipoIdentificacion === 1) {

                    cedula = user.data.usuario.identificacion;
                    tipo = "1";



                }
                else if (user.data.usuario.idTipoIdentificacion === 2) {

                    ruc = user.data.usuario.identificacion;
                    tipo = "2";

                    razonsocial = user.data.usuario.nombre;

                }
                else if (user.data.usuario.idTipoIdentificacion === 3) {

                    pasaporte = user.data.usuario.identificacion;
                    tipo = "3";

                }


                await EncryptedStorage.setItem("medidor", JSON.stringify({ email: user.data.usuario.email, nombre: user.data.usuario.nombre, apellido: user.data.usuario.apellido, pasaporte: pasaporte, razonsocial: razonsocial, ruc: ruc, telefono: user.data.usuario.telefono, tipo: tipo, direccion: user.data.usuario.direccion, cedula: cedula }));

                const token = user.data.token;




                try {


                    let asdasd = await postnotificacion(token);


                } catch (response) {


                    console.log("dasdsadasdasd", response);


                    try {


                        await messaging().registerDeviceForRemoteMessages();
                        const notificacion = await messaging().getToken();

                        console.log("Se creo el token ", notificacion);


                        await posttoken(notificacion, token);





                    } catch (response) {




                        console.log("dasdsadasdas", response);




                    }




                }




                if (user.success) {




                    await EncryptedStorage.setItem("token", token);

                    // const measurers = await getmeasurers(token);

                    // console.log(measurers);

                    // await EncryptedStorage.setItem("measurers", JSON.stringify(measurers.data));

                    // await EncryptedStorage.setItem("monthsconsumption", JSON.stringify([{ "mes": "Noviembre", "pago": "15.70" }, { "mes": "Octubre", "pago": "35.25" }, { "mes": "Septiembre", "pago": "31.23" }, { "mes": "Agosto", "pago": "36.20" }, { "mes": "Julio", "pago": "31.68" }, { "mes": "Junio", "pago": "29.11" }]));

                    // await EncryptedStorage.setItem("medidor", JSON.stringify(data));


                    // const transaction = await gettransaction(measurers.data[0], token);

                    // await EncryptedStorage.setItem("transaction", JSON.stringify(transaction.data));





                    await navigation.navigate('Medidores');


                }
                else {

                    mediluxalert("Error", user.message);

                }

            }
            else {

                mediluxalert("Error", "No ha ingresado un codigo otp valido");


            }



        } catch (response) {


            console.log(response);



            if (response.response.data) {



                mediluxalert("Error", response.response.data.message);


            }
            else {

                mediluxalert("Error", "Ha ocurrido un error en el servidor");

            }



        }

        setLabelotp("Enviar OTP");
    }



    React.useEffect(() => {


        cargar();

        navigation.setOptions({
            headerShown: 0
        });

        return () => { };
    }, []);


    return (





        <View style={styles.iconButton}      >



            <Text style={styles.pago} variant="headlineSmall"    > Ingreso de OTP  </Text>







            <Textmedilux style={styles.logo} variant="titleMedium" label="Se ha enviado un código de identificación al correo registrado" ></Textmedilux>


            <View style={styles.otpmedilux}      >


                <OtpInput onTextChange={(otp) => {

                    setOtp(otp);

                }} autoFocusOnLoad={0} focusColor="#11D077" otpLength={6} />

            </View>



            <Button onPress={() => {

                otpsend();

            }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.logobutton} mode="text"  >
                ¿Recibiste el código?
            </Button>


            <Button disabled={labelotp === "Enviando"} loading={labelotp === "Enviando"} buttonColor="#11D077" onPress={() => {

                guardar();

            }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.labelbutton} mode="contained"  >
                {labelotp}
            </Button>

            <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} />



        </View>






    );

}

const styles = StyleSheet.create({
    view: {




        flex: 1, backgroundColor: "transparent"
    },














    card: {

        backgroundColor: "#FFFFFF",
        marginHorizontal: "10%",
        borderRadius: 5,
        marginTop: "5%"


    },


    label: {


        color: '#092D3D',

        margin: "5%",

        fontFamily: "Outfit-SemiBold",










    },


    logo: {
        marginTop: "15%",

        textAlign: 'center',



        color: '#092D3D',

        fontFamily: "Outfit-Medium",







    },

    otpmedilux: {

        width: "80%",



        marginTop: "5%",


    },


    logobutton: {


        color: '#11D077',
        fontFamily: "Outfit-Medium",




    },

    labelbutton: {


        fontFamily: "Outfit-Medium",




    },

    logo: {
        marginTop: 20,


        textAlign: 'center',



        color: '#092D3D',


        borderRadius: 10,

        width: "80%"

    },


    mediluxlabel: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginTop: "0%"

    },

    button: {



        height: 50,






    },

    mediluxcontent: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",
        borderBottomColor: "green",
        borderBottomWidth: 2

    },

    mediluxpago: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",


    },


    iconButton: {

        marginTop: "5%",

        alignItems: "center",
        justifyContent: "center",





    }



    ,

    medilux: {



        textAlign: 'center',

        color: '#092D3D',


        fontFamily: "Outfit-SemiBold"










    },


    mediluxcuenta: {



        textAlign: 'center',


        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },

    pago: {


        color: '#092D3D',




        fontFamily: "Outfit-SemiBold",










    },







    goBackButton: {
        marginLeft: "40%",
        justifyContent: "center",
    }
});
