import * as React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { Button, IconButton, Text } from "react-native-paper";
import { ScrollView } from 'react-native';
import { View } from 'react-native';












import Mediluxalert from '../Otros/Mediluxalert.js';
import Textmedilux from "../Otros/Textmedilux.js";



export default function Measurer({ navigation, measurers }) {




    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");





    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }

    async function guardar(measurer) {

        await EncryptedStorage.setItem("measurer", JSON.stringify(measurer));

        await navigation.navigate("Update", { measurers: measurers.length });


    }

    React.useEffect(() => {


        return () => { };
    }, []);



    return (



        <View style={styles.iconButton}      >


            <Textmedilux style={styles.mediluxlabel} variant="headlineSmall" label='Tus medidores' ></Textmedilux>



            <ScrollView style={styles.contentmeasurermodal}      >

                <Button buttonColor="#11D077" style={styles.button} contentStyle={styles.contentbutton} labelStyle={styles.buttonlabel} mode="contained" onPress={() => {

                    navigation.navigate("Post");

                }}    >
                    Registrar medidor
                </Button>



                <Textmedilux style={styles.modallabel} variant="headlineMedium" label="Nombre de medidor" ></Textmedilux>







                <View style={styles.contentmeasurer}      >





                    {




                        measurers.map((contentlabel) => {
                            return (


                                <View style={styles.measurermodal} >
                                    <Textmedilux style={styles.measurerlabel} variant="labelLarge" label={contentlabel.alias} ></Textmedilux>


                                    <IconButton

                                        style={styles.icon}
                                        icon={require('../../Imagenes/medidores.png')}
                                        size={23}
                                        iconColor="#FFFFFF"

                                        onPress={() => {

                                            guardar(contentlabel)


                                        }}
                                    />



                                </View>
                            )






                        })




                    }


                </View>

            </ScrollView>


        </View>



    );

}

const styles = StyleSheet.create({
    view: { height: "200%", backgroundColor: "white" },


















    iconButton: {

        flex: 1,





    }



    ,


    icon: {

        backgroundColor: "#11D077",

        bordertoRadius: 2,


        height: "100%"

    },

    modallabel: {

        marginTop: "5%",

        textAlign: 'left',

        color: '#677185',


        fontSize: 18,



        fontFamily: "Outfit-Medium"





    },

    contentmeasurer: {

        flex: 1,
        marginTop: "5%",
        gap: 15
    },

    measurerlabel: {


        color: "#092D3D",

        fontFamily: "Outfit-SemiBold",


        fontSize: 15,
        marginHorizontal: "5%"



    },

    contentbutton: {


        marginTop: "2%",


        marginHorizontal: "5%",

        borderRadius: 15



    },

    button: {

        height: 50,
        borderRadius: 10,


    },

    buttonlabel: {


        fontFamily: "Outfit-Medium",

        fontSize: 18
    },




    contentmeasurermodal: {

        flex: 1,

        marginHorizontal: "5%"
    },

    measurermodal: {

        flexDirection: "row",

        justifyContent: "space-between",

        alignItems: "center",

        backgroundColor: "#FFFFFF",

        height: 60,

        borderRadius: 20,



    },

    measurerlabel: {


        color: "#092D3D",

        fontFamily: "Outfit-SemiBold",


        fontSize: 15,
        marginHorizontal: "5%"



    },

    logo: {
        color: "#A5ACB8",

        fontFamily: "Outfit-SemiBold",


        fontSize: 15,
        marginHorizontal: "5%"







    },

    measurercontent: {

        marginTop: "5%",

        flexDirection: "row",

    },

    mediluxlabel: {

        textAlign: 'center',

        color: '#092D3D',



        height: 50,


        marginTop: "5%",



        fontFamily: "Outfit-SemiBold"





    },


});
