import * as React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { Button, IconButton, Text } from "react-native-paper";
import { ScrollView } from 'react-native';
import { View } from 'react-native';












import Mediluxalert from '../Otros/Mediluxalert.js';
import Textmedilux from "../Otros/Textmedilux.js";
import { getmeasurers, getotp, gettransaction, otp, postemail, postnotificacion, postotp, posttoken, postuser } from "../Apicuentacontrato.js";
import { err } from "react-native-svg/lib/typescript/xml.js";
import { OtpInput } from "react-native-otp-entry";



export default function Notification({ navigation, route }) {



    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");

    const [token, setToken] = React.useState("");

    const [otp, setOtp] = React.useState("");

    const [medidor, setMedidor] = React.useState("");




    const [labelotp, setLabelotp] = React.useState("Enviar OTP");




    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }


    async function otpsend() {
        try {

            const user = await postemail(medidor);


            mediluxalert("Ok", "Se envio el codigo OTP");

        } catch (error) {

            console.log(error.response);


            mediluxalert("Error", "Hubo un error en el servidor");


        }
    }



    async function cargar() {
        try {



            setMedidor(route.params.email);

            console.log(medidorresponse);

        } catch (error) {

        }
    }


    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }

    async function guardar() {

        try {

            if (otp.length === 6) {

                setLabelotp("Enviando");


                await postotp(medidor, otp);



                await navigation.navigate('Edit', { email: medidor });


            }



            else {

                mediluxalert("Error", "No ha ingresado un codigo otp valido");


            }



        } catch (response) {


            console.log(response);



            if (response.response.data) {



                mediluxalert("Error", response.response.data.message);


            }
            else {

                mediluxalert("Error", "Ha ocurrido un error en el servidor");

            }



        }

        setLabelotp("Enviar OTP");
    }



    React.useEffect(() => {


        cargar();

        navigation.setOptions({
            headerShown: 0
        });

        return () => { };
    }, []);


    return (





        <View style={styles.iconButton}      >



            <Text style={styles.pago} variant="headlineSmall"    > Ingreso de OTP  </Text>







            <Textmedilux style={styles.logo} variant="titleMedium" label="Se ha enviado un código de identificación al correo registrado" ></Textmedilux>





            <View style={styles.otpmedilux}      >


                <OtpInput onTextChange={(otp) => {

                    setOtp(otp);

                }} autoFocusOnLoad={0} focusColor="#11D077" otpLength={6} />

            </View>





            <Button onPress={() => {

                otpsend();

            }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.logobutton} mode="text"  >
                ¿Recibiste el código?
            </Button>


            <Button disabled={labelotp === "Enviando"} loading={labelotp === "Enviando"} buttonColor="#11D077" onPress={() => {

                guardar();

            }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.labelbutton} mode="contained"  >
                {labelotp}
            </Button>

            <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} />



        </View>






    );

}

const styles = StyleSheet.create({
    view: {




        flex: 1, backgroundColor: "transparent"
    },














    card: {

        backgroundColor: "#FFFFFF",
        marginHorizontal: "10%",
        borderRadius: 5,
        marginTop: "5%"


    },


    label: {


        color: '#092D3D',

        margin: "5%",

        fontFamily: "Outfit-SemiBold",










    },


    logo: {
        marginTop: "15%",

        textAlign: 'center',



        color: '#092D3D',

        fontFamily: "Outfit-Medium",







    },


    logobutton: {


        color: '#11D077',
        fontFamily: "Outfit-Medium",




    },

    labelbutton: {


        fontFamily: "Outfit-Medium",




    },



    logo: {
        marginTop: 20,


        textAlign: 'center',



        color: '#092D3D',


        borderRadius: 10,

        width: "80%"

    },

    otpmedilux: {

        width: "80%",



        marginTop: "5%",


    },



    mediluxlabel: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginTop: "0%"

    },

    button: {



        height: 50,






    },

    mediluxcontent: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",
        borderBottomColor: "green",
        borderBottomWidth: 2

    },

    mediluxpago: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",


    },


    iconButton: {

        marginTop: "5%",

        alignItems: "center",
        justifyContent: "center",





    }



    ,

    medilux: {



        textAlign: 'center',

        color: '#092D3D',


        fontFamily: "Outfit-SemiBold"










    },


    mediluxcuenta: {



        textAlign: 'center',


        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },

    pago: {


        color: '#092D3D',




        fontFamily: "Outfit-SemiBold",










    },







    goBackButton: {
        marginLeft: "40%",
        justifyContent: "center",
    }
});
