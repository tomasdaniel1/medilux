import * as React from "react";
import { StyleSheet, SafeAreaView, BackHandler } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { ActivityIndicator, Card, IconButton, Text } from "react-native-paper";
import { Button } from "react-native-paper";
import { Image, View } from "react-native";
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Textmedilux from "../Otros/Textmedilux";
import { gethistoric, getmonto, gettarjetas, gettipo, posttarjeta } from "../Apicuentacontrato";
import { ScrollView } from "react-native-gesture-handler";
import Mediluxalert from "../Otros/Mediluxalert";











export default function Notificacion({ navigation, route }) {





    const [tarjetas, setTarjetas] = React.useState([]);

    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");

    const [token, setToken] = React.useState("");

    const [medidor, setMedidor] = React.useState([]);

    const [tarjeta, setTarjeta] = React.useState([]);

    const [screen, setScreen] = React.useState("");

    const [pago, setPago] = React.useState([]);

    const [tipo, setTipo] = React.useState("Guardar tarjeta");





    const [status, setStatus] = React.useState("enabled");


    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }








    React.useEffect(() => {

        BackHandler.addEventListener('hardwareBackPress', () => {



            if (navigation.isFocused()) {


                navigation.replace("Tarjeta", { medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), tarjeta: JSON.stringify(tarjeta), token: token });
                return 1;
            }




        });


    }, [pago]);




    React.useEffect(() => {





        setToken(route.params.token);

        setMedidor(JSON.parse(route.params.medidor));

        setTarjeta(JSON.parse(route.params.tarjeta));

        setPago(JSON.parse(route.params.pago));

        guardar(JSON.parse(route.params.tarjeta), JSON.parse(route.params.pago), route.params.token);

        navigation.setOptions({
            headerShown: 0
        });

        return () => { };
    }, []);







    async function guardarmonto(tipo) {

        try {


            setStatus("disabled");

            const tarjetasresponse = await getmonto(tarjeta, pago, tipo, token);

            console.log(tarjetasresponse.data);

            pago.medidorConsumo.monto = Number(tarjetasresponse.data.interest).toFixed(2);


            navigation.replace("Tarjeta", { medidor: JSON.stringify(medidor), tarjeta: JSON.stringify(tarjeta), pago: JSON.stringify(pago), tipo: JSON.stringify(tipo), token: token });











        } catch (error) {

            console.log(error);



            if (error.response) {

                if (error.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (error.response.data) {

                    console.log(error.response.data);





                    mediluxalert("Error", error.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }


        }

        setStatus("enabled");



    }

    async function guardar(tarjeta, pago, token) {

        try {


            setStatus("disabled");

            const tarjetasresponse = await gettipo(tarjeta.payerId, pago.medidorConsumo.totalPagar, token);

            console.log(tarjetasresponse.data);


            setTarjetas(tarjetasresponse.data.creditos);








        } catch (error) {


            console.log(error);



            if (error.response) {

                if (error.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (error.response.data) {

                    console.log(error.response.data);





                    mediluxalert("Error", error.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }

        }

        setStatus("enabled");



    }




    return (




        <View style={styles.view}>




            <View style={styles.iconButton}      >


                <IconButton
                    icon={require('../../Imagenes/credit.png')}
                    size={50}
                    iconColor='#08CE72'
                    onPress={() => {

                        navigation.replace("Tarjeta", { medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), tarjeta: JSON.stringify(tarjeta), token: token });



                    }}
                />
                <Text style={styles.pago} variant="headlineSmall"    >Tipo de pago  </Text>




            </View>





            <ScrollView style={styles.pendientes} >

                {status === "enabled" && (


                    tarjetas.map((label) => {
                        return (




                            <Card style={styles.cardpago} onPress={() => {




                                guardarmonto(label);

                            }}      >


                                <View style={styles.mediluxlabel}      >
                                    <Textmedilux style={styles.medilux} variant="labelLarge" label={label.descripcion} ></Textmedilux>







                                </View>

                                <View style={styles.mediluxlabel}      >
                                    <Textmedilux style={styles.medilux} variant="labelLarge" label={"Cuotas: " + label.cuotas} ></Textmedilux>






                                </View>





                            </Card>



                        )
                    })

                )}





                {status === "disabled" && (


                    <ActivityIndicator animating size='large' color="#11D077" />


                )}




            </ScrollView>



            <Mediluxalert hide={hide} label={label} content={content} screen={screen} setHide={setHide} />





        </View>




    );

}

const styles = StyleSheet.create({

    view: {

        flex: 1,

    },

    pago: {

        marginTop: "5%",
        marginBottom: "5%",
        color: "#FFFFFF",
        fontFamily: "Outfit-SemiBold"






    },

    alertpendiente: {

        marginTop: "5%",

        backgroundColor: "#FFFFFF",
        height: 200,
        justifyContent: "center",
        borderRadius: 10

    },

    pagoslabel: {
        flex: 1,

        borderBottomLeftRadius: 23,
        borderBottomRightRadius: 23,


        alignItems: "center",




        backgroundColor: "#092D3D",



    },
    tipolabel: {

        fontFamily: "Outfit-Medium",

        color: "#FFFFFF"


    },

    cardpago: {

        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        marginTop: "5%",

        borderColor: "#11D077",
        borderWidth: 1,

    },

    pagolabel: {

        alignItems: "center",
        marginBottom: "5%"



    },
    pagostipo: {

        flex: 1,


    },

    goBackButton: {

        width: '50%',
        height: '50%',

        aspectRatio: 0.6,
        resizeMode: 'contain',
        margin: 5



    }

    ,

    pago: {


        color: '#092D3D',



        marginLeft: "10%",

        fontFamily: "Outfit-SemiBold",










    },


    iconButton: {

        flexDirection: "row",
        alignItems: "center",





    },




    pagopendiente: {




        textAlign: 'left',


        color: "#11D077",


        fontFamily: "Outfit-Regular",






    },


    mediluxlabel: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginTop: "5%"

    },



    mediluxnotificacion: {




        color: "#A5ACB8",



        fontFamily: "Outfit-Medium",




        margin: "5%",

        alignSelf: 'center'



    },

    medilux: {



        textAlign: 'center',

        color: "#092D3D",


        fontFamily: "Outfit-SemiBold"










    },


    mediluxcuenta: {



        textAlign: 'center',


        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },



    logo: {
        marginTop: '3%',

        textAlign: 'center',


        color: '#092D3D',

        borderRadius: 10,

        width: "80%"

    },


    card: {

        backgroundColor: "#FFFFFF",
        marginHorizontal: "10%",
        borderRadius: 5,
        marginTop: "5%"


    },


    button: {



        height: 50,





    },

    labelpago: {



        fontFamily: "Outfit-Regular",





    },

    labelpagopendiente: {




        fontFamily: "Outfit-Regular",



        color: "#A5ACB8"


    },

    alertpago: {

        borderColor: "rgba(165, 172, 184, 0.5)",
        borderWidth: 1,

        width: "100%",
        flexDirection: "row",
        backgroundColor: "#E4EEED",
        alignItems: "center",
        marginTop: '5%',
        borderRadius: 10,






    },


    labelpendientepago: {

        gap: 10,
        marginHorizontal: "5%"





    },


    button: {



        height: 50,





    },
    imagen: {
        width: '23%',
        height: '23%',

        aspectRatio: 1.1,
        resizeMode: 'contain',
        margin: "5%"

    },

    pendientes: {



        marginHorizontal: "5%",



    }
});
