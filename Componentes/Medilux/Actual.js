import React from 'react';
import { View } from 'react-native';

import { StyleSheet } from 'react-native';
import PagerView from 'react-native-pager-view';
import { ActivityIndicator, Text } from 'react-native-paper';
import Textmedilux from '../Otros/Textmedilux.js';
import { Button } from 'react-native-paper';
import { Image } from 'react-native';
import { LineChart, BarChart } from "react-native-gifted-charts";



export default function Actual({ monthsconsumption, statuspago, maxvalue, medilux, navigation }) {








  console.log("maxvalue", maxvalue);







  return (




    <View style={styles.actual}      >



      <Textmedilux style={styles.label} variant="titleMedium" label="Pagos realizados"></Textmedilux>







      <View style={styles.cuentalabel}      >




        {
          medilux.map((label) => {
            return (

              <View style={styles.cuentapago}      >

                <View style={styles.cuentamedidor}      >
                  <Textmedilux style={styles.medilux} variant="labelLarge" label={label.label} ></Textmedilux>


                  <Image





                    source={require('../../Imagenes/medilux.png')}
                    style={styles.imagen}
                  />
                  <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={`${label.value} $`} ></Textmedilux>




                </View>

              </View>

            )






          })


        }


      </View>










      {statuspago === 1 && (




        <View style={styles.mediluxpago}      >


          <BarChart style={styles.mediluxpago} data={monthsconsumption}

            // maxValue={43}

            height={120} isAnimated animationDuration={1000}
            noOfSections={3} spacing={50} color={'#08CE72'} showYAxisIndices dataPointsColor={'#08CE72'} barBorderRadius={4} yAxisThickness={0}
            xAxisThickness={0} yAxisTextStyle={styles.labelchartmedilux} frontColor={'#08CE72'} barWidth={22} renderTooltip={(item, index) => {
              return (
                <View
                  style={{
                    marginBottom: 20,
                    marginLeft: -6,
                    backgroundColor: '#092D3D',
                    paddingHorizontal: 6,
                    paddingVertical: 4,
                    borderRadius: 4,

                  }}>
                  <Text style={styles.labelchart}    >{`${item.value} $`}</Text>


                </View>
              );
            }}

          />

        </View>



      )}

      {statuspago === 0 && (


        <ActivityIndicator animating size='large' color="#11D077" />


      )}

    </View>


  );
}









const styles = StyleSheet.create({



  medidor: {
    backgroundColor: "#092D3D",
    marginTop: 50,

    height: "20%",
    marginHorizontal: "10%",
    width: "80%",

  },

  mediluxpago: {

    backgroundColor: "rgba(255,255,255,1)",
    marginHorizontal: '2%',




    marginTop: '5%',
    borderRadius: 10,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    marginBottom: '5%',


  },



  pagos: {
    marginTop: 10,

    width: "100%",





  },


  changescreen: {


    flexDirection: "row",



  },

  imagen: {

    flex: 1,
    resizeMode: 'contain',
    aspectRatio: 0.5


  },

  buttonpagos: {
    backgroundColor: "white",
    width: "10%",
  },

  labelchartmedilux: {


    fontFamily: "Outfit-Medium",
    color: "#A5ACB8"

  },






  button: {
    marginHorizontal: '20%',
    marginTop: 10,

  },



  iconbutton: {

    flex: 1,



  },
  labelchart: {


    fontFamily: "Outfit-Regular",

    color: "#FFFFFF",








  },

  cuentamedidor: {
    backgroundColor: "#FFFFFF",
    flex: 1,

    borderRadius: 10,

    margin: "5%",


    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },

  cuentapago: {
    backgroundColor: "#FFFFFF",
    flex: 1,

    marginHorizontal: '2%',
    borderRadius: 10,


    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },

  cuentalabel: {

    marginTop: '2%',





    height: "29%",
    flexDirection: "row",

  },

  actual: {
    flex: 1,
    backgroundColor: "transparent",



    marginHorizontal: '5%',


  },


  label: {

    marginLeft: "2%",
    marginTop: '3%',
    color: "#092D3D",
    fontFamily: "Outfit-SemiBold"










  },


  mediluxcuenta: {



    textAlign: 'center',


    color: "#A5ACB8",



    fontFamily: "Outfit-Medium"



  },


  medilux: {



    textAlign: 'center',

    color: "#092D3D",


    fontFamily: "Outfit-SemiBold"










  },

  pagerView: {
    height: "20%",
  },
});




