import React from 'react';
import { View } from 'react-native';

import { StyleSheet } from 'react-native';


import { FormBuilder } from 'react-native-paper-form-builder';
import { useForm } from 'react-hook-form';
import { TextInput } from 'react-native-paper';
import { formatdate, format } from '../Api.js';
import { Button } from 'react-native-paper';

import Textmedilux from '../Otros/Textmedilux.js';

import { RNCamera } from 'react-native-camera';


import { Modal, Portal, IconButton, Text } from 'react-native-paper';
import { Image } from 'react-native';

import Mediluxalert from '../Otros/Mediluxalert.js';
import { gettipos, postincidencia } from '../Apicuentacontrato.js';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';





export default function Incidents({ navigation, route }) {


  const [hide, setHide] = React.useState(0);
  const [label, setLabel] = React.useState("");
  const [content, setContent] = React.useState("");



  const { control, setFocus, handleSubmit } = useForm({
    defaultValues: {
      incidencia: '',
      measurer: '',
      tipo: ''
    },
    mode: 'onChange',
  });

  const cameraRef = React.useRef();

  const [modal, setModal] = React.useState(0);

  const [uri, setUri] = React.useState(null);



  const [token, setToken] = React.useState("");

  const [tipos, setTipos] = React.useState([]);


  const [screen, setScreen] = React.useState(null);




  const [status, setStatus] = React.useState("enabled");


  const [measurers, setMeasurers] = React.useState([]);

  const [open, setOpen] = React.useState(0);

  const [incidencia, setIncidencia] = React.useState(moment(Date.now()).format("YYYY-MM-DD HH:mm:ss"));

  const [indicator, setIndicator] = React.useState(0);








  async function guardar(token) {

    try {




      const tarjetasresponse = await gettipos(token);


      const tipostarjeta = tarjetasresponse.data.map((tipo) => {


        return {
          label: tipo.motivo,
          value: tipo.idTipoIncidencia,
        };
      });



      let measurerstipo = JSON.parse(route.params.measurers).map((tipo) => {


        return {
          label: tipo.numeroMedidor,
          value: tipo.id,
        };
      });


      setTipos(tipostarjeta);

      setMeasurers(measurerstipo);








    } catch (error) {
      console.log(error);



      if (error.response) {

        if (error.response.status === 401) {

          mediluxalert("Error", "El token de sesión ha expirado");


        }

        else if (error.response.data) {

          console.log(error.response.data);





          mediluxalert("Error", error.response.data.message);






        }

      }


      else {

        mediluxalert("Error", "Hubo un error en el servidor ");


      }





    }

  }

  async function guardarincidencia(data) {

    try {

      console.log(uri);
      setStatus("disabled");



      const incidenciaresponse = await postincidencia(uri, data, incidencia, token);


      console.log(incidenciaresponse);


      // setTipos(tipostarjeta);

      // setMeasurers(measurerstipo);

      setScreen("Medilux");



      mediluxalert("¡Listo!", "Se guardo la incidencia");





    } catch (error) {


      if (error.response.data) {

        console.log(error.response.data);


        if (error.response.status === 401) {


          setScreen("Login");

          mediluxalert("Error", "El token de sesión ha expirado");



        }
        else {

          mediluxalert("Error", error.response.data.message);


        }



      }
      else {

        mediluxalert("Error", "Hubo un error en el servidor ");


      }

      console.log(error.response.data);





    }

    setStatus("enabled");


  }



  async function grabar() {
    try {




      const data = await cameraRef.current.takePictureAsync({ quality: 0.5 });

      setUri(data.uri);

      setModal(0);


      setIndicator(1);






      console.log(data.uri);















    } catch (error) {



    }
  }








  React.useEffect(() => {

    setToken(route.params.token);



    guardar(route.params.token);




    navigation.setOptions({ headerShown: 0 });








    return () => {

    };
  }, []);



  function mediluxalert(label, content) {


    setHide(1);



    setLabel(label);
    setContent(content);





  }





  return (


    <View style={styles.incidencialabel}    >






      <View style={styles.iconButton}      >


        <IconButton
          icon={require('../../Imagenes/credit.png')}
          size={50}
          iconColor='#08CE72'
          onPress={() => {

            navigation.goBack();




          }}
        />
        <Text style={styles.pago} variant="headlineSmall"    >Incidencias</Text>

        <IconButton
          size={50}
          iconColor='#08CE72'
          onPress={() => {

            navigation.goBack();




          }}
        />







      </View>

      <View style={styles.incidencia}    >


        <DatePicker
          locale='es'
          modal
          date={new Date()}
          open={open}
          onConfirm={(date) => {

            setOpen(0);

            console.log(date);

            setIncidencia(moment(date).format('MM-DD-YYYY HH:mm:ss'));


          }}
          onCancel={() => {

            setOpen(0);
          }}

          title="Seleccione el dia y la hora"

          confirmText="Confirmar"

          cancelText="Cancelar"

        />

        <FormBuilder
          control={control}
          setFocus={setFocus}
          formConfigArray={[

            {
              name: 'fecha',
              type: 'text',

              textInputProps: {
                label: <Textmedilux style={styles.label} variant="titleMedium" label={incidencia} ></Textmedilux>,
                disabled: 'Password' === 'Password',
                left: <TextInput.Icon icon={require('../../Imagenes/calendar.png')} onPress={() => {

                  setOpen(1);
                }} />,
                style: styles.label,
                theme: styles.texttheme




              },
            },

            {
              name: 'tipo',
              type: 'select',
              rules: {
                required: {
                  value: 1,
                  message: 'Debe seleccionar un tipo de incidencia',
                  style: styles.label

                },
              },







              textInputProps: {

                left: <TextInput.Icon icon={require('../../Imagenes/id.png')} />,
                label: <Textmedilux style={styles.label} variant="titleMedium" label='Seleccione un tipo de incidencia *' ></Textmedilux>,
                contentStyle: styles.incidentid,
                theme: styles.texttheme

              },
              options: tipos
            },



            {
              name: 'measurer',
              type: 'select',
              rules: {
                required: {
                  value: 1,
                  message: 'Debe seleccionar un medidor',
                  style: styles.label

                },
              },







              textInputProps: {

                left: <TextInput.Icon icon={require('../../Imagenes/measurer.png')} />,
                theme: styles.texttheme,
                label: <Textmedilux style={styles.label} variant="titleMedium" label='Seleccione un medidor *' ></Textmedilux>,
                contentStyle: styles.incidentid
              },
              options: measurers
            },



















            {
              name: 'incidencia',
              type: 'text',
              rules: {
                required: {
                  value: 1,
                  message: 'Debe poner una incidencia',
                }
              },
              textInputProps: {
                activeOutlineColor: '#11D077',
                underlineColor: '#11D077',
                theme: styles.texttheme,

                left: <TextInput.Icon icon={require('../../Imagenes/incident.png')} />,
                label: <Textmedilux style={styles.label} variant="titleMedium" label='Incidencia' ></Textmedilux>,
                contentStyle: styles.incident,
                multiline: 'Password' === 'Password'
              }







            }]} />

        {uri && (

          <Button buttonColor="#FFFFFF" style={styles.logopago} contentStyle={styles.button} labelStyle={styles.labelbutton} textColor='#092D3D' mode="contained" onPress={() => { setModal(1); }} >
            Tomar fotografia de la incidencia
          </Button>

        )}

        {uri === null && (

          <Button buttonColor="#FFFFFF" style={styles.logo} contentStyle={styles.button} labelStyle={styles.labelbutton} textColor='#092D3D' mode="contained" onPress={() => { setModal(1); }} >
            Tomar fotografia de la incidencia
          </Button>

        )}


        <Button buttonColor="#11D077" disabled={status === "disabled"} loading={status === "disabled"} labelStyle={styles.labelbutton} onPress={handleSubmit((data: any) => {
          guardarincidencia(data);
        })} style={styles.logo} contentStyle={styles.button} mode="contained"  >
          Enviar incidencia
        </Button>


        <Portal style={styles.portal} >
          <Modal visible={modal} style={styles.modal} onDismiss={() => { setModal(0); }}  >


            <View style={styles.modallabel}       >


              <View style={styles.iconButton}      >



                <Text style={styles.pago} variant="headlineSmall"    >  Incidencia </Text>


              </View>





              <RNCamera style={styles.rnCamera} ref={cameraRef} />

              <Button buttonColor="#FFFFFF" style={styles.modalbutton} contentStyle={styles.button} labelStyle={styles.labelbutton} textColor='#092D3D' mode="contained" onPress={grabar} >
                Tomar fotografía  de la incidencia
              </Button>


            </View>


          </Modal>
        </Portal>


        <Portal style={styles.portal} >
          <Modal visible={indicator} style={styles.modal} onDismiss={() => setIndicator(0)}  >













            <View style={styles.modallabel}       >


              <View style={styles.iconButton}      >



                <Text style={styles.pago} variant="headlineSmall"    > Incidencia  </Text>


              </View>

              <Image
                style={styles.mediluxlogo}
                source={{
                  uri: uri,
                }}
              />



              <View style={styles.buttons}    >


                <Button buttonColor="#FFFFFF" style={styles.logo} contentStyle={styles.button} labelStyle={styles.labelbutton} textColor='#092D3D' mode="contained" onPress={(() => {
                  setIndicator(0);
                })} >
                  Guardar
                </Button>
                <Button buttonColor="#11D077" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbutton} mode="contained" onPress={(() => {
                  setIndicator(0);
                  setModal(1)
                })} >
                  Tomar de nuevo
                </Button>
              </View>


            </View>





          </Modal>
        </Portal>

      </View>



      <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} screen={screen} navigation={navigation} />


    </View>



  );
}







const styles = StyleSheet.create({




  formbutton: {
    marginTop: 50,

  },

  incident: {
    height: 100,
    fontFamily: "Outfit-Regular",
  },


  incidentid: {
    fontFamily: "Outfit-Regular",
  },
  button: {



    height: 50,








  },

  texttheme: {

    roundness: 10,


    colors: {

      background:

        "#FFFFFF"

    }


  },


  label: {




    color: '#092D3D',


    fontFamily: "Outfit-Medium",

    backgroundColor: "#FFFFFF"



  },
  mediluxlabel: {

    textAlign: 'center',

    color: '#092D3D',



    height: 50,


    marginTop: "5%",



    fontFamily: "Outfit-SemiBold"





  },
  modalbutton: {


    marginTop: "37%",




  },
  portal: {


  },
  mediluxlogo: {
    width: "100%",
    height: "60%",
  },
  labelbutton: {


    fontFamily: "Outfit-Regular",


  },

  buttonlogo: {

    textAlign: 'center',


    color: '#092D3D',

    borderRadius: 10,

    marginTop: "30%",




  },

  iconButton: {

    flexDirection: "row",
    alignItems: "center",

    justifyContent: "space-between"





  },


  pago: {


    color: '#092D3D',




    fontFamily: "Outfit-SemiBold",













  },



  pagobutton: {





    marginRight: "23%",











  },

  incidencialabel: {



    flex: 1,







  },
  logopago: {
    marginTop: "5.9%",

    textAlign: 'center',


    color: '#092D3D',

    borderRadius: 10,


    borderColor: "#11D077",

    borderWidth: 2





  },

  logo: {
    marginTop: "5.9%",

    textAlign: 'center',


    color: '#092D3D',

    borderRadius: 10,







  },

  modal: {

    backgroundColor: '#F5F5F5',







  },

  modallabel: {

    marginHorizontal: '5%',
    justifyContent: "center",




  },
  rnCamera: {

    height: "53.8%",



  },

  buttonlabel: {

    textAlign: 'center',
    borderRadius: 10,
    marginHorizontal: '1%',
    marginTop: "5.9%"







  }
  ,
  buttons: {

    marginTop: "1%"




  },

  incidencia: {



    flex: 1,


    marginHorizontal: "5%",





  },
});

