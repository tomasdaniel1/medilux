import * as React from "react";
import { StyleSheet, SafeAreaView, BackHandler } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { ActivityIndicator, Card, IconButton, Modal, Portal, Text } from "react-native-paper";
import { Button } from "react-native-paper";
import { Image, View } from "react-native";
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Textmedilux from "../Otros/Textmedilux";
import { deletetipo, gethistoric, getincidencias, gettarjetas, posttarjeta, updatetipo } from "../Apicuentacontrato";
import { ScrollView } from "react-native-gesture-handler";
import Mediluxalert from "../Otros/Mediluxalert";
import moment from "moment";











export default function Incidencia({ navigation, route }) {





    const [tarjetas, setTarjetas] = React.useState([]);

    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");

    const [token, setToken] = React.useState("");

    const [medidor, setMedidor] = React.useState([]);

    const [modalstatus, setModalstatus] = React.useState(0);

    const [modaltarjeta, setModaltarjeta] = React.useState("");

    const [screen, setScreen] = React.useState(null);

    const [measurers, setMeasurers] = React.useState([]);

    const [incidencia, setIncidencia] = React.useState([]);



    const [pago, setPago] = React.useState([]);

    const [status, setStatus] = React.useState("enabled");


    const [tipo, setTipo] = React.useState("Generar incidencia");



    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }



    async function guardartarjeta() {
        try {

            setTipo("Guardando");

            const tarjeta = await posttarjeta(token);

            console.log(tarjeta.data);

            navigation.navigate("PTP", { medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), tarjeta: JSON.stringify(tarjeta.data), token: token });


        } catch (error) {

            console.log(error.response.data);

            mediluxalert("Error", "Hubo un error en el servidor ");


        }

        setTipo("Guardar tarjeta");

    }






    React.useEffect(() => {


        setToken(route.params.token);

        setMeasurers(JSON.parse(route.params.measurers));



        guardar(route.params.token);

        navigation.setOptions({
            headerShown: 0
        });

        return () => { };
    }, []);


    async function guardar(token) {

        try {


            setStatus("disabled");

            const tarjetasresponse = await getincidencias(token);

            console.log(tarjetasresponse.data);


            setTarjetas(tarjetasresponse.data);








        } catch (error) {


            console.log(error);



            if (error.response) {

                if (error.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (error.response.data) {

                    console.log(error.response.data);





                    mediluxalert("Error", error.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }


        }


        setStatus("enabled");



    }


    React.useEffect(() => {

        BackHandler.addEventListener('hardwareBackPress', () => {



            if (navigation.isFocused()) {


                navigation.navigate("Medilux");
                return 1;
            }




        });


    }, []);




    return (




        <View style={styles.view}>






            <View style={styles.iconButton}      >


                <IconButton
                    icon={require('../../Imagenes/credit.png')}
                    size={50}
                    iconColor='#08CE72'
                    onPress={() => {

                        navigation.navigate("Medilux");




                    }}
                />
                <Text style={styles.pago} variant="headlineSmall"    >Incidencias</Text>

                <IconButton

                />







            </View>

            {status === "enabled" && (


                <View>




                    <Button buttonColor="#11D077" labelStyle={styles.labelbuttonpago} disabled={tipo === "Guardando"} loading={tipo === "Guardando"} onPress={() => {




                        setTipo("Guardando");

                        navigation.navigate("Incidents", { token: token, measurers: JSON.stringify(measurers) });


                        setTipo("Generar incidencia");







                    }} style={styles.buttonpago} contentStyle={styles.button} mode="contained"  >
                        {tipo}
                    </Button>





                    <ScrollView style={styles.pendientes} >











                        {tarjetas.length === 0 && (



                            <Textmedilux style={styles.mediluxnotificacion} variant="titleLarge" label="No tiene incidencias" ></Textmedilux>

                        )}

                        {tarjetas.map((label) => {
                            return (


                                <View>


                                    <Card style={styles.cardpago}  >


                                        <View style={styles.modalpago}      >
                                            <Textmedilux style={styles.medilux} variant="labelLarge" label={label.motivo} ></Textmedilux>







                                        </View>

                                        <View style={styles.modalpago}      >
                                            <Textmedilux style={styles.medilux} variant="labelLarge" label={moment.format(label.fechaSuceso).format("YYYY-MM-DD HH:mm:ss")} ></Textmedilux>






                                        </View>

                                        <View style={styles.tipopago}      >



                                            <Button buttonColor="#11D077" labelStyle={styles.labelbuttonpago} onPress={() => {



                                                setIncidencia(label);
                                                setModalstatus(1);





                                            }} style={styles.buttontipo} contentStyle={styles.button} mode="contained"  >
                                                Información
                                            </Button>



                                        </View>



                                    </Card>






                                </View>


                            )
                        })


                        }



                    </ScrollView>

                </View>

            )}


            {status === "disabled" && (


                <ActivityIndicator animating size='large' color="#11D077" />


            )}








            <Portal>



                <Modal visible={modalstatus} onDismiss={(() => {

                    setModalstatus(0);

                })} >

                    <View style={styles.modal}   >






                        <ScrollView style={styles.mediluxpago}      >





                            <View style={styles.mediluxmodal}      >


                                <Textmedilux style={styles.medilux} variant="labelLarge" label={incidencia.numeroTicket} ></Textmedilux>



                                <Textmedilux style={styles.medilux} variant="labelLarge" label={incidencia.motivo} ></Textmedilux>


                                <View style={styles.mediluxlabel}      >




                                    <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={moment(incidencia.fechaSuceso).format("YYYY-MM-DD HH:mm:ss")} ></Textmedilux>


                                    <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={"Nro de medidor:" + incidencia.cuentaContrato} ></Textmedilux>



                                    <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={incidencia.descripcion} ></Textmedilux>



                                </View>


                            </View>



                        </ScrollView>





                    </View >





                </Modal >

            </Portal >

            <Portal >


                <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} screen={screen} navigation={navigation} />

            </Portal >



        </View>




    );

}

const styles = StyleSheet.create({

    view: {

        flex: 1,

    },

    pago: {

        marginTop: "5%",
        marginBottom: "5%",
        color: "#FFFFFF",
        fontFamily: "Outfit-SemiBold"






    },

    alertpendiente: {

        marginTop: "5%",

        backgroundColor: "#FFFFFF",
        height: 200,
        justifyContent: "center",
        borderRadius: 10

    },

    pagoslabel: {
        flex: 1,

        borderBottomLeftRadius: 23,
        borderBottomRightRadius: 23,


        alignItems: "center",




        backgroundColor: "#092D3D",



    },
    tipolabel: {

        fontFamily: "Outfit-Medium",

        color: "#FFFFFF"


    },
    pagolabel: {

        alignItems: "center",
        marginBottom: "5%"



    },


    mediluxnotificacion: {




        color: "#A5ACB8",



        fontFamily: "Outfit-Medium",




        margin: "5%",

        alignSelf: 'center'



    },

    pagostipo: {

        flex: 1,


    },

    goBackButton: {

        width: '50%',
        height: '50%',

        aspectRatio: 0.6,
        resizeMode: 'contain',
        margin: 5



    }

    ,

    pago: {


        color: '#092D3D',




        fontFamily: "Outfit-SemiBold",










    },

    iconButton: {

        flexDirection: "row",
        alignItems: "center",

        justifyContent: "space-between"





    },


    cardpago: {

        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        marginTop: "5%",

        borderColor: "#11D077",
        borderWidth: 1,

    },



    pagopendiente: {




        textAlign: 'left',


        color: "#11D077",


        fontFamily: "Outfit-Regular",






    },


    modalpago: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginBottom: "0%",
        justifyContent: "center"

    },

    medilux: {




        color: '#092D3D',


        fontFamily: "Outfit-SemiBold"










    },



    tipopago: {

        margin: "5%",

        marginTop: "0%",

        justifyContent: "center",


    },

    labelbuttonpago: {

        fontFamily: "Outfit-Medium",

        fontSize: 18
    },


    mediluxcuenta: {





        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },



    modal: {

        backgroundColor: "#F5F8F9",


        justifyContent: 'center',



        borderRadius: 10,





        marginHorizontal: "5%",




    },









    buttonlabel: {

        borderRadius: 10,
        marginTop: "5.9%"







    },






    button: {



        height: 50,













    },
    labelbuttonmedilux: {

        alignSelf: "center",



        fontFamily: "Outfit-Regular",


    },

    mediluxlabelcuenta: {


        fontSize: 20,


        fontFamily: "Outfit-Regular",



        color: "#A5ACB8"




    },

    mediluxmodal: {


        margin: "10%",



        gap: 5

    },


    mediluxlabel: {


        marginTop: "5%",

        height: "100%",


        gap: 5

    },


    modaltarjeta: {


        flexDirection: 'row',

        justifyContent: 'center',

        alignItems: 'center',

        gap: 53,




    },



    logo: {
        marginTop: '3%',

        textAlign: 'center',


        color: '#092D3D',

        borderRadius: 10,

        width: "80%"

    },


    card: {

        backgroundColor: "#FFFFFF",
        marginHorizontal: "10%",
        borderRadius: 5,
        marginTop: "5%",
        justifyContent: "center"


    },






    labelpago: {



        fontFamily: "Outfit-Regular",





    },


    labelpagopendiente: {




        fontFamily: "Outfit-Regular",



        color: "#A5ACB8"


    },

    alertpago: {

        borderColor: "rgba(165, 172, 184, 0.5)",
        borderWidth: 1,

        width: "100%",
        flexDirection: "row",
        backgroundColor: "#E4EEED",
        alignItems: "center",
        marginTop: '5%',
        borderRadius: 10,






    },


    labelpendientepago: {

        gap: 10,
        marginHorizontal: "5%"





    },

    buttonpago: {

        marginTop: "5%",

        borderRadius: 10,

        marginHorizontal: "5%",


    },

    buttontipo: {

        marginTop: "5%",

        flex: 1

    },


    button: {



        height: 50,





    },
    imagen: {
        width: '23%',
        height: '23%',

        aspectRatio: 1.1,
        resizeMode: 'contain',
        margin: "5%"

    },

    pendientes: {



        marginHorizontal: "5%",



    }
});
