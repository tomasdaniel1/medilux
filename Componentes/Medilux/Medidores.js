import * as React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { Button, IconButton, Text, TextInput } from "react-native-paper";
import { ScrollView } from 'react-native';
import { View } from 'react-native';












import Mediluxalert from '../Otros/Mediluxalert.js';
import Textmedilux from "../Otros/Textmedilux.js";
import { FormBuilder } from "react-native-paper-form-builder";
import { useForm } from 'react-hook-form';
import { getmeasurers, gettransaction, postmeasurer, updatemeasurer } from "../Apicuentacontrato.js";



export default function Medidores({ navigation }) {




    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");

    const [measurer, setMeasurer] = React.useState([]);
    const [token, setToken] = React.useState("");

    const [status, setStatus] = React.useState("enabled");



    const { control, setFocus, handleSubmit } = useForm({
        defaultValues: {
            medidor: '',
            cuenta: '',

        },
        mode: 'onSubmit',
    });



    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }

    async function update(data) {

        try {

            setStatus("disabled");

            const update = await postmeasurer(data, token);

            console.log("update", update);

            const measurers = await getmeasurers(token);

            await EncryptedStorage.setItem("measurers", JSON.stringify(measurers.data));


            navigation.navigate("Medilux");

        }

        catch (err) {

            console.log(err);

            if (err.response) {

                mediluxalert("Error", err.response.data.message);


            }
            else {

                mediluxalert("Error", "Hubo un error en el servidor ");

            }




        }

        setStatus("enabled");

    }


    async function guardar() {

        const measurerresponse = await JSON.parse(await EncryptedStorage.getItem("measurer"));

        const tokenresponse = await EncryptedStorage.getItem("token");

        setMeasurer(measurerresponse);

        setToken(tokenresponse)



    }

    React.useEffect(() => {



        guardar();

        navigation.setOptions({ headerShown: 0 });








        return () => {

        };
    }, []);



    return (


        <View style={styles.view}      >


            <View style={styles.iconButton}      >



                <Text style={styles.pago} variant="headlineSmall"    > {"Bienvenido a Medilux "}   </Text>

                <Text style={styles.mediluxlabelcuenta} variant="labelLarge"    > {"Por favor, ponga un número de cuenta contrato para usar la aplicación "}   </Text>




            </View>



            <View style={styles.labelpago}      >




                <FormBuilder
                    control={control}
                    setFocus={setFocus}
                    formConfigArray={[

                        {
                            type: "text",
                            name: 'measurer',







                            rules: {

                                required: {
                                    value: 1,
                                    message: 'No puede dejar este campo vacio',
                                },





                            },



                            textInputProps: {

                                label: <Textmedilux style={styles.mediluxlabelcuenta} variant="labelLarge" label='Nombre de medidor' ></Textmedilux>,

                                activeOutlineColor: '#11D077',
                                textColor: '#092D3D',
                                underlineColor: 'transparent',
                                outlineColor: 'transparent',
                                contentStyle: styles.labelcontent,
                                theme: styles.texttheme

                            },
                        },

                        {
                            type: "text",
                            name: 'cuenta',

















                            rules: {

                                required: {
                                    value: 1,
                                    message: 'No puede dejar este campo vacio',
                                },



                                pattern: {
                                    value:
                                        /^0|[1-9]\d*$/,
                                    message: 'Ingrese una cuenta contrato valido',
                                },



                            },

                            textInputProps: {

                                label: <Textmedilux style={styles.mediluxlabelcuenta} variant="labelLarge" label='Cuenta contrato' ></Textmedilux>,

                                keyboardType: 'numeric',
                                activeOutlineColor: '#11D077',
                                textColor: '#092D3D',
                                underlineColor: 'transparent',
                                outlineColor: 'transparent',
                                contentStyle: styles.labelcontent,
                                theme: styles.texttheme

                            },



                        },
                    ]}
                />



                <View style={styles.buttonedit}      >



                    <Button disabled={status === "disabled"} loading={status === "disabled"} buttonColor="#11D077" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbutton} mode="contained" onPress={handleSubmit((data: any) => {



                        update(data);
                    })} >
                        Crear
                    </Button>
                </View>



            </View >




            <Mediluxalert hide={hide} label={label} content={content} setHide={setHide} navigation={navigation} />


        </View >






    );

}

const styles = StyleSheet.create({
    view: {



        margin: "5%",


        gap: 30,




        flex: 1, backgroundColor: "transparent"
    },














    card: {

        backgroundColor: "#FFFFFF",
        marginHorizontal: "10%",
        borderRadius: 5,
        marginTop: "5%"


    },
    buttonlabel: {

        borderRadius: 10,
        marginTop: "5.9%"







    },

    buttonedit: {


    },

    label: {


        color: '#092D3D',

        margin: "5%",

        fontFamily: "Outfit-SemiBold",










    },

    labelpago: {

        marginHorizontal: "5%"

    },

    mediluxlabel: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginTop: "0%"

    },

    mediluxcontent: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",
        borderBottomColor: "green",
        borderBottomWidth: 2

    },

    mediluxpago: {

        flexDirection: "row",
        justifyContent: "space-between",
        margin: "5%",
        marginTop: "0%",


    },

    button: {



        height: 50,













    },
    labelbutton: {



        fontSize: 17,


        fontFamily: "Outfit-Medium",


        color: "#FFFFFF",




    },

    measurermodal: {

        justifyContent: "center",


        backgroundColor: "#FFFFFF",

        height: 50,

        borderRadius: 10,



    },
    iconButton: {

        marginHorizontal: "5%",


        gap: 31,



        justifyContent: "center",


        alignItems: "center",





    },
    logo: {
        marginTop: "15%",

        textAlign: 'center',


        color: '#092D3D',

        fontFamily: "Outfit-Medium",

        backgroundColor: "#FFFFFF"






    },

    measurerlabel: {




        color: "#092D3D",

        fontFamily: "Outfit-SemiBold",


        fontSize: 15,
        marginHorizontal: "5%"



    },

    texttheme: {

        roundness: 10,


        colors: {

            background:

                "#FFFFFF"

        }


    },



    medilux: {



        textAlign: 'center',

        color: "#092D3D",


        fontFamily: "Outfit-SemiBold"










    },
    mediluxlabelcuenta: {


        fontSize: 20,


        fontFamily: "Outfit-Regular",



        color: "#A5ACB8"




    },

    mediluxcuenta: {



        textAlign: 'center',


        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },

    pago: {


        color: '#092D3D',




        fontFamily: "Outfit-SemiBold",










    },







    goBackButton: {
        marginLeft: "40%",
        justifyContent: "center",
    }
});
