import React from 'react';
import { View } from 'react-native';

import { StyleSheet } from 'react-native';
import { Image } from 'react-native';
import Textmedilux from '../Otros/Textmedilux.js';
import { Button, Modal, Portal } from 'react-native-paper';
import EncryptedStorage from "react-native-encrypted-storage";





export default function Cuenta({ email, measurers, navigation, token }) {








  const [modalstatus, setModalstatus] = React.useState(0);





  function guardar() {


    setModalstatus(0);


    EncryptedStorage.clear();
    navigation.replace("Login");





  }


  console.log(measurers);












  return (

    <View style={styles.cuenta}  >









      <View style={styles.imagenlogo}
      >

        <Textmedilux style={styles.mediluxcuenta} variant="headlineSmall" label='Usuario' ></Textmedilux>

        <Image

          source={require('../../Imagenes/user-profile.png')}
          style={styles.imagen}
        />

        <Textmedilux style={styles.mediluxcuenta} variant="titleMedium" label={email} ></Textmedilux>






      </View >

















      <Button buttonColor="#FFFFFF" onPress={(() => {

        navigation.replace("Incidencia", { token: token, measurers: JSON.stringify(measurers) });

      })} style={styles.logo} contentStyle={styles.button} labelStyle={styles.labelbutton} mode="contained"  >
        Soporte
      </Button>

      <Button buttonColor="#FFFFFF" onPress={(() => {

        navigation.navigate("FAQ");

      })} style={styles.logo} contentStyle={styles.button} labelStyle={styles.labelbutton} mode="contained"  >
        FAQ
      </Button>

      <Button buttonColor="#FFFFFF" onPress={(() => {

        navigation.navigate("Pagado", { token: token });

      })} style={styles.logo} contentStyle={styles.button} labelStyle={styles.labelbutton} mode="contained"  >
        Información de pagos
      </Button>
      <Button buttonColor="#FFFFFF" onPress={(() => {

        setModalstatus(1);

      })} style={styles.logo} labelStyle={styles.labelbutton} contentStyle={styles.button} mode="contained"  >
        Cerrar sesión
      </Button>






      <Portal>



        <Modal visible={modalstatus} onDismiss={(() => {

          setModalstatus(0);

        })} >

          <View style={styles.modal}   >


            <Textmedilux style={styles.mediluxlabelcuenta} variant="labelLarge" label='¿Está seguro?' ></Textmedilux>


            <View style={styles.modaltarjeta}   >


              <Button buttonColor="#11D077" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbuttonmedilux} mode="contained" onPress={(() => {

                guardar();


              })} >
                Confirmar
              </Button>

              <Button buttonColor="#FFFFFF" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbuttonmedilux} textColor='#092D3D' mode="contained" onPress={(() => {

                setModalstatus(0);
              })} >
                Cancelar
              </Button>


            </View >

          </View >





        </Modal >

      </Portal >



    </View >

  );
}












const styles = StyleSheet.create({





  cuenta: {
    alignItems: "center",

  },





  imagen: {
    width: '50%',
    height: '50%',

    aspectRatio: 1.5,
    resizeMode: 'contain',
    margin: 5,
    marginTop: "5%",





  },


  imagenlogo: {
    justifyContent: "center",
    alignItems: "center",
    width: '100%',
    height: '53%',
    backgroundColor: '#11D077',
    borderBottomLeftRadius: 23,
    borderBottomRightRadius: 23


  },


  mediluxcuenta: {

    marginTop: "5%",



    textAlign: 'center',


    color: "#FFFFFF",

    fontFamily: "Outfit-Regular",








  },


  logo: {
    marginTop: "3%",



    height: 50,




    color: '#092D3D',



    width: "90%"

  },


  modal: {

    backgroundColor: "#F5F8F9",


    justifyContent: 'center',

    alignItems: 'center',


    borderRadius: 10,





    marginHorizontal: "5%",

    height: "45%",



  },









  buttonlabel: {

    borderRadius: 10,
    flex: 1,







  },






  button: {



    height: 50,













  },
  labelbuttonmedilux: {

    alignSelf: "center",



    fontFamily: "Outfit-Regular",


  },

  mediluxlabelcuenta: {


    marginTop: "5%",


    fontSize: 20,


    fontFamily: "Outfit-Regular",



    color: '#092D3D',




  },


  modaltarjeta: {

    margin: "5%",


    flexDirection: 'row',



    gap: 15,




  },


  labelbutton: {



    color: '#092D3D',
    fontFamily: "Outfit-Medium",


    fontSize: 16

  },


  button: {

    marginTop: "1.5%",

    justifyContent: "center",

    borderRadius: 15,










  }
});




