import * as React from "react";
import { StyleSheet, SafeAreaView, BackHandler } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { ActivityIndicator, Card, IconButton, Modal, Portal, Text } from "react-native-paper";
import { Button } from "react-native-paper";
import { Image, View } from "react-native";
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Textmedilux from "../Otros/Textmedilux";
import { deletetipo, gethistoric, gettarjetas, posttarjeta, updatetipo } from "../Apicuentacontrato";
import { ScrollView } from "react-native-gesture-handler";
import Mediluxalert from "../Otros/Mediluxalert";











export default function Tipo({ navigation, route }) {





    const [tarjetas, setTarjetas] = React.useState([]);

    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");

    const [token, setToken] = React.useState("");

    const [medidor, setMedidor] = React.useState([]);

    const [modalstatus, setModalstatus] = React.useState(0);

    const [modaltarjeta, setModaltarjeta] = React.useState("");

    const [screen, setScreen] = React.useState(null);



    const [pago, setPago] = React.useState([]);

    const [status, setStatus] = React.useState("enabled");


    const [tipo, setTipo] = React.useState("Registrar tarjeta");



    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }



    async function guardartarjeta() {
        try {

            setTipo("Guardando");

            const tarjeta = await posttarjeta(token);

            console.log(tarjeta.data);

            navigation.navigate("PTP", { medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), tarjeta: JSON.stringify(tarjeta.data), token: token });


        } catch (error) {


            console.log(error);



            if (error.response) {

                if (error.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (error.response.data) {

                    console.log(error.response.data);





                    mediluxalert("Error", error.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }




        }

        setTipo("Guardar tarjeta");

    }














    async function tarjetadelete(tarjetalabel) {
        try {

            setModalstatus(0);

            setStatus("disabled");

            const tarjeta = await deletetipo(tarjetalabel, token);


            mediluxalert("¡Listo!", "Se removio la tarjeta")


        } catch (error) {

            console.log(error);



            if (error.response) {

                if (error.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (error.response.data) {

                    console.log(error.response.data);





                    mediluxalert("Error", error.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }




        }


        setStatus("enabled");


    }

    async function tarjetaupdate(tarjetalabel) {
        try {


            const tarjeta = await updatetipo(tarjetalabel, token);


            navigation.navigate("PTP", { medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), tarjeta: JSON.stringify(tarjeta.data), token: token });


        } catch (error) {

            console.log(error);



            if (error.response) {

                if (error.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (error.response.data) {

                    console.log(error.response.data);





                    mediluxalert("Error", error.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }




        }

    }


    React.useEffect(() => {

        BackHandler.addEventListener('hardwareBackPress', () => {



            if (navigation.isFocused()) {

                navigation.replace("Tarjeta", { medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), token: token });
                return 1;
            }




        });


    }, [pago]);


    React.useEffect(() => {




        setToken(route.params.token);

        setMedidor(JSON.parse(route.params.medidor));

        setPago(JSON.parse(route.params.pago));

        guardar(route.params.token);

        navigation.setOptions({
            headerShown: 0
        });




    }, []);


    async function guardar(token) {

        try {


            setStatus("disabled");

            const tarjetasresponse = await gettarjetas(token);

            console.log(tarjetasresponse.data);


            setTarjetas(tarjetasresponse.data);








        } catch (error) {




            if (error.response.status === 401) {

                mediluxalert("Error", "El token de sesión ha expirado");


            }


            else if (error.response.data) {

                console.log(error.response.data);





                mediluxalert("Error", error.response.data.message);






            }
            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }

            console.log(error);


        }


        setStatus("enabled");



    }




    return (




        <View style={styles.view}>




            <View style={styles.iconButton}      >


                <IconButton
                    icon={require('../../Imagenes/credit.png')}
                    size={50}
                    iconColor='#08CE72'
                    onPress={() => {

                        navigation.replace("Tarjeta", { medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), token: token });


                    }}
                />
                <Text style={styles.pago} variant="headlineSmall"    >Tarjetas  </Text>




            </View>

            {status === "enabled" && (


                <View>




                    <Button buttonColor="#11D077" labelStyle={styles.labelbuttonpago} disabled={tipo === "Guardando"} loading={tipo === "Guardando"} onPress={() => {




                        guardartarjeta();





                    }} style={styles.buttonpago} contentStyle={styles.button} mode="contained"  >
                        {tipo}
                    </Button>





                    <ScrollView style={styles.pendientes} >



                        {tarjetas.length === 0 && (



                            <Textmedilux style={styles.mediluxnotificacion} variant="titleLarge" label="No tiene tarjetas" ></Textmedilux>

                        )}



                        {tarjetas.map((label) => {
                            return (


                                <View>


                                    <Card style={styles.cardpago} onPress={() => {





                                        navigation.replace("Notificacion", { medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), tarjeta: JSON.stringify(label), token: token });







                                    }}      >


                                        <View style={styles.mediluxlabel}      >
                                            <Textmedilux style={styles.medilux} variant="labelLarge" label={label.franquicia} ></Textmedilux>







                                        </View>

                                        <View style={styles.mediluxlabel}      >
                                            <Textmedilux style={styles.medilux} variant="labelLarge" label={"************" + label.ultimosDigitos} ></Textmedilux>






                                        </View>





                                    </Card>


                                    <View style={styles.tipopago}      >



                                        <Button buttonColor="#11D077" labelStyle={styles.labelbuttonpago} onPress={() => {




                                            tarjetaupdate(label.payerId);





                                        }} style={styles.buttontipo} contentStyle={styles.button} mode="contained"  >
                                            Editar
                                        </Button>

                                        <Button buttonColor="#11D077" labelStyle={styles.labelbuttonpago} onPress={() => {





                                            setModalstatus(1);
                                            setModaltarjeta(label.payerId)






                                        }} style={styles.buttontipo} contentStyle={styles.button} mode="contained"  >
                                            Borrar
                                        </Button>

                                    </View>



                                </View>


                            )
                        })


                        }



                    </ScrollView>

                </View>

            )}


            {status === "disabled" && (


                <ActivityIndicator animating size='large' color="#11D077" />


            )}



            <Mediluxalert hide={hide} label={label} content={content} screen={screen} route={{ medidor: JSON.stringify(medidor), pago: JSON.stringify(pago), token: token }} navigation={navigation} setHide={setHide} />



            <Portal>



                <Modal visible={modalstatus} onDismiss={(() => {

                    setModalstatus(0);

                })} >

                    <View style={styles.modal}   >


                        <Textmedilux style={styles.mediluxlabelcuenta} variant="labelLarge" label='¿Está seguro?' ></Textmedilux>


                        <View style={styles.modaltarjeta}   >


                            <Button buttonColor="#11D077" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbuttonmedilux} mode="contained" onPress={(() => {

                                tarjetadelete(modaltarjeta);


                            })} >
                                Confirmar
                            </Button>

                            <Button buttonColor="#FFFFFF" style={styles.buttonlabel} contentStyle={styles.button} labelStyle={styles.labelbuttonmedilux} textColor='#092D3D' mode="contained" onPress={(() => {

                                setModalstatus(0);
                            })} >
                                Cancelar
                            </Button>


                        </View >

                    </View >





                </Modal >

            </Portal >



        </View>




    );

}

const styles = StyleSheet.create({

    view: {

        flex: 1,

    },

    pago: {

        marginTop: "5%",
        marginBottom: "5%",
        color: "#FFFFFF",
        fontFamily: "Outfit-SemiBold"






    },

    alertpendiente: {

        marginTop: "5%",

        backgroundColor: "#FFFFFF",
        height: 200,
        justifyContent: "center",
        borderRadius: 10

    },

    pagoslabel: {
        flex: 1,

        borderBottomLeftRadius: 23,
        borderBottomRightRadius: 23,


        alignItems: "center",




        backgroundColor: "#092D3D",



    },
    tipolabel: {

        fontFamily: "Outfit-Medium",

        color: "#FFFFFF"


    },
    pagolabel: {

        alignItems: "center",
        marginBottom: "5%"



    },
    pagostipo: {

        flex: 1,


    },

    goBackButton: {

        width: '50%',
        height: '50%',

        aspectRatio: 0.6,
        resizeMode: 'contain',
        margin: 5



    }

    ,

    pago: {


        color: '#092D3D',



        marginLeft: "21%",

        fontFamily: "Outfit-SemiBold",










    },


    iconButton: {

        flexDirection: "row",
        alignItems: "center",





    },


    cardpago: {

        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        marginTop: "5%",

        borderColor: "#11D077",
        borderWidth: 1,

    },

    pagopendiente: {




        textAlign: 'left',


        color: "#11D077",


        fontFamily: "Outfit-Regular",






    },


    mediluxlabel: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginTop: "5%",
        justifyContent: "center"

    },

    medilux: {



        textAlign: 'center',

        color: "#092D3D",


        fontFamily: "Outfit-SemiBold"










    },



    tipopago: {

        flexDirection: "row",

        gap: 5,

        justifyContent: "center",


    },

    labelbuttonpago: {

        fontFamily: "Outfit-Medium",

        fontSize: 18
    },


    mediluxcuenta: {



        textAlign: 'center',


        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },



    modal: {

        backgroundColor: "#F5F8F9",


        justifyContent: 'center',

        alignItems: 'center',


        borderRadius: 10,





        marginHorizontal: "5%",

        height: "50%",



    },









    buttonlabel: {

        borderRadius: 10,
        marginTop: "5.9%"







    },






    button: {



        height: 50,













    },
    labelbuttonmedilux: {

        alignSelf: "center",



        fontFamily: "Outfit-Regular",


    },

    mediluxlabelcuenta: {


        fontSize: 20,


        fontFamily: "Outfit-Regular",



        color: "#A5ACB8"




    },


    modaltarjeta: {


        flexDirection: 'row',

        justifyContent: 'center',

        alignItems: 'center',

        gap: 53,




    },


    mediluxnotificacion: {




        color: "#A5ACB8",



        fontFamily: "Outfit-Medium",




        margin: "5%",

        alignSelf: 'center'



    },



    logo: {
        marginTop: '3%',

        textAlign: 'center',


        color: '#092D3D',

        borderRadius: 10,

        width: "80%"

    },


    card: {

        backgroundColor: "#FFFFFF",
        marginHorizontal: "10%",
        borderRadius: 5,
        marginTop: "5%",
        justifyContent: "center"


    },




    labelpago: {



        fontFamily: "Outfit-Regular",





    },


    labelpagopendiente: {




        fontFamily: "Outfit-Regular",



        color: "#A5ACB8"


    },

    alertpago: {

        borderColor: "rgba(165, 172, 184, 0.5)",
        borderWidth: 1,

        width: "100%",
        flexDirection: "row",
        backgroundColor: "#E4EEED",
        alignItems: "center",
        marginTop: '5%',
        borderRadius: 10,






    },


    labelpendientepago: {

        gap: 10,
        marginHorizontal: "5%"





    },

    buttonpago: {

        marginTop: "5%",

        borderRadius: 10,


        marginHorizontal: "5%",


    },

    buttontipo: {

        marginTop: "5%",

        borderRadius: 10,

        flex: 1

    },


    button: {



        height: 50,





    },
    imagen: {
        width: '23%',
        height: '23%',

        aspectRatio: 1.1,
        resizeMode: 'contain',
        margin: "5%"

    },

    pendientes: {



        marginHorizontal: "5%",



    }
});
