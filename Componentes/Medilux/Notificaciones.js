import * as React from "react";
import { StyleSheet, SafeAreaView, TouchableOpacity, BackHandler } from "react-native";
import EncryptedStorage from "react-native-encrypted-storage";
import { WebView } from "react-native-webview";
import axios from "axios";
import { NetworkInfo } from "react-native-network-info";

import { ActivityIndicator, IconButton, Modal, Portal, Text } from "react-native-paper";
import { Button } from "react-native-paper";
import { Image, View } from "react-native";
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Textmedilux from "../Otros/Textmedilux";
import { gethistoric, getnotificaciones, tipopago, updatenotificacion } from "../Apicuentacontrato";
import { ScrollView } from "react-native-gesture-handler";
import moment from "moment";
import Mediluxalert from "../Otros/Mediluxalert";









export default function Notificaciones({ navigation, route }) {



    const [hide, setHide] = React.useState(0);
    const [label, setLabel] = React.useState("");
    const [content, setContent] = React.useState("");


    const [token, setToken] = React.useState([]);

    const [status, setStatus] = React.useState("enabled");

    const [notificaciones, setNotificaciones] = React.useState([]);

    const [screen, setScreen] = React.useState("");


    const [notificacionespago, setNotificacionespago] = React.useState("");

    const [notificacionestarjeta, setNotificacionestarjeta] = React.useState("");

    const [modalstatus, setModalstatus] = React.useState(0);



    React.useEffect(() => {



        BackHandler.addEventListener('hardwareBackPress', () => {



            if (navigation.isFocused()) {

                navigation.replace("Medilux");

                return 1;

            }




        });

        return () => {
            BackHandler.removeEventListener('hardwareBackPress', () => {



                if (navigation.isFocused()) {

                    navigation.replace("Medilux");

                    return 1;

                }




            });
        };
    }, []);

    function mediluxalert(label, content) {


        setHide(1);



        setLabel(label);
        setContent(content);


    }

    React.useEffect(() => {

        setToken(route.params.token);



        guardar(route.params.token);

        navigation.setOptions({
            headerShown: 0
        });

        return () => { };
    }, []);


    async function guardar(token) {


        try {


            setStatus("disabled");

            const notificacionesresponse = await getnotificaciones(token);


            console.log(notificacionesresponse.data);


            setNotificaciones(notificacionesresponse.data);





        } catch (error) {



            console.log(error);



            if (error.response) {

                if (error.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (error.response.data) {

                    console.log(error.response.data);





                    mediluxalert("Error", error.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }




        }


        setStatus("enabled");









    }

    async function statusnotificacion(id) {


        try {


            setStatus("disabled");

            await updatenotificacion(id, token);

            const notificacionesresponse = await getnotificaciones(token);


            console.log(notificacionesresponse.data);


            setNotificaciones(notificacionesresponse.data);








        } catch (error) {



            console.log(error);



            if (error.response) {

                if (error.response.status === 401) {

                    mediluxalert("Error", "El token de sesión ha expirado");


                }

                else if (error.response.data) {

                    console.log(error.response.data);





                    mediluxalert("Error", error.response.data.message);






                }

            }


            else {

                mediluxalert("Error", "Hubo un error en el servidor ");


            }


        }


        setStatus("enabled");









    }




    return (




        <View style={styles.view}>



            {/* <View style={styles.pagoslabel}>












        <Text variant="headlineMedium" style={styles.pago}>
          Pagos
        </Text>


        <View style={styles.pagolabel}      >

          <Text variant="headlineSmall" style={styles.tipolabel}>
            {"Seleccione el tipo de pago "}
          </Text>


          <Text variant="headlineSmall" style={styles.tipolabel}>
            {"que desea realizar"}
          </Text>



        </View>








        <Button buttonColor="#11D077" onPress={() => {





          guardar();
        }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.tipolabel} mode="contained"  >
          Pago de luz
        </Button>

        <Button buttonColor="#11D077" onPress={() => {
        }} style={styles.logo} contentStyle={styles.button} labelStyle={styles.tipolabel} mode="contained"  >
          Otros pagos
        </Button>



      </View>

 */}
            <View style={styles.iconButton}      >


                <IconButton
                    icon={require('../../Imagenes/credit.png')}
                    size={50}
                    iconColor='#08CE72'
                    onPress={() => {

                        navigation.replace("Medilux");





                    }}
                />
                <Text style={styles.pago} variant="headlineSmall"    >Notificaciones</Text>

                <IconButton
                    size={50}
                    iconColor='#08CE72'
                    onPress={() => {

                        navigation.goBack();




                    }}
                />



            </View>



            <View style={styles.pagostipo}>




                {status === "disabled" && (


                    <ActivityIndicator animating size='large' color="#11D077" />


                )}


                {status === "enabled" && (





                    <ScrollView style={styles.pendientes} >


                        {notificaciones.length === 0 && (



                            <Textmedilux style={styles.mediluxnotificacion} variant="titleLarge" label="No tiene notificaciones" ></Textmedilux>

                        )}


                        {
                            notificaciones.map((label) => {


                                if (label.idEstadoNotificacion === 1) {

                                    return (















                                        <View style={styles.pagomedilux}      >









                                            <TouchableOpacity style={styles.pagolabel} onPress={() => {






                                                setNotificacionespago(label.asunto);
                                                setNotificacionestarjeta(label.mensaje);

                                                setModalstatus(1);
                                                statusnotificacion(label.idNotificacionUsuario);

                                            }}  >







                                                <View style={styles.mediluxlabel}      >
                                                    <Textmedilux style={styles.medilux} variant="labelLarge" label={label.asunto} ></Textmedilux>







                                                </View>

                                                <View style={styles.mediluxlabel}      >


                                                    <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={label.mensaje.substring(0, 30) + "..."} ></Textmedilux>




                                                </View>



                                            </TouchableOpacity>



                                        </View>





                                    )




                                }
                                else if (label.idEstadoNotificacion === 2) {


                                    return (















                                        <View style={styles.pagotarjeta}      >









                                            <TouchableOpacity style={styles.pagolabel} onPress={() => {






                                                setNotificacionespago(label.asunto);
                                                setNotificacionestarjeta(label.mensaje);

                                                setModalstatus(1);

                                            }}  >







                                                <View style={styles.mediluxlabel}      >
                                                    <Textmedilux style={styles.medilux} variant="labelLarge" label={label.asunto} ></Textmedilux>







                                                </View>

                                                <View style={styles.mediluxlabel}      >


                                                    <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={label.mensaje.substring(0, 30) + "..."} ></Textmedilux>




                                                </View>



                                            </TouchableOpacity>



                                        </View>





                                    )



                                }



                            })
                        }










                    </ScrollView>



                )
                }
            </View>





            <Portal>



                <Modal visible={modalstatus} onDismiss={(() => {

                    setModalstatus(0);

                })} >

                    <View style={styles.modal}   >






                        <ScrollView style={styles.mediluxpago}      >





                            <View style={styles.mediluxmodal}      >





                                <Textmedilux style={styles.medilux} variant="labelLarge" label={notificacionespago} ></Textmedilux>









                                <Textmedilux style={styles.mediluxcuenta} variant="labelLarge" label={notificacionestarjeta} ></Textmedilux>

                            </View>



                        </ScrollView>





                    </View >





                </Modal >

            </Portal >
            <Mediluxalert hide={hide} label={label} content={content} screen={screen} setHide={setHide} navigation={navigation} />


        </View>




    );

}

const styles = StyleSheet.create({

    view: {

        backgroundColor: "#F5F8F9",

        flex: 1,

    },

    pago: {

        marginTop: "5%",
        marginBottom: "5%",
        color: "#FFFFFF",
        fontFamily: "Outfit-SemiBold"






    },

    alertpendiente: {

        marginTop: "5%",

        backgroundColor: "#FFFFFF",
        height: 200,
        justifyContent: "center",
        borderRadius: 10

    },

    pagoslabel: {
        flex: 1,

        borderBottomLeftRadius: 23,
        borderBottomRightRadius: 23,


        alignItems: "center",




        backgroundColor: "#092D3D",



    },

    mediluxpago: {

        margin: "10%",




        gap: 23

    },

    mediluxmodal: {

        height: "100%",



        gap: 5

    },

    tipolabel: {

        fontFamily: "Outfit-Medium",

        color: "#FFFFFF"


    },
    pagolabel: {

        alignItems: "center",
        marginBottom: "5%"



    },

    mediluxnotificacion: {




        color: "#A5ACB8",



        fontFamily: "Outfit-Medium",




        margin: "5%",

        alignSelf: 'center'



    },
    pagostipo: {

        flex: 1,


    },

    pagolabel: {

        margin: "5%"


    },

    goBackButton: {

        width: '50%',
        height: '50%',

        aspectRatio: 0.6,
        resizeMode: 'contain',
        margin: 5



    }

    ,

    pago: {


        color: '#092D3D',




        fontFamily: "Outfit-SemiBold",










    },

    modal: {

        backgroundColor: "#FFFFFF",

        width: "80%",

        alignSelf: "center",

        borderRadius: 10,




    },


    iconButton: {

        flexDirection: "row",
        alignItems: "center",

        justifyContent: "space-between"





    },




    pagopendiente: {




        textAlign: 'left',


        color: "#11D077",


        fontFamily: "Outfit-Regular",






    },





    mediluxlabel: {

        flexDirection: "row",
        gap: 5,
        margin: "5%",
        marginTop: "0%",
        marginBottom: "0%"


    },

    medilux: {




        color: '#092D3D',


        fontFamily: "Outfit-SemiBold"










    },


    mediluxcuenta: {





        color: "#A5ACB8",



        fontFamily: "Outfit-Medium"



    },



    logo: {
        marginTop: '3%',

        textAlign: 'center',


        color: '#092D3D',

        borderRadius: 10,

        width: "80%"

    },


    pagotarjeta: {

        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        marginTop: "4%",



    },

    pagomedilux: {

        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        marginTop: "4%",
        borderColor: "#11D077",
        borderWidth: 2




    },


    button: {



        height: 50,





    },

    labelpago: {



        fontFamily: "Outfit-Regular",





    },

    labelpagopendiente: {




        fontFamily: "Outfit-Regular",



        color: "#A5ACB8"


    },

    alertpago: {

        borderColor: "rgba(165, 172, 184, 0.5)",
        borderWidth: 1,

        width: "100%",
        flexDirection: "row",
        backgroundColor: "#E4EEED",
        alignItems: "center",
        marginTop: '5%',
        borderRadius: 10,






    },


    labelpendientepago: {

        gap: 10,
        marginHorizontal: "5%"





    },


    button: {



        height: 50,





    },
    imagen: {
        width: '23%',
        height: '23%',

        aspectRatio: 1.1,
        resizeMode: 'contain',
        margin: "5%"

    },

    pendientes: {


        backgroundColor: "#F5F8F9",
        marginHorizontal: "5%",



    }
});
